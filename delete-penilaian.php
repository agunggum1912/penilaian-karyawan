<?php


include 'koneksi.php';

  // mengaktifkan session
  session_start();
  if (!isset($_SESSION['adminlogin'])) {
  // if($_SESSION['status'] != "login") {
    echo '<script language="javascript">alert("Dilarang Akses, login terlebih dahulu"); location.href="login.php"</script>';
  }

  	if (isset($_GET['id'])) {
  		$id = $_GET['id'];
      // list ($data1, $data2, $data3) = split('[/.-]', $id);
      $data = explode(".", $id);

      $data1 = "$data[0]";
      $data2 = "$data[1]";
      $data3 = "$data[2]";

      $ceksql = "SELECT id, nik FROM tb_karyawan WHERE id='$data1'";
      $cekqry = mysqli_query($koneksi, $ceksql) or die ("Query Cek salah!");
      $cekrow = mysqli_fetch_array($cekqry);
      $nik = $cekrow['nik'];

      $sqlkontrak1 = "SELECT a.id, a.nama_karyawan, a.nik, b.id_karyawan, b.mulai_penilaian AS mpk1p1, b.selesai_penilaian AS spk1p1, b.hasil AS hk1p1, c.id_karyawan, c.mulai_penilaian AS mpk1p2, c.selesai_penilaian AS spk1p2, c.hasil AS hk1p2, d.id_karyawan, d.mulai_penilaian AS mpk1p3, d.selesai_penilaian AS spk1p3, d.hasil AS hk1p3, e.id_karyawan, e.mulai_penilaian AS mpk1p4, e.selesai_penilaian AS spk1p4, e.hasil AS hk1p4 FROM tb_karyawan AS a LEFT JOIN tb_kon1_pen1 AS b ON a.id=b.id_karyawan LEFT JOIN tb_kon1_pen2 AS c ON a.id=c.id_karyawan LEFT JOIN tb_kon1_pen3 AS d ON a.id=d.id_karyawan LEFT JOIN tb_kon1_pen4 AS e ON a.id=e.id_karyawan WHERE a.nik='$nik'";
      $qrykontrak1 = mysqli_query($koneksi, $sqlkontrak1);
      $rowkontrak1 = mysqli_fetch_array($qrykontrak1);

      $sqlkontrak2 = "SELECT a.id, a.nama_karyawan, a.nik, b.id_karyawan, b.mulai_penilaian AS mpk2p1, b.selesai_penilaian AS spk2p1, b.hasil AS hk2p1, c.id_karyawan, c.mulai_penilaian AS mpk2p2, c.selesai_penilaian AS spk2p2, c.hasil AS hk2p2, d.id_karyawan, d.mulai_penilaian AS mpk2p3, d.selesai_penilaian AS spk2p3, d.hasil AS hk2p3, e.id_karyawan, e.mulai_penilaian AS mpk2p4, e.selesai_penilaian AS spk2p4, e.hasil AS hk2p4 FROM tb_karyawan AS a LEFT JOIN tb_kon2_pen1 AS b ON a.id=b.id_karyawan LEFT JOIN tb_kon2_pen2 AS c ON a.id=c.id_karyawan LEFT JOIN tb_kon2_pen3 AS d ON a.id=d.id_karyawan LEFT JOIN tb_kon2_pen4 AS e ON a.id=e.id_karyawan WHERE a.nik='$nik'";
      $qrykontrak2 = mysqli_query($koneksi, $sqlkontrak2);
      $rowkontrak2 = mysqli_fetch_array($qrykontrak2);

      $sqlkontrak3 = "SELECT a.id, a.nama_karyawan, a.nik, b.id_karyawan, b.mulai_penilaian AS mpk3p1, b.selesai_penilaian AS spk3p1, b.hasil AS hk3p1, c.id_karyawan, c.mulai_penilaian AS mpk3p2, c.selesai_penilaian AS spk3p2, c.hasil AS hk3p2, d.id_karyawan, d.mulai_penilaian AS mpk3p3, d.selesai_penilaian AS spk3p3, d.hasil AS hk3p3, e.id_karyawan, e.mulai_penilaian AS mpk3p4, e.selesai_penilaian AS spk3p4, e.hasil AS hk3p4 FROM tb_karyawan AS a LEFT JOIN tb_kon3_pen1 AS b ON a.id=b.id_karyawan LEFT JOIN tb_kon3_pen2 AS c ON a.id=c.id_karyawan LEFT JOIN tb_kon3_pen3 AS d ON a.id=d.id_karyawan LEFT JOIN tb_kon3_pen4 AS e ON a.id=e.id_karyawan WHERE a.nik='$nik'";
      $qrykontrak3 = mysqli_query($koneksi, $sqlkontrak3);
      $rowkontrak3 = mysqli_fetch_array($qrykontrak3);



      if ($data2 == "1" && $data3 == "1") {
        if ($rowkontrak3['mpk3p1'] != "0000-00-00") {
          echo "<script>alert('$nik sudah sampai proses Contract 3, Silahkan proses hapus Contract 3 terlebih dahulu!');history.go(-1)</script>";
        }elseif ($rowkontrak2['mpk2p1'] != "0000-00-00") {
          echo "<script>alert('$nik sudah sampai proses Contract 2, Silahkan proses hapus Contract 2 terlebih dahulu!');history.go(-1)</script>";
        }elseif ($rowkontrak1['mpk1p4'] != "0000-00-00") {
          echo "<script>alert('Silahkan proses hapus Penilaian 4 terlebih dahulu!');history.go(-1)</script>";
        }elseif ($rowkontrak1['mpk1p3'] != "0000-00-00") {
          echo "<script>alert('Silahkan proses hapus Penilaian 3 terlebih dahulu!');history.go(-1)</script>";
        }elseif ($rowkontrak1['mpk1p2'] != "0000-00-00") {
          echo "<script>alert('Silahkan proses hapus Penilaian 2 terlebih dahulu!');history.go(-1)</script>";
        }else{
          $sql1 = "UPDATE tb_kon1_pen1 SET mulai_penilaian='', selesai_penilaian='' WHERE id_karyawan='$data1'";
          $qry1 = mysqli_query($koneksi, $sql1) or die ("Query kontrak 1 penilaian 1 salah!");
          
          $sqlkon1 ="UPDATE tb_kontrak1 SET status_penilaian='' WHERE id_karyawan='$data1'";
          $qrykon1 = mysqli_query($koneksi, $sqlkon1) or die ("Query Kon1 salah!");

          echo "<script>alert('Contract 1 pada Penilaian 1 telah di hapus!') ;window.location='editass.php?nik=$nik'; </script>";
        }
      }elseif ($data2 == "1" && $data3 == "2") {
        if ($rowkontrak3['mpk3p1'] != "0000-00-00") {
          echo "<script>alert('$nik sudah sampai proses Contract 3, Silahkan proses hapus Contract 3 terlebih dahulu!');history.go(-1)</script>";
        }elseif ($rowkontrak2['mpk2p1'] != "0000-00-00") {
          echo "<script>alert('$nik sudah sampai proses Contract 2, Silahkan proses hapus Contract 2 terlebih dahulu!');history.go(-1)</script>";
        }elseif ($rowkontrak1['mpk1p4'] != "0000-00-00") {
          echo "<script>alert('Silahkan proses hapus Penilaian 4 terlebih dahulu!');history.go(-1)</script>";
        }elseif ($rowkontrak1['mpk1p3'] != "0000-00-00") {
          echo "<script>alert('Silahkan proses hapus Penilaian 3 terlebih dahulu!');history.go(-1)</script>";
        }else{
          $sql2 = "UPDATE tb_kon1_pen2 SET mulai_penilaian='', selesai_penilaian='' WHERE id_karyawan='$data1'";
          $qry2 = mysqli_query($koneksi, $sql2) or die ("Query kontrak 1 penilaian 2 salah!");
          
          $sqlkon2 ="UPDATE tb_kontrak1 SET status_penilaian='1' WHERE id_karyawan='$data1'";
          $qrykon2 = mysqli_query($koneksi, $sqlkon2) or die ("Query Kon1 salah!");

          echo "<script>alert('Contract 1 pada Penilaian 2 telah di hapus!') ;window.location='editass.php?nik=$nik'; </script>";
        }
      }elseif ($data2 == "1" && $data3 == "3") {
        if ($rowkontrak3['mpk3p1'] != "0000-00-00") {
          echo "<script>alert('$nik sudah sampai proses Contract 3, Silahkan proses hapus Contract 3 terlebih dahulu!');history.go(-1)</script>";
        }elseif ($rowkontrak2['mpk2p1'] != "0000-00-00") {
          echo "<script>alert('$nik sudah sampai proses Contract 2, Silahkan proses hapus Contract 2 terlebih dahulu!');history.go(-1)</script>";
        }elseif ($rowkontrak1['mpk1p4'] != "0000-00-00") {
          echo "<script>alert('Silahkan proses hapus Penilaian 4 terlebih dahulu!');history.go(-1)</script>";
        }else{
          $sql3 = "UPDATE tb_kon1_pen3 SET mulai_penilaian='', selesai_penilaian='' WHERE id_karyawan='$data1'";
          $qry3 = mysqli_query($koneksi, $sql3) or die ("Query kontrak 1 penilaian 3 salah!");

          $sqlkon3 ="UPDATE tb_kontrak1 SET status_penilaian='2' WHERE id_karyawan='$data1'";
          $qrykon3 = mysqli_query($koneksi, $sqlkon3) or die ("Query Kon1 salah!");

          echo "<script>alert('Contract 1 pada Penilaian 3 telah di hapus!') ;window.location='editass.php?nik=$nik'; </script>";
        }
      }elseif ($data2 == "1" && $data3 == "4") {
        if ($rowkontrak3['mpk3p1'] != "0000-00-00") {
          echo "<script>alert('$nik sudah sampai proses Contract 3, Silahkan proses hapus Contract 3 terlebih dahulu!');history.go(-1)</script>";
        }elseif ($rowkontrak2['mpk2p1'] != "0000-00-00") {
          echo "<script>alert('$nik sudah sampai proses Contract 2, Silahkan proses hapus Contract 2 terlebih dahulu!');history.go(-1)</script>";
        }else{
          $sql4 = "UPDATE tb_kon1_pen4 SET mulai_penilaian='', selesai_penilaian='' WHERE id_karyawan='$data1'";
          $qry4 = mysqli_query($koneksi, $sql4) or die ("Query kontrak 1 penilaian 4 salah!");

          $sqlkon4 ="UPDATE tb_kontrak1 SET status_penilaian='3' WHERE id_karyawan='$data1'";
          $qrykon4 = mysqli_query($koneksi, $sqlkon4) or die ("Query Kon1 salah!");

          echo "<script>alert('Contract 1 pada Penilaian 4 telah di hapus!') ;window.location='editass.php?nik=$nik'; </script>";
        }
      }elseif ($data2 == "2" && $data3 == "1") {
        if ($rowkontrak3['mpk3p1'] != "0000-00-00") {
          echo "<script>alert('$nik sudah sampai proses Contract 3, Silahkan proses hapus Contract 3 terlebih dahulu!');history.go(-1)</script>";
        }elseif ($rowkontrak2['mpk2p4'] != "0000-00-00") {
          echo "<script>alert('Silahkan proses hapus Penilaian 4 terlebih dahulu!');history.go(-1)</script>";
        }elseif ($rowkontrak2['mpk2p3'] != "0000-00-00") {
          echo "<script>alert('Silahkan proses hapus Penilaian 3 terlebih dahulu!');history.go(-1)</script>";
        }elseif ($rowkontrak2['mpk2p2'] != "0000-00-00") {
          echo "<script>alert('Silahkan proses hapus Penilaian 2 terlebih dahulu!');history.go(-1)</script>";
        }else{
          $sql5 = "UPDATE tb_kon2_pen1 SET mulai_penilaian='', selesai_penilaian='' WHERE id_karyawan='$data1'";
          $qry5 = mysqli_query($koneksi, $sql5) or die ("Query kontrak 2 penilaian 1 salah!");
          
          $sqlkon5 ="UPDATE tb_kontrak2 SET status_penilaian='' WHERE id_karyawan='$data1'";
          $qrykon5 = mysqli_query($koneksi, $sqlkon5) or die ("Query Kon1 salah!");

          echo "<script>alert('Contract 2 pada Penilaian 1 telah di hapus!') ;window.location='editass.php?nik=$nik'; </script>";
        }
      }elseif ($data2 == "2" && $data3 == "2") {
        if ($rowkontrak3['mpk3p1'] != "0000-00-00") {
          echo "<script>alert('$nik sudah sampai proses Contract 3, Silahkan proses hapus Contract 3 terlebih dahulu!');history.go(-1)</script>";
        }elseif ($rowkontrak2['mpk2p4'] != "0000-00-00") {
          echo "<script>alert('Silahkan proses hapus Penilaian 4 terlebih dahulu!');history.go(-1)</script>";
        }elseif ($rowkontrak2['mpk2p3'] != "0000-00-00") {
          echo "<script>alert('Silahkan proses hapus Penilaian 3 terlebih dahulu!');history.go(-1)</script>";
        }else{
          $sql6 = "UPDATE tb_kon2_pen2 SET mulai_penilaian='', selesai_penilaian='' WHERE id_karyawan='$data1'";
          $qry6 = mysqli_query($koneksi, $sql6) or die ("Query kontrak 2 penilaian 2 salah!");

          $sqlkon6 ="UPDATE tb_kontrak2 SET status_penilaian='1' WHERE id_karyawan='$data1'";
          $qrykon6 = mysqli_query($koneksi, $sqlkon6) or die ("Query Kon1 salah!");

          echo "<script>alert('Contract 2 pada Penilaian 2 telah di hapus!') ;window.location='editass.php?nik=$nik'; </script>";
        }
      }elseif ($data2 == "2" && $data3 == "3") {
        if ($rowkontrak3['mpk3p1'] != "0000-00-00") {
          echo "<script>alert('$nik sudah sampai proses Contract 3, Silahkan proses hapus Contract 3 terlebih dahulu!');history.go(-1)</script>";
        }elseif ($rowkontrak2['mpk2p4'] != "0000-00-00") {
          echo "<script>alert('Silahkan proses hapus Penilaian 4 terlebih dahulu!');history.go(-1)</script>";
        }else{
          $sql7 = "UPDATE tb_kon2_pen3 SET mulai_penilaian='', selesai_penilaian='' WHERE id_karyawan='$data1'";
          $qry7 = mysqli_query($koneksi, $sql7) or die ("Query kontrak 2 penilaian 3 salah!");
          
          $sqlkon7 ="UPDATE tb_kontrak2 SET status_penilaian='2' WHERE id_karyawan='$data1'";
          $qrykon7 = mysqli_query($koneksi, $sqlkon7) or die ("Query Kon1 salah!");

          echo "<script>alert('Contract 2 pada Penilaian 3 telah di hapus!') ;window.location='editass.php?nik=$nik'; </script>";
        }
      }elseif ($data2 == "2" && $data3 == "4") {
        if ($rowkontrak3['mpk3p1'] != "0000-00-00") {
          echo "<script>alert('$nik sudah sampai proses Contract 3, Silahkan proses hapus Contract 3 terlebih dahulu!');history.go(-1)</script>";
        }else{
          $sql8 = "UPDATE tb_kon2_pen4 SET mulai_penilaian='', selesai_penilaian='' WHERE id_karyawan='$data1'";
          $qry8 = mysqli_query($koneksi, $sql8) or die ("Query kontrak 2 penilaian 4 salah!");
          
          $sqlkon8 ="UPDATE tb_kontrak2 SET status_penilaian='3' WHERE id_karyawan='$data1'";
          $qrykon8 = mysqli_query($koneksi, $sqlkon8) or die ("Query Kon1 salah!");

          echo "<script>alert('Contract 2 pada Penilaian 4 telah di hapus!') ;window.location='editass.php?nik=$nik'; </script>";
        }
      }elseif ($data2 == "3" && $data3 == "1") {
        if ($rowkontrak3['mpk3p4'] != "0000-00-00") {
          echo "<script>alert('Silahkan proses hapus Penilaian 4 terlebih dahulu!');history.go(-1)</script>";
        }elseif ($rowkontrak3['mpk3p3'] != "0000-00-00") {
          echo "<script>alert('Silahkan proses hapus Penilaian 3 terlebih dahulu!');history.go(-1)</script>";
        }elseif ($rowkontrak3['mpk3p2'] != "0000-00-00") {
          echo "<script>alert('Silahkan proses hapus Penilaian 2 terlebih dahulu!');history.go(-1)</script>";
        }else{
          $sql9 = "UPDATE tb_kon3_pen1 SET mulai_penilaian='', selesai_penilaian='' WHERE id_karyawan='$data1'";
          $qry9 = mysqli_query($koneksi, $sql9) or die ("Query kontrak 3 penilaian 1 salah!");
          
          $sqlkon9 ="UPDATE tb_kontrak3 SET status_penilaian='' WHERE id_karyawan='$data1'";
          $qrykon9 = mysqli_query($koneksi, $sqlkon9) or die ("Query Kon1 salah!");

          echo "<script>alert('Contract 3 pada Penilaian 1 telah di hapus!') ;window.location='editass.php?nik=$nik'; </script>";
        }
      }elseif ($data2 == "3" && $data3 == "2") {
        if ($rowkontrak3['mpk3p4'] != "0000-00-00") {
          echo "<script>alert('Silahkan proses hapus Penilaian 4 terlebih dahulu!');history.go(-1)</script>";
        }elseif ($rowkontrak3['mpk3p3'] != "0000-00-00") {
          echo "<script>alert('Silahkan proses hapus Penilaian 3 terlebih dahulu!');history.go(-1)</script>";
        }else{
          $sql10 = "UPDATE tb_kon3_pen2 SET mulai_penilaian='', selesai_penilaian='' WHERE id_karyawan='$data1'";
          $qry10 = mysqli_query($koneksi, $sql10) or die ("Query kontrak 3 penilaian 2 salah!");

          $sqlkon10 ="UPDATE tb_kontrak3 SET status_penilaian='1' WHERE id_karyawan='$data1'";
          $qrykon10 = mysqli_query($koneksi, $sqlkon10) or die ("Query Kon1 salah!");

          echo "<script>alert('Contract 3 pada Penilaian 2 telah di hapus!') ;window.location='editass.php?nik=$nik'; </script>";
        }
      }elseif ($data2 == "3" && $data3 == "3") {
        if ($rowkontrak3['mpk3p4'] != "0000-00-00") {
          echo "<script>alert('Silahkan proses hapus Penilaian 4 terlebih dahulu!');history.go(-1)</script>";
        }else{
          $sql11 = "UPDATE tb_kon3_pen3 SET mulai_penilaian='', selesai_penilaian='' WHERE id_karyawan='$data1'";
          $qry11 = mysqli_query($koneksi, $sql11) or die ("Query kontrak 3 penilaian 3 salah!");
          
          $sqlkon11 ="UPDATE tb_kontrak3 SET status_penilaian='2' WHERE id_karyawan='$data1'";
          $qrykon11 = mysqli_query($koneksi, $sqlkon11) or die ("Query Kon1 salah!");

          echo "<script>alert('Contract 3 pada Penilaian 3 telah di hapus!') ;window.location='editass.php?nik=$nik'; </script>";
        }
      }elseif ($data2 == "3" && $data3 == "4") {
        $sql12 = "UPDATE tb_kon3_pen4 SET mulai_penilaian='', selesai_penilaian='' WHERE id_karyawan='$data1'";
        $qry12 = mysqli_query($koneksi, $sql12) or die ("Query kontrak 3 penilaian 4 salah!");
        
        $sqlkon12 ="UPDATE tb_kontrak3 SET status_penilaian='3' WHERE id_karyawan='$data1'";
          $qrykon12 = mysqli_query($koneksi, $sqlkon12) or die ("Query Kon1 salah!");

        echo "<script>alert('Contract 3 pada Penilaian 4 telah di hapus!') ;window.location='editass.php?nik=$nik'; </script>";
      }
  	}else{
  		echo "<script>alert('Ups!!! ada yang salah, silahkan kembali.') ;window.location='viewass.php'; </script>";
  	}

?>