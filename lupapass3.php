<?php
   include 'koneksi.php';
   include('../email_php/phpmailer/Exception.php');
   include('../email_php/phpmailer/PHPMailer.php');
   include('../email_php/phpmailer/SMTP.php');
   if(isset($_POST["email"]) && (!empty($_POST["email"]))){
   $email = $_POST["email"];
   $email = filter_var($email, FILTER_SANITIZE_EMAIL);
   $email = filter_var($email, FILTER_VALIDATE_EMAIL);
      if (!$email) {
         $error .="<p>Alamat email tidak valid, Silahkan masukkan email yang valid!</p>";
      }else{
         $sel_query = "SELECT * FROM tb_user WHERE email='$email'";
         $results = mysqli_query($koneksi,$sel_query);
         $row = mysqli_num_rows($results);
         if ($row==""){
            $error .= "<p>Tidak ada pengguna terdaftar dengan alamat email ini!</p>";
         }
      }
   
      if($error!=""){
         echo "<div class='error'>".$error."</div><br/><a href='javascript:history.go(-1)'>Go Back</a>";
      }else{
         $expFormat = mktime(date("H"), date("i"), date("s"), date("m") ,date("d")+1, date("Y"));
         $expDate = date("Y-m-d H:i:s",$expFormat);
         $key = md5(2418*2+$email);
         $addKey = substr(md5(uniqid(rand(),1)),3,10);
         $key = $key . $addKey;
         // Insert Temp Table
         mysqli_query($con,"INSERT INTO password_reset_temp (email,key,expDate) VALUES ('$email','$key','$expDate');");
 
         $output='<p>Dear user,</p>';
         $output.='<p>Please click on the following link to reset your password.</p>';
         $output.='<p>-------------------------------------------------------------</p>';
         $output.='<p><a href="https://localhost/penilaian/reset-password.php?key='.$key.'&email='.$email.'&action=reset" target="_blank">https://localhost/penilaian/reset-password.php?key='.$key.'&email='.$email.'&action=reset</a></p>'; 
         $output.='<p>-------------------------------------------------------------</p>';
         $output.='<p>Please be sure to copy the entire link into your browser.The link will expire after 1 day for security reason.</p>';
         $output.='<p>If you did not request this forgotten password email, no action is needed, your password will not be reset. However, you may want to log into your account and change your security password as someone may have guessed it.</p>';   
         $output.='<p>Thanks,</p>';
         $body = $output; 
         $subject = "Password Recovery";
          
         $email_to = $email;
         $fromserver = "localhost"; 
         require("PHPMailer/PHPMailerAutoload.php");
         $mail = new PHPMailer();
         $mail->IsSMTP();
         $mail->Host = "smtp.gmail.com"; // Enter your host here
         $mail->SMTPAuth = true;
         $mail->Username = "agunggumelar1912@gmail.com"; // Enter your email here
         $mail->Password = "RebmemeR2552"; //Enter your password here
         $mail->Port = 465;
         $mail->IsHTML(true);
         $mail->From = "agunggumelar1912@gmail.com";
         $mail->FromName = "Agung Gumelar";
         $mail->Sender = $fromserver; // indicates ReturnPath header
         $mail->Subject = $subject;
         $mail->Body = $body;
         $mail->AddAddress($email_to);
            if(!$mail->Send()){
               echo "Mailer Error: " . $mail->ErrorInfo;
            }else{
               echo "<div class='error'><p>An email has been sent to you with instructions on how to reset your password.</p></div><br /><br /><br />";
            }
      }
   }else{
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="assets/css/style.css">
    <link rel="stylesheet" href="assets/font-awesome/css/all.min.css" type="text/css">

    <link rel="shortcut icon" type="image/x-icon" href="gambar/logociputra.svg">
    <title>Ciputra</title>

    
</head>

<body class="bg-color-1">
   <div>
      <form class="form" name="login" action="" method="post">
         <div class="align-middle">
            <div class="tengah2 kotak_tengah2">
               </br>
               <center><i class="fas fa-lock fa-5x"></i></center><br>
               <center><h3 style="line-height: 24px;">Forgot Password?</h3></center>
               <center><font size="2">You can reset your password here.</font></center>
               </br>
               <div class="input-group mb-3">
                  <div class="input-group-append">
                     <span class="input-group-text warna-icon-login"><i class="fas fa-envelope"></i></span>
                  </div>
                     <input type="text" name="email" class="form-control input_user border-list" placeholder="Email">
               </div>
               </br>

               <table width="100%">
                  <tr>
                     <td style="border: 0; padding: 0px;">
                        <button type="submit" name="submit" class="btn btn-primary button-right">Send</button>
                        <a href="login.php" class="btn btn-dark button-left button-space">Back</a>
                     </td>
                  </tr>
               </table>
            </div>
         </div>
      </form>
   </div>


   <script src="assets/js/jquery.js"></script> 
   <script src="assets/js/popper.js"></script> 
   <script src="assets/js/bootstrap.js"></script>
</body>
</html>
<?php } ?>