<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <link rel="shortcut icon" type="image/x-icon" href="gambar/logociputra.svg">
   <title>Ciputra</title>
   <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

   <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.css">
   <link rel="stylesheet" type="text/css" href="assets/css/style.css">
   <link rel="stylesheet" href="assets/font-awesome/css/all.min.css" type="text/css">

    
</head>
<?php
   include 'koneksi.php';
   include('phpmailer/src/Exception.php');
   include('phpmailer/src/PHPMailer.php');
   include('phpmailer/src/SMTP.php');

   if (isset($_POST['submit'])) {
      $email = $_POST['email'];

      $cek = "SELECT email FROM tb_user WHERE email='$email'";
      $cekqry = mysqli_query($koneksi, $cek) or die("Query cek email salah!");
      $rowcek = mysqli_fetch_array($cekqry);

      if ($rowcek == "") {
         echo "<script>alert('Tidak ada pengguna terdaftar dengan alamat email ini!');history.go(-1)</script>";
      }elseif ($email == $rowcek['email']) {
         $expFormat = mktime(date("H"), date("i"), date("s"), date("m") ,date("d")+1, date("Y"));
         $expDate = date("Y-m-d H:i:s",$expFormat);
         $key = md5(2418*2+floatval($email));
         $addKey = substr(md5(uniqid(rand(),1)),3,10);
         $key = $key . $addKey;
         print_r($email);
         echo "<br>";
         print_r($key);
         echo "<br>";
         print_r($expDate);
         $sql = "INSERT INTO `password_reset_temp` (`email`, `key`, `expDate`) VALUES ('".$email."', '".$key."', '".$expDate."');";
         $qry = mysqli_query($koneksi, $sql) or die ("Query Insert salah!");

         $output='<p>Dear user,</p>';
         $output.='<p>Please click on the following link to reset your password.</p>';
         $output.='<p>-------------------------------------------------------------</p>';
         $output.='<p><a href="https://localhost/penilaian/reset-password.php?key='.$key.'&email='.$email.'&action=reset" target="_blank">https://localhost/penilaian/reset-password.php?key='.$key.'&email='.$email.'&action=reset</a></p>'; 
         $output.='<p>-------------------------------------------------------------</p>';
         $output.='<p>Please be sure to copy the entire link into your browser.The link will expire after 1 day for security reason.</p>';
         $output.='<p>If you did not request this forgotten password email, no action is needed, your password will not be reset. However, you may want to log into your account and change your security password as someone may have guessed it.</p>';   
         $output.='<p>Thanks,</p>';
         $body = $output; 
         $subject = "Password Recovery";
         $email_pengirim = "agung.phpmailer@gmail.com";
         $nama_pengirim = "Support System";
          
         // $email_to = $email;
         // $fromserver = "localhost"; 
         require('phpmailer/src/PHPMailerAutoload.php');
         $mail = new PHPMailer();
         $mail->IsSMTP();
         $mail->Host = "smtp.gmail.com"; // Enter your host here
         $mail->Username = $email_pengirim; // Enter your email here
         $mail->Password = "uazfasbaspcfcdru"; //Enter your password here
         $mail->Port = 465;
         $mail->SMTPAuth = true;
         $mail->SMTPSecure = 'ssl';
         $mail->SMTPDebug = 2;

         $mail->setFrom($email_pengirim, $nama_pengirim);
         $mail->addAddress($email, '');
         $mail->IsHTML(true);

         ob_start();
         include ('phpmailer/src/content.php');
         $content = ob_get_contents(); // Ambil isi file content.php dan masukan ke variabel $content
         ob_end_clean();

         $mail->Subject = $subject;
         $mail->Body = $content;
         // $mail->FromName = $nama_pengirim;

         $send = $mail->send();

         if ($send) {
            echo "<script>alert('Password Recovery telah dikirimkan kepada email anda, Silahkan ikuti langkah-langkah di email untuk Recover Password anda');window.location='login.php';</script>";
         }else{
            echo "<script>alert('Email gagal terkirim!');history.go(-1)</script>";
         }
      }
   }

?>

<body class="bg-color-1">
   <div>
      <form class="form" name="login" action="" method="post">
         <div class="align-middle">
            <div class="tengah2 kotak_tengah2">
               </br>
               <center><i class="fas fa-lock fa-5x"></i></center><br>
               <center><h3 style="line-height: 24px;">Forgot Password?</h3></center>
               <center><font size="2">You can reset your password here.</font></center>
               </br>
               <div class="input-group mb-3">
                  <div class="input-group-append">
                     <span class="input-group-text warna-icon-login"><i class="fas fa-envelope"></i></span>
                  </div>
                     <input type="text" name="email" class="form-control input_user border-list" placeholder="Email" autocomplete="off">
               </div>
               </br>

               <table width="100%">
                  <tr>
                     <td style="border: 0; padding: 0px;">
                        <button type="submit" name="submit" class="btn btn-primary button-right">Send</button>
                        <a href="login.php" class="btn btn-dark button-left button-space">Back</a>
                     </td>
                  </tr>
               </table>
            </div>
         </div>
      </form>
   </div>


   <script src="assets/js/jquery.js"></script> 
   <script src="assets/js/popper.js"></script> 
   <script src="assets/js/bootstrap.js"></script>
</body>
</html>