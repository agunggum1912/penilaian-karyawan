-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 07 Agu 2020 pada 13.51
-- Versi server: 10.4.11-MariaDB
-- Versi PHP: 7.4.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_penilaian`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `password_reset_temp`
--

CREATE TABLE `password_reset_temp` (
  `email` varchar(250) NOT NULL,
  `key` varchar(250) NOT NULL,
  `expDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `password_reset_temp`
--

INSERT INTO `password_reset_temp` (`email`, `key`, `expDate`) VALUES
('maryulianti26@gmail.com', '768e78024aa8fdb9b8fe87be86f6474517ad2d8b5b', '2020-08-05 19:29:01'),
('maryulianti26@gmail.com', '768e78024aa8fdb9b8fe87be86f64745f122245100', '2020-08-06 19:28:43');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_karyawan`
--

CREATE TABLE `tb_karyawan` (
  `id` int(10) NOT NULL,
  `nik` varchar(10) NOT NULL,
  `nama_karyawan` varchar(60) NOT NULL,
  `id_user1` int(10) NOT NULL,
  `id_user2` int(10) NOT NULL,
  `tgl_masuk` date NOT NULL,
  `status_karyawan` varchar(10) NOT NULL,
  `lokasi` varchar(60) NOT NULL,
  `departemen_karyawan` varchar(30) NOT NULL,
  `posisi_karyawan` varchar(50) NOT NULL,
  `golongan` varchar(2) NOT NULL,
  `tgl_dibuat` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_karyawan`
--

INSERT INTO `tb_karyawan` (`id`, `nik`, `nama_karyawan`, `id_user1`, `id_user2`, `tgl_masuk`, `status_karyawan`, `lokasi`, `departemen_karyawan`, `posisi_karyawan`, `golongan`, `tgl_dibuat`) VALUES
(22, 'A2 2222', 'Heru', 11, 12, '2020-07-27', 'Kontrak 2', 'Mall', 'HCGA', 'Admin', '1B', '2020-07-30 09:18:48'),
(23, 'A3 3333', 'Maze', 11, 1, '2020-07-27', 'Kontrak 1', 'Citra Raya', 'SLDI', 'Customer Service', '1B', '2020-07-30 07:26:31'),
(24, 'A1 1111', 'Almar', 11, 12, '2020-07-28', 'Kontrak 2', 'BSD', 'BKPL', 'Sales Promotion', '1C', '2020-07-30 09:19:03'),
(26, 'A4 4444', 'Michael Jordan', 12, 1, '2020-07-29', 'Kontrak 1', 'United Kingdom', 'UFC', 'Gulat', '1D', '2020-07-30 07:26:39'),
(27, 'A5 5555', 'Prilly', 12, 1, '2020-07-29', 'Kontrak 1', 'Jakarta', 'PJ', 'Actor', '1D', '2020-07-30 07:27:34'),
(28, 'A1-1111', 'Nike', 12, 1, '2020-07-30', 'Kontrak 1', 'Ciputra', 'FA', 'Front Office', '1C', '2020-07-30 07:27:22');

--
-- Trigger `tb_karyawan`
--
DELIMITER $$
CREATE TRIGGER `after_insert_karyawan` AFTER INSERT ON `tb_karyawan` FOR EACH ROW BEGIN
	INSERT INTO tb_kontrak1 (id_karyawan,id_proses) VALUES (NEW.id,1);
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `before_delete_karyawan` BEFORE DELETE ON `tb_karyawan` FOR EACH ROW BEGIN
    DELETE FROM tb_kontrak1 WHERE id_karyawan=OLD.id;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_kon1_pen1`
--

CREATE TABLE `tb_kon1_pen1` (
  `id_karyawan` int(10) NOT NULL,
  `mulai_penilaian` date NOT NULL,
  `selesai_penilaian` date NOT NULL,
  `jawaban1` int(1) NOT NULL,
  `jawaban2` int(1) NOT NULL,
  `jawaban3` int(1) NOT NULL,
  `jawaban4` int(1) NOT NULL,
  `jawaban5` int(1) NOT NULL,
  `jawaban6` int(1) NOT NULL,
  `jawaban7` int(1) NOT NULL,
  `jawaban8` int(1) NOT NULL,
  `jawaban9` int(1) NOT NULL,
  `jawaban10` int(1) NOT NULL,
  `jawaban11` int(1) NOT NULL,
  `ket1` varchar(1200) NOT NULL,
  `ket2` varchar(1200) NOT NULL,
  `ket3` varchar(1200) NOT NULL,
  `hasil` varchar(1) NOT NULL,
  `rekomendasi` varchar(70) NOT NULL,
  `tgl_penilaian` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_kon1_pen1`
--

INSERT INTO `tb_kon1_pen1` (`id_karyawan`, `mulai_penilaian`, `selesai_penilaian`, `jawaban1`, `jawaban2`, `jawaban3`, `jawaban4`, `jawaban5`, `jawaban6`, `jawaban7`, `jawaban8`, `jawaban9`, `jawaban10`, `jawaban11`, `ket1`, `ket2`, `ket3`, `hasil`, `rekomendasi`, `tgl_penilaian`) VALUES
(22, '2020-07-27', '2020-10-27', 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 'siap', 'pasti siap', '', 'D', '', '2020-07-29 16:30:50'),
(23, '2020-07-27', '2020-10-27', 2, 2, 2, 2, 2, 1, 1, 1, 1, 1, 1, '', '', '', 'E', '', '2020-07-27 19:20:58'),
(24, '2020-07-28', '2020-10-28', 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 'Sudah ok', 'Lanjukan', '', 'B', '', '2020-07-29 09:46:24'),
(26, '2020-07-29', '2020-10-29', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '', '2020-07-29 05:49:44'),
(27, '0000-00-00', '0000-00-00', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '', '2020-07-29 05:42:35'),
(28, '0000-00-00', '0000-00-00', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '', '2020-07-29 18:15:22');

--
-- Trigger `tb_kon1_pen1`
--
DELIMITER $$
CREATE TRIGGER `after_insert_kon1_pen1` AFTER INSERT ON `tb_kon1_pen1` FOR EACH ROW BEGIN
	INSERT INTO tb_kon1_pen2 (id_karyawan) VALUES (NEW.id_karyawan);
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `before_delete_kon1_pen1` BEFORE DELETE ON `tb_kon1_pen1` FOR EACH ROW BEGIN
    DELETE FROM tb_kon1_pen2 WHERE id_karyawan=OLD.id_karyawan;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_kon1_pen2`
--

CREATE TABLE `tb_kon1_pen2` (
  `id_karyawan` int(10) NOT NULL,
  `mulai_penilaian` date NOT NULL,
  `selesai_penilaian` date NOT NULL,
  `jawaban1` int(1) NOT NULL,
  `jawaban2` int(1) NOT NULL,
  `jawaban3` int(1) NOT NULL,
  `jawaban4` int(1) NOT NULL,
  `jawaban5` int(1) NOT NULL,
  `jawaban6` int(1) NOT NULL,
  `jawaban7` int(1) NOT NULL,
  `jawaban8` int(1) NOT NULL,
  `jawaban9` int(1) NOT NULL,
  `jawaban10` int(1) NOT NULL,
  `jawaban11` int(1) NOT NULL,
  `ket1` varchar(1200) NOT NULL,
  `ket2` varchar(1200) NOT NULL,
  `ket3` varchar(1200) NOT NULL,
  `hasil` varchar(1) NOT NULL,
  `rekomendasi` varchar(70) NOT NULL,
  `tgl_penilaian` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_kon1_pen2`
--

INSERT INTO `tb_kon1_pen2` (`id_karyawan`, `mulai_penilaian`, `selesai_penilaian`, `jawaban1`, `jawaban2`, `jawaban3`, `jawaban4`, `jawaban5`, `jawaban6`, `jawaban7`, `jawaban8`, `jawaban9`, `jawaban10`, `jawaban11`, `ket1`, `ket2`, `ket3`, `hasil`, `rekomendasi`, `tgl_penilaian`) VALUES
(22, '2020-10-27', '2021-01-27', 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 'Bismillah 2x', 'ok ini mah 3x', 'iya bos udah pusing 4x', 'C', 'Diangkat menjadi karyawan tetap setelah habis masa kerja kontrak.', '2020-07-29 17:37:59'),
(23, '2020-10-27', '2021-01-27', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '', '2020-07-29 05:59:23'),
(24, '2020-10-28', '2021-01-28', 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 'Siapp boss', 'Semoga lancar boss', '', 'B', '', '2020-07-28 17:31:17'),
(26, '2020-10-29', '2021-01-29', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '', '2020-07-29 05:49:44'),
(27, '0000-00-00', '0000-00-00', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '', '2020-07-29 05:42:35'),
(28, '0000-00-00', '0000-00-00', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '', '2020-07-29 18:15:22');

--
-- Trigger `tb_kon1_pen2`
--
DELIMITER $$
CREATE TRIGGER `after_insert_kon1_pen2` AFTER INSERT ON `tb_kon1_pen2` FOR EACH ROW BEGIN
	INSERT INTO tb_kon1_pen3 (id_karyawan) VALUES (NEW.id_karyawan);
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `before_delete_kon1_pen2` BEFORE DELETE ON `tb_kon1_pen2` FOR EACH ROW BEGIN
    DELETE FROM tb_kon1_pen3 WHERE id_karyawan=OLD.id_karyawan;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_kon1_pen3`
--

CREATE TABLE `tb_kon1_pen3` (
  `id_karyawan` int(10) NOT NULL,
  `mulai_penilaian` date NOT NULL,
  `selesai_penilaian` date NOT NULL,
  `jawaban1` int(1) NOT NULL,
  `jawaban2` int(1) NOT NULL,
  `jawaban3` int(1) NOT NULL,
  `jawaban4` int(1) NOT NULL,
  `jawaban5` int(1) NOT NULL,
  `jawaban6` int(1) NOT NULL,
  `jawaban7` int(1) NOT NULL,
  `jawaban8` int(1) NOT NULL,
  `jawaban9` int(1) NOT NULL,
  `jawaban10` int(1) NOT NULL,
  `jawaban11` int(1) NOT NULL,
  `ket1` varchar(1200) NOT NULL,
  `ket2` varchar(1200) NOT NULL,
  `ket3` varchar(1200) NOT NULL,
  `hasil` varchar(1) NOT NULL,
  `rekomendasi` varchar(70) NOT NULL,
  `tgl_penilaian` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_kon1_pen3`
--

INSERT INTO `tb_kon1_pen3` (`id_karyawan`, `mulai_penilaian`, `selesai_penilaian`, `jawaban1`, `jawaban2`, `jawaban3`, `jawaban4`, `jawaban5`, `jawaban6`, `jawaban7`, `jawaban8`, `jawaban9`, `jawaban10`, `jawaban11`, `ket1`, `ket2`, `ket3`, `hasil`, `rekomendasi`, `tgl_penilaian`) VALUES
(22, '0000-00-00', '0000-00-00', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '', '2020-07-26 18:01:20'),
(23, '0000-00-00', '0000-00-00', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '', '2020-07-26 18:13:19'),
(24, '2021-01-28', '2021-04-28', 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 'bismillah', 'semoga selesai besok aminn', '', 'A', '', '2020-07-28 17:32:10'),
(26, '0000-00-00', '0000-00-00', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '', '2020-07-29 05:41:09'),
(27, '0000-00-00', '0000-00-00', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '', '2020-07-29 05:42:35'),
(28, '0000-00-00', '0000-00-00', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '', '2020-07-29 18:15:22');

--
-- Trigger `tb_kon1_pen3`
--
DELIMITER $$
CREATE TRIGGER `after_insert_kon1_pen3` AFTER INSERT ON `tb_kon1_pen3` FOR EACH ROW BEGIN
	INSERT INTO tb_kon1_pen4 (id_karyawan) VALUES (NEW.id_karyawan);
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `before_delete_kon1_pen3` BEFORE DELETE ON `tb_kon1_pen3` FOR EACH ROW BEGIN
    DELETE FROM tb_kon1_pen4 WHERE id_karyawan=OLD.id_karyawan;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_kon1_pen4`
--

CREATE TABLE `tb_kon1_pen4` (
  `id_karyawan` int(10) NOT NULL,
  `mulai_penilaian` date NOT NULL,
  `selesai_penilaian` date NOT NULL,
  `jawaban1` int(1) NOT NULL,
  `jawaban2` int(1) NOT NULL,
  `jawaban3` int(1) NOT NULL,
  `jawaban4` int(1) NOT NULL,
  `jawaban5` int(1) NOT NULL,
  `jawaban6` int(1) NOT NULL,
  `jawaban7` int(1) NOT NULL,
  `jawaban8` int(1) NOT NULL,
  `jawaban9` int(1) NOT NULL,
  `jawaban10` int(1) NOT NULL,
  `jawaban11` int(1) NOT NULL,
  `ket1` varchar(1200) NOT NULL,
  `ket2` varchar(1200) NOT NULL,
  `ket3` varchar(1200) NOT NULL,
  `hasil` varchar(1) NOT NULL,
  `rekomendasi` varchar(70) NOT NULL,
  `tgl_penilaian` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_kon1_pen4`
--

INSERT INTO `tb_kon1_pen4` (`id_karyawan`, `mulai_penilaian`, `selesai_penilaian`, `jawaban1`, `jawaban2`, `jawaban3`, `jawaban4`, `jawaban5`, `jawaban6`, `jawaban7`, `jawaban8`, `jawaban9`, `jawaban10`, `jawaban11`, `ket1`, `ket2`, `ket3`, `hasil`, `rekomendasi`, `tgl_penilaian`) VALUES
(22, '0000-00-00', '0000-00-00', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '', '2020-07-26 18:01:20'),
(23, '0000-00-00', '0000-00-00', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '', '2020-07-26 18:13:19'),
(24, '2021-04-28', '2021-07-28', 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 'bismillahhhh', 'ok bismillah', 'iya aminnn', 'A', 'Diangkat menjadi karyawan tetap setelah habis masa kerja kontrak.', '2020-07-28 17:44:46'),
(26, '0000-00-00', '0000-00-00', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '', '2020-07-29 05:41:09'),
(27, '0000-00-00', '0000-00-00', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '', '2020-07-29 05:42:35'),
(28, '0000-00-00', '0000-00-00', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '', '2020-07-29 18:15:22');

--
-- Trigger `tb_kon1_pen4`
--
DELIMITER $$
CREATE TRIGGER `after_insert_kon1_pen4` AFTER INSERT ON `tb_kon1_pen4` FOR EACH ROW BEGIN
	INSERT INTO tb_kon2_pen1 (id_karyawan) VALUES (NEW.id_karyawan);
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `before_delete_kon1_pen4` BEFORE DELETE ON `tb_kon1_pen4` FOR EACH ROW BEGIN
    DELETE FROM tb_kon2_pen1 WHERE id_karyawan=OLD.id_karyawan;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_kon2_pen1`
--

CREATE TABLE `tb_kon2_pen1` (
  `id_karyawan` int(10) NOT NULL,
  `mulai_penilaian` date NOT NULL,
  `selesai_penilaian` date NOT NULL,
  `jawaban1` int(1) NOT NULL,
  `jawaban2` int(1) NOT NULL,
  `jawaban3` int(1) NOT NULL,
  `jawaban4` int(1) NOT NULL,
  `jawaban5` int(1) NOT NULL,
  `jawaban6` int(1) NOT NULL,
  `jawaban7` int(1) NOT NULL,
  `jawaban8` int(1) NOT NULL,
  `jawaban9` int(1) NOT NULL,
  `jawaban10` int(1) NOT NULL,
  `jawaban11` int(1) NOT NULL,
  `ket1` varchar(1200) NOT NULL,
  `ket2` varchar(1200) NOT NULL,
  `ket3` varchar(1200) NOT NULL,
  `hasil` varchar(1) NOT NULL,
  `rekomendasi` varchar(70) NOT NULL,
  `tgl_penilaian` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_kon2_pen1`
--

INSERT INTO `tb_kon2_pen1` (`id_karyawan`, `mulai_penilaian`, `selesai_penilaian`, `jawaban1`, `jawaban2`, `jawaban3`, `jawaban4`, `jawaban5`, `jawaban6`, `jawaban7`, `jawaban8`, `jawaban9`, `jawaban10`, `jawaban11`, `ket1`, `ket2`, `ket3`, `hasil`, `rekomendasi`, `tgl_penilaian`) VALUES
(22, '2021-01-27', '2021-04-27', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '', '2020-07-30 07:19:11'),
(23, '0000-00-00', '0000-00-00', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '', '2020-07-26 18:13:19'),
(24, '2021-07-28', '2021-10-28', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '', '2020-07-28 18:19:22'),
(26, '0000-00-00', '0000-00-00', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '', '2020-07-29 05:41:09'),
(27, '0000-00-00', '0000-00-00', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '', '2020-07-29 05:42:35'),
(28, '0000-00-00', '0000-00-00', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '', '2020-07-29 18:15:22');

--
-- Trigger `tb_kon2_pen1`
--
DELIMITER $$
CREATE TRIGGER `after_insert_kon2_pen1` AFTER INSERT ON `tb_kon2_pen1` FOR EACH ROW BEGIN
	INSERT INTO tb_kon2_pen2 (id_karyawan) VALUES (NEW.id_karyawan);
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `before_delete_kon2_pen1` BEFORE DELETE ON `tb_kon2_pen1` FOR EACH ROW BEGIN
    DELETE FROM tb_kon2_pen2 WHERE id_karyawan=OLD.id_karyawan;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_kon2_pen2`
--

CREATE TABLE `tb_kon2_pen2` (
  `id_karyawan` int(10) NOT NULL,
  `mulai_penilaian` date NOT NULL,
  `selesai_penilaian` date NOT NULL,
  `jawaban1` int(1) NOT NULL,
  `jawaban2` int(1) NOT NULL,
  `jawaban3` int(1) NOT NULL,
  `jawaban4` int(1) NOT NULL,
  `jawaban5` int(1) NOT NULL,
  `jawaban6` int(1) NOT NULL,
  `jawaban7` int(1) NOT NULL,
  `jawaban8` int(1) NOT NULL,
  `jawaban9` int(1) NOT NULL,
  `jawaban10` int(1) NOT NULL,
  `jawaban11` int(1) NOT NULL,
  `ket1` varchar(1200) NOT NULL,
  `ket2` varchar(1200) NOT NULL,
  `ket3` varchar(1200) NOT NULL,
  `hasil` varchar(1) NOT NULL,
  `rekomendasi` varchar(70) NOT NULL,
  `tgl_penilaian` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_kon2_pen2`
--

INSERT INTO `tb_kon2_pen2` (`id_karyawan`, `mulai_penilaian`, `selesai_penilaian`, `jawaban1`, `jawaban2`, `jawaban3`, `jawaban4`, `jawaban5`, `jawaban6`, `jawaban7`, `jawaban8`, `jawaban9`, `jawaban10`, `jawaban11`, `ket1`, `ket2`, `ket3`, `hasil`, `rekomendasi`, `tgl_penilaian`) VALUES
(22, '2021-04-27', '2021-07-27', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '', '2020-07-30 07:19:11'),
(23, '0000-00-00', '0000-00-00', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '', '2020-07-26 18:13:19'),
(24, '2021-10-28', '2021-01-28', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '', '2020-07-28 18:19:22'),
(26, '0000-00-00', '0000-00-00', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '', '2020-07-29 05:41:09'),
(27, '0000-00-00', '0000-00-00', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '', '2020-07-29 05:42:35'),
(28, '0000-00-00', '0000-00-00', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '', '2020-07-29 18:15:22');

--
-- Trigger `tb_kon2_pen2`
--
DELIMITER $$
CREATE TRIGGER `after_insert_kon2_pen2` AFTER INSERT ON `tb_kon2_pen2` FOR EACH ROW BEGIN
	INSERT INTO tb_kon2_pen3 (id_karyawan) VALUES (NEW.id_karyawan);
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `before_delete_kon2_pen2` BEFORE DELETE ON `tb_kon2_pen2` FOR EACH ROW BEGIN
    DELETE FROM tb_kon2_pen3 WHERE id_karyawan=OLD.id_karyawan;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_kon2_pen3`
--

CREATE TABLE `tb_kon2_pen3` (
  `id_karyawan` int(10) NOT NULL,
  `mulai_penilaian` date NOT NULL,
  `selesai_penilaian` date NOT NULL,
  `jawaban1` int(1) NOT NULL,
  `jawaban2` int(1) NOT NULL,
  `jawaban3` int(1) NOT NULL,
  `jawaban4` int(1) NOT NULL,
  `jawaban5` int(1) NOT NULL,
  `jawaban6` int(1) NOT NULL,
  `jawaban7` int(1) NOT NULL,
  `jawaban8` int(1) NOT NULL,
  `jawaban9` int(1) NOT NULL,
  `jawaban10` int(1) NOT NULL,
  `jawaban11` int(1) NOT NULL,
  `ket1` varchar(1200) NOT NULL,
  `ket2` varchar(1200) NOT NULL,
  `ket3` varchar(1200) NOT NULL,
  `hasil` varchar(1) NOT NULL,
  `rekomendasi` varchar(70) NOT NULL,
  `tgl_penilaian` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_kon2_pen3`
--

INSERT INTO `tb_kon2_pen3` (`id_karyawan`, `mulai_penilaian`, `selesai_penilaian`, `jawaban1`, `jawaban2`, `jawaban3`, `jawaban4`, `jawaban5`, `jawaban6`, `jawaban7`, `jawaban8`, `jawaban9`, `jawaban10`, `jawaban11`, `ket1`, `ket2`, `ket3`, `hasil`, `rekomendasi`, `tgl_penilaian`) VALUES
(22, '0000-00-00', '0000-00-00', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '', '2020-07-26 18:01:20'),
(23, '0000-00-00', '0000-00-00', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '', '2020-07-26 18:13:19'),
(24, '0000-00-00', '0000-00-00', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '', '2020-07-28 13:11:28'),
(26, '0000-00-00', '0000-00-00', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '', '2020-07-29 05:41:09'),
(27, '0000-00-00', '0000-00-00', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '', '2020-07-29 05:42:35'),
(28, '0000-00-00', '0000-00-00', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '', '2020-07-29 18:15:22');

--
-- Trigger `tb_kon2_pen3`
--
DELIMITER $$
CREATE TRIGGER `after_insert_kon2_pen3` AFTER INSERT ON `tb_kon2_pen3` FOR EACH ROW BEGIN
	INSERT INTO tb_kon2_pen4 (id_karyawan) VALUES (NEW.id_karyawan);
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `before_delete_kon2_pen3` BEFORE DELETE ON `tb_kon2_pen3` FOR EACH ROW BEGIN
    DELETE FROM tb_kon2_pen4 WHERE id_karyawan=OLD.id_karyawan;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_kon2_pen4`
--

CREATE TABLE `tb_kon2_pen4` (
  `id_karyawan` int(10) NOT NULL,
  `mulai_penilaian` date NOT NULL,
  `selesai_penilaian` date NOT NULL,
  `jawaban1` int(1) NOT NULL,
  `jawaban2` int(1) NOT NULL,
  `jawaban3` int(1) NOT NULL,
  `jawaban4` int(1) NOT NULL,
  `jawaban5` int(1) NOT NULL,
  `jawaban6` int(1) NOT NULL,
  `jawaban7` int(1) NOT NULL,
  `jawaban8` int(1) NOT NULL,
  `jawaban9` int(1) NOT NULL,
  `jawaban10` int(1) NOT NULL,
  `jawaban11` int(1) NOT NULL,
  `ket1` varchar(1200) NOT NULL,
  `ket2` varchar(1200) NOT NULL,
  `ket3` varchar(1200) NOT NULL,
  `hasil` varchar(1) NOT NULL,
  `rekomendasi` varchar(70) NOT NULL,
  `tgl_penilaian` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_kon2_pen4`
--

INSERT INTO `tb_kon2_pen4` (`id_karyawan`, `mulai_penilaian`, `selesai_penilaian`, `jawaban1`, `jawaban2`, `jawaban3`, `jawaban4`, `jawaban5`, `jawaban6`, `jawaban7`, `jawaban8`, `jawaban9`, `jawaban10`, `jawaban11`, `ket1`, `ket2`, `ket3`, `hasil`, `rekomendasi`, `tgl_penilaian`) VALUES
(22, '0000-00-00', '0000-00-00', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '', '2020-07-26 18:01:20'),
(23, '0000-00-00', '0000-00-00', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '', '2020-07-26 18:13:19'),
(24, '0000-00-00', '0000-00-00', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '', '2020-07-28 13:11:28'),
(26, '0000-00-00', '0000-00-00', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '', '2020-07-29 05:41:09'),
(27, '0000-00-00', '0000-00-00', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '', '2020-07-29 05:42:35'),
(28, '0000-00-00', '0000-00-00', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '', '2020-07-29 18:15:22');

--
-- Trigger `tb_kon2_pen4`
--
DELIMITER $$
CREATE TRIGGER `after_insert_kon2_pen4` AFTER INSERT ON `tb_kon2_pen4` FOR EACH ROW BEGIN
	INSERT INTO tb_kon3_pen1 (id_karyawan) VALUES (NEW.id_karyawan);
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `before_delete_kon2_pen4` BEFORE DELETE ON `tb_kon2_pen4` FOR EACH ROW BEGIN
    DELETE FROM tb_kon3_pen1 WHERE id_karyawan=OLD.id_karyawan;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_kon3_pen1`
--

CREATE TABLE `tb_kon3_pen1` (
  `id_karyawan` int(10) NOT NULL,
  `mulai_penilaian` date NOT NULL,
  `selesai_penilaian` date NOT NULL,
  `jawaban1` int(1) NOT NULL,
  `jawaban2` int(1) NOT NULL,
  `jawaban3` int(1) NOT NULL,
  `jawaban4` int(1) NOT NULL,
  `jawaban5` int(1) NOT NULL,
  `jawaban6` int(1) NOT NULL,
  `jawaban7` int(1) NOT NULL,
  `jawaban8` int(1) NOT NULL,
  `jawaban9` int(1) NOT NULL,
  `jawaban10` int(1) NOT NULL,
  `jawaban11` int(1) NOT NULL,
  `ket1` varchar(1200) NOT NULL,
  `ket2` varchar(1200) NOT NULL,
  `ket3` varchar(1200) NOT NULL,
  `hasil` varchar(1) NOT NULL,
  `rekomendasi` varchar(70) NOT NULL,
  `tgl_penilaian` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_kon3_pen1`
--

INSERT INTO `tb_kon3_pen1` (`id_karyawan`, `mulai_penilaian`, `selesai_penilaian`, `jawaban1`, `jawaban2`, `jawaban3`, `jawaban4`, `jawaban5`, `jawaban6`, `jawaban7`, `jawaban8`, `jawaban9`, `jawaban10`, `jawaban11`, `ket1`, `ket2`, `ket3`, `hasil`, `rekomendasi`, `tgl_penilaian`) VALUES
(22, '0000-00-00', '0000-00-00', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '', '2020-07-26 18:01:20'),
(23, '0000-00-00', '0000-00-00', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '', '2020-07-26 18:13:19'),
(24, '0000-00-00', '0000-00-00', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '', '2020-07-28 13:11:28'),
(26, '0000-00-00', '0000-00-00', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '', '2020-07-29 05:41:09'),
(27, '0000-00-00', '0000-00-00', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '', '2020-07-29 05:42:35'),
(28, '0000-00-00', '0000-00-00', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '', '2020-07-29 18:15:22');

--
-- Trigger `tb_kon3_pen1`
--
DELIMITER $$
CREATE TRIGGER `after_insert_kon3_pen1` AFTER INSERT ON `tb_kon3_pen1` FOR EACH ROW BEGIN
	INSERT INTO tb_kon3_pen2 (id_karyawan) VALUES (NEW.id_karyawan);
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `before_delete_kon3_pen1` BEFORE DELETE ON `tb_kon3_pen1` FOR EACH ROW BEGIN
    DELETE FROM tb_kon3_pen2 WHERE id_karyawan=OLD.id_karyawan;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_kon3_pen2`
--

CREATE TABLE `tb_kon3_pen2` (
  `id_karyawan` int(10) NOT NULL,
  `mulai_penilaian` date NOT NULL,
  `selesai_penilaian` date NOT NULL,
  `jawaban1` int(1) NOT NULL,
  `jawaban2` int(1) NOT NULL,
  `jawaban3` int(1) NOT NULL,
  `jawaban4` int(1) NOT NULL,
  `jawaban5` int(1) NOT NULL,
  `jawaban6` int(1) NOT NULL,
  `jawaban7` int(1) NOT NULL,
  `jawaban8` int(1) NOT NULL,
  `jawaban9` int(1) NOT NULL,
  `jawaban10` int(1) NOT NULL,
  `jawaban11` int(1) NOT NULL,
  `ket1` varchar(1200) NOT NULL,
  `ket2` varchar(1200) NOT NULL,
  `ket3` varchar(1200) NOT NULL,
  `hasil` varchar(1) NOT NULL,
  `rekomendasi` varchar(70) NOT NULL,
  `tgl_penilaian` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_kon3_pen2`
--

INSERT INTO `tb_kon3_pen2` (`id_karyawan`, `mulai_penilaian`, `selesai_penilaian`, `jawaban1`, `jawaban2`, `jawaban3`, `jawaban4`, `jawaban5`, `jawaban6`, `jawaban7`, `jawaban8`, `jawaban9`, `jawaban10`, `jawaban11`, `ket1`, `ket2`, `ket3`, `hasil`, `rekomendasi`, `tgl_penilaian`) VALUES
(22, '0000-00-00', '0000-00-00', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '', '2020-07-26 18:01:20'),
(23, '0000-00-00', '0000-00-00', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '', '2020-07-26 18:13:19'),
(24, '0000-00-00', '0000-00-00', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '', '2020-07-28 13:11:28'),
(26, '0000-00-00', '0000-00-00', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '', '2020-07-29 05:41:09'),
(27, '0000-00-00', '0000-00-00', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '', '2020-07-29 05:42:35'),
(28, '0000-00-00', '0000-00-00', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '', '2020-07-29 18:15:22');

--
-- Trigger `tb_kon3_pen2`
--
DELIMITER $$
CREATE TRIGGER `after_insert_kon3_pen2` AFTER INSERT ON `tb_kon3_pen2` FOR EACH ROW BEGIN
	INSERT INTO tb_kon3_pen3 (id_karyawan) VALUES (NEW.id_karyawan);
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `before_delete_kon3_pen2` BEFORE DELETE ON `tb_kon3_pen2` FOR EACH ROW BEGIN
    DELETE FROM tb_kon3_pen3 WHERE id_karyawan=OLD.id_karyawan;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_kon3_pen3`
--

CREATE TABLE `tb_kon3_pen3` (
  `id_karyawan` int(10) NOT NULL,
  `mulai_penilaian` date NOT NULL,
  `selesai_penilaian` date NOT NULL,
  `jawaban1` int(1) NOT NULL,
  `jawaban2` int(1) NOT NULL,
  `jawaban3` int(1) NOT NULL,
  `jawaban4` int(1) NOT NULL,
  `jawaban5` int(1) NOT NULL,
  `jawaban6` int(1) NOT NULL,
  `jawaban7` int(1) NOT NULL,
  `jawaban8` int(1) NOT NULL,
  `jawaban9` int(1) NOT NULL,
  `jawaban10` int(1) NOT NULL,
  `jawaban11` int(1) NOT NULL,
  `ket1` varchar(1200) NOT NULL,
  `ket2` varchar(1200) NOT NULL,
  `ket3` varchar(1200) NOT NULL,
  `hasil` varchar(1) NOT NULL,
  `rekomendasi` varchar(70) NOT NULL,
  `tgl_penilaian` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_kon3_pen3`
--

INSERT INTO `tb_kon3_pen3` (`id_karyawan`, `mulai_penilaian`, `selesai_penilaian`, `jawaban1`, `jawaban2`, `jawaban3`, `jawaban4`, `jawaban5`, `jawaban6`, `jawaban7`, `jawaban8`, `jawaban9`, `jawaban10`, `jawaban11`, `ket1`, `ket2`, `ket3`, `hasil`, `rekomendasi`, `tgl_penilaian`) VALUES
(22, '0000-00-00', '0000-00-00', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '', '2020-07-26 18:01:20'),
(23, '0000-00-00', '0000-00-00', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '', '2020-07-26 18:13:19'),
(24, '0000-00-00', '0000-00-00', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '', '2020-07-28 13:11:28'),
(26, '0000-00-00', '0000-00-00', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '', '2020-07-29 05:41:09'),
(27, '0000-00-00', '0000-00-00', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '', '2020-07-29 05:42:35'),
(28, '0000-00-00', '0000-00-00', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '', '2020-07-29 18:15:22');

--
-- Trigger `tb_kon3_pen3`
--
DELIMITER $$
CREATE TRIGGER `after_insert_kon3_pen3` AFTER INSERT ON `tb_kon3_pen3` FOR EACH ROW BEGIN
	INSERT INTO tb_kon3_pen4 (id_karyawan) VALUES (NEW.id_karyawan);
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `before_delete_kon3_pen3` BEFORE DELETE ON `tb_kon3_pen3` FOR EACH ROW BEGIN
    DELETE FROM tb_kon3_pen4 WHERE id_karyawan=OLD.id_karyawan;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_kon3_pen4`
--

CREATE TABLE `tb_kon3_pen4` (
  `id_karyawan` int(10) NOT NULL,
  `mulai_penilaian` date NOT NULL,
  `selesai_penilaian` date NOT NULL,
  `jawaban1` int(1) NOT NULL,
  `jawaban2` int(1) NOT NULL,
  `jawaban3` int(1) NOT NULL,
  `jawaban4` int(1) NOT NULL,
  `jawaban5` int(1) NOT NULL,
  `jawaban6` int(1) NOT NULL,
  `jawaban7` int(1) NOT NULL,
  `jawaban8` int(1) NOT NULL,
  `jawaban9` int(1) NOT NULL,
  `jawaban10` int(1) NOT NULL,
  `jawaban11` int(1) NOT NULL,
  `ket1` varchar(1200) NOT NULL,
  `ket2` varchar(1200) NOT NULL,
  `ket3` varchar(1200) NOT NULL,
  `hasil` varchar(1) NOT NULL,
  `rekomendasi` varchar(70) NOT NULL,
  `tgl_penilaian` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_kon3_pen4`
--

INSERT INTO `tb_kon3_pen4` (`id_karyawan`, `mulai_penilaian`, `selesai_penilaian`, `jawaban1`, `jawaban2`, `jawaban3`, `jawaban4`, `jawaban5`, `jawaban6`, `jawaban7`, `jawaban8`, `jawaban9`, `jawaban10`, `jawaban11`, `ket1`, `ket2`, `ket3`, `hasil`, `rekomendasi`, `tgl_penilaian`) VALUES
(22, '0000-00-00', '0000-00-00', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '', '2020-07-26 18:01:20'),
(23, '0000-00-00', '0000-00-00', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '', '2020-07-26 18:13:19'),
(24, '0000-00-00', '0000-00-00', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '', '2020-07-28 13:11:28'),
(26, '0000-00-00', '0000-00-00', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '', '2020-07-29 05:41:09'),
(27, '0000-00-00', '0000-00-00', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '', '2020-07-29 05:42:35'),
(28, '0000-00-00', '0000-00-00', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '', '2020-07-29 18:15:22');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_kontrak1`
--

CREATE TABLE `tb_kontrak1` (
  `id_karyawan` int(10) NOT NULL,
  `status_penilaian` int(1) NOT NULL,
  `banyak_penilaian` int(1) NOT NULL,
  `id_proses` int(1) NOT NULL,
  `mulai_kontrak` date NOT NULL,
  `selesai_kontrak` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_kontrak1`
--

INSERT INTO `tb_kontrak1` (`id_karyawan`, `status_penilaian`, `banyak_penilaian`, `id_proses`, `mulai_kontrak`, `selesai_kontrak`) VALUES
(22, 2, 2, 0, '2020-07-27', '2021-01-27'),
(23, 2, 2, 0, '2020-07-27', '2021-07-27'),
(24, 4, 4, 1, '2020-07-28', '2021-07-28'),
(26, 2, 4, 0, '2020-07-29', '2021-07-29'),
(27, 0, 0, 1, '2020-07-29', '2021-07-29'),
(28, 0, 0, 1, '2020-07-30', '2021-07-30');

--
-- Trigger `tb_kontrak1`
--
DELIMITER $$
CREATE TRIGGER `after_insert_kontrak1` AFTER INSERT ON `tb_kontrak1` FOR EACH ROW BEGIN
	INSERT INTO tb_kontrak2 (id_karyawan,id_proses) VALUES (NEW.id_karyawan,1);
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `before_delete_kontrak1` BEFORE DELETE ON `tb_kontrak1` FOR EACH ROW BEGIN
    DELETE FROM tb_kontrak2 WHERE id_karyawan=OLD.id_karyawan;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_kontrak2`
--

CREATE TABLE `tb_kontrak2` (
  `id_karyawan` int(10) NOT NULL,
  `status_penilaian` int(1) NOT NULL,
  `banyak_penilaian` int(1) NOT NULL,
  `id_proses` int(1) NOT NULL,
  `mulai_kontrak` date NOT NULL,
  `selesai_kontrak` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_kontrak2`
--

INSERT INTO `tb_kontrak2` (`id_karyawan`, `status_penilaian`, `banyak_penilaian`, `id_proses`, `mulai_kontrak`, `selesai_kontrak`) VALUES
(22, 2, 2, 1, '2021-01-27', '2022-01-27'),
(23, 0, 0, 1, '0000-00-00', '0000-00-00'),
(24, 2, 2, 1, '2021-07-28', '2022-01-28'),
(26, 0, 0, 1, '0000-00-00', '0000-00-00'),
(27, 0, 0, 1, '0000-00-00', '0000-00-00'),
(28, 0, 0, 1, '0000-00-00', '0000-00-00');

--
-- Trigger `tb_kontrak2`
--
DELIMITER $$
CREATE TRIGGER `after_insert_kontrak2` AFTER INSERT ON `tb_kontrak2` FOR EACH ROW BEGIN
	INSERT INTO tb_kontrak3 (id_karyawan,id_proses) VALUES (NEW.id_karyawan,1);
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `before_delete_kontrak2` BEFORE DELETE ON `tb_kontrak2` FOR EACH ROW BEGIN
    DELETE FROM tb_kontrak3 WHERE id_karyawan=OLD.id_karyawan;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_kontrak3`
--

CREATE TABLE `tb_kontrak3` (
  `id_karyawan` int(10) NOT NULL,
  `status_penilaian` int(1) NOT NULL,
  `banyak_penilaian` int(1) NOT NULL,
  `id_proses` int(11) NOT NULL,
  `mulai_kontrak` date NOT NULL,
  `selesai_kontrak` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_kontrak3`
--

INSERT INTO `tb_kontrak3` (`id_karyawan`, `status_penilaian`, `banyak_penilaian`, `id_proses`, `mulai_kontrak`, `selesai_kontrak`) VALUES
(22, 0, 0, 1, '0000-00-00', '0000-00-00'),
(23, 0, 0, 1, '0000-00-00', '0000-00-00'),
(24, 0, 0, 1, '0000-00-00', '0000-00-00'),
(26, 0, 0, 1, '0000-00-00', '0000-00-00'),
(27, 0, 0, 1, '0000-00-00', '0000-00-00'),
(28, 0, 0, 1, '0000-00-00', '0000-00-00');

--
-- Trigger `tb_kontrak3`
--
DELIMITER $$
CREATE TRIGGER `after_insert_kontrak3` AFTER INSERT ON `tb_kontrak3` FOR EACH ROW BEGIN
	INSERT INTO tb_kon1_pen1 (id_karyawan) VALUES (NEW.id_karyawan);
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `before_delete_kontrak3` BEFORE DELETE ON `tb_kontrak3` FOR EACH ROW BEGIN
    DELETE FROM tb_kon1_pen1 WHERE id_karyawan=OLD.id_karyawan;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_user`
--

CREATE TABLE `tb_user` (
  `id` int(10) NOT NULL,
  `email` varchar(60) NOT NULL,
  `login_status` int(1) NOT NULL,
  `password` varchar(300) NOT NULL,
  `nama` varchar(60) NOT NULL,
  `no_hp` varchar(13) NOT NULL,
  `tgl_lahir` date NOT NULL,
  `departemen` varchar(5) NOT NULL,
  `posisi` varchar(50) NOT NULL,
  `jenis_kelamin` varchar(6) NOT NULL,
  `foto` varchar(300) NOT NULL,
  `tgl_daftar` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `trn_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_user`
--

INSERT INTO `tb_user` (`id`, `email`, `login_status`, `password`, `nama`, `no_hp`, `tgl_lahir`, `departemen`, `posisi`, `jenis_kelamin`, `foto`, `tgl_daftar`, `trn_date`) VALUES
(1, '', 0, '', '', '', '0000-00-00', '', '', '', '', '2020-07-30 06:51:40', '0000-00-00 00:00:00'),
(2, 'user@gmail.com', 2, '200820e3227815ed1756a6b531e7e0d2', 'User', '08999888877', '2002-02-02', 'HC', 'Human Capital', 'Female', '20200726120359Small Malious.png', '2020-07-26 10:03:59', '0000-00-00 00:00:00'),
(3, 'admin2@gmail.com', 1, '200820e3227815ed1756a6b531e7e0d2', 'Admin2', '08979998888', '1995-05-05', 'HCM', 'Human Capital Ciputra', 'Male', 'Save Earth.jpg', '2020-07-18 12:31:22', '0000-00-00 00:00:00'),
(4, 'dahlia@ciputra.com', 1, '200820e3227815ed1756a6b531e7e0d2', 'Dahlia', '08977778888', '1993-03-03', 'HCGA', 'HC OFFICER', 'Female', '', '2020-07-18 14:47:55', '0000-00-00 00:00:00'),
(5, 'admin@gmail.com', 1, '46f94c8de14fb36680850768ff1b7f2a', 'Admin', '08979100048', '2000-12-19', 'CONS', 'HC OFFICER', 'Female', '20200718212140pas foto.jpg', '2020-07-30 06:51:33', '0000-00-00 00:00:00'),
(11, 'hendrik@ciputra.com', 2, '200820e3227815ed1756a6b531e7e0d2', 'Hendrik', '08777889990', '1995-05-05', 'EM', 'DIRECTOR', 'Male', '20200726122553instagram.png', '2020-07-26 10:25:53', '0000-00-00 00:00:00'),
(12, 'abdul@ciputra.com', 2, '200820e3227815ed1756a6b531e7e0d2', 'Abdul', '08777666888', '1992-02-02', 'LND', 'ESTATE MANAGEMENT DEPUTY MANAGER', 'Male', '', '2020-07-19 09:06:45', '0000-00-00 00:00:00'),
(13, 'maryulianti26@gmail.com', 2, '200820e3227815ed1756a6b531e7e0d2', 'Ria Yulia', '081315708221', '1996-07-22', 'FA', 'ASSISTANT ACCOUNTING MANAGER', 'Female', '', '2020-08-04 16:52:25', '0000-00-00 00:00:00'),
(14, 'agunggumelar1912@gmail.com', 2, '200820e3227815ed1756a6b531e7e0d2', 'Agung Gumelar', '08979100048', '1997-12-19', 'CONS', 'WEB DEVELOPER', 'Male', '', '2020-08-06 16:41:49', '2020-08-06 18:41:06');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `password_reset_temp`
--
ALTER TABLE `password_reset_temp`
  ADD UNIQUE KEY `expDate` (`expDate`);

--
-- Indeks untuk tabel `tb_karyawan`
--
ALTER TABLE `tb_karyawan`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `nik` (`nik`);

--
-- Indeks untuk tabel `tb_kon1_pen1`
--
ALTER TABLE `tb_kon1_pen1`
  ADD PRIMARY KEY (`id_karyawan`);

--
-- Indeks untuk tabel `tb_kon1_pen2`
--
ALTER TABLE `tb_kon1_pen2`
  ADD PRIMARY KEY (`id_karyawan`);

--
-- Indeks untuk tabel `tb_kon1_pen3`
--
ALTER TABLE `tb_kon1_pen3`
  ADD PRIMARY KEY (`id_karyawan`);

--
-- Indeks untuk tabel `tb_kon1_pen4`
--
ALTER TABLE `tb_kon1_pen4`
  ADD PRIMARY KEY (`id_karyawan`);

--
-- Indeks untuk tabel `tb_kon2_pen1`
--
ALTER TABLE `tb_kon2_pen1`
  ADD PRIMARY KEY (`id_karyawan`);

--
-- Indeks untuk tabel `tb_kon2_pen2`
--
ALTER TABLE `tb_kon2_pen2`
  ADD PRIMARY KEY (`id_karyawan`);

--
-- Indeks untuk tabel `tb_kon2_pen3`
--
ALTER TABLE `tb_kon2_pen3`
  ADD PRIMARY KEY (`id_karyawan`);

--
-- Indeks untuk tabel `tb_kon2_pen4`
--
ALTER TABLE `tb_kon2_pen4`
  ADD PRIMARY KEY (`id_karyawan`);

--
-- Indeks untuk tabel `tb_kon3_pen1`
--
ALTER TABLE `tb_kon3_pen1`
  ADD PRIMARY KEY (`id_karyawan`);

--
-- Indeks untuk tabel `tb_kon3_pen2`
--
ALTER TABLE `tb_kon3_pen2`
  ADD PRIMARY KEY (`id_karyawan`);

--
-- Indeks untuk tabel `tb_kon3_pen3`
--
ALTER TABLE `tb_kon3_pen3`
  ADD PRIMARY KEY (`id_karyawan`);

--
-- Indeks untuk tabel `tb_kon3_pen4`
--
ALTER TABLE `tb_kon3_pen4`
  ADD PRIMARY KEY (`id_karyawan`);

--
-- Indeks untuk tabel `tb_kontrak1`
--
ALTER TABLE `tb_kontrak1`
  ADD PRIMARY KEY (`id_karyawan`);

--
-- Indeks untuk tabel `tb_kontrak2`
--
ALTER TABLE `tb_kontrak2`
  ADD PRIMARY KEY (`id_karyawan`);

--
-- Indeks untuk tabel `tb_kontrak3`
--
ALTER TABLE `tb_kontrak3`
  ADD PRIMARY KEY (`id_karyawan`);

--
-- Indeks untuk tabel `tb_user`
--
ALTER TABLE `tb_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `tb_karyawan`
--
ALTER TABLE `tb_karyawan`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT untuk tabel `tb_kon1_pen1`
--
ALTER TABLE `tb_kon1_pen1`
  MODIFY `id_karyawan` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT untuk tabel `tb_kon1_pen2`
--
ALTER TABLE `tb_kon1_pen2`
  MODIFY `id_karyawan` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT untuk tabel `tb_kon1_pen3`
--
ALTER TABLE `tb_kon1_pen3`
  MODIFY `id_karyawan` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT untuk tabel `tb_kon1_pen4`
--
ALTER TABLE `tb_kon1_pen4`
  MODIFY `id_karyawan` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT untuk tabel `tb_kon2_pen1`
--
ALTER TABLE `tb_kon2_pen1`
  MODIFY `id_karyawan` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT untuk tabel `tb_kon2_pen2`
--
ALTER TABLE `tb_kon2_pen2`
  MODIFY `id_karyawan` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT untuk tabel `tb_kon2_pen3`
--
ALTER TABLE `tb_kon2_pen3`
  MODIFY `id_karyawan` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT untuk tabel `tb_kon2_pen4`
--
ALTER TABLE `tb_kon2_pen4`
  MODIFY `id_karyawan` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT untuk tabel `tb_kon3_pen1`
--
ALTER TABLE `tb_kon3_pen1`
  MODIFY `id_karyawan` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT untuk tabel `tb_kon3_pen2`
--
ALTER TABLE `tb_kon3_pen2`
  MODIFY `id_karyawan` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT untuk tabel `tb_kon3_pen3`
--
ALTER TABLE `tb_kon3_pen3`
  MODIFY `id_karyawan` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT untuk tabel `tb_kon3_pen4`
--
ALTER TABLE `tb_kon3_pen4`
  MODIFY `id_karyawan` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT untuk tabel `tb_kontrak1`
--
ALTER TABLE `tb_kontrak1`
  MODIFY `id_karyawan` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT untuk tabel `tb_kontrak2`
--
ALTER TABLE `tb_kontrak2`
  MODIFY `id_karyawan` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT untuk tabel `tb_kontrak3`
--
ALTER TABLE `tb_kontrak3`
  MODIFY `id_karyawan` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT untuk tabel `tb_user`
--
ALTER TABLE `tb_user`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
