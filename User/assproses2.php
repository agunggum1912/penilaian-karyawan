<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <link rel="shortcut icon" type="image/x-icon" href="../gambar/logociputra.svg">
  <title>Ciputra</title>
  <!-- css manual -->
  <link rel="stylesheet" type="text/css" href="../assets/css/style.css">

  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="../plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Tempusdominus Bbootstrap 4 -->
  <link rel="stylesheet" href="../plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="../plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- JQVMap -->
  <link rel="stylesheet" href="../plugins/jqvmap/jqvmap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../dist/css/adminlte.min.css">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="../plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="../plugins/daterangepicker/daterangepicker.css">
  <!-- summernote -->
  <link rel="stylesheet" href="../plugins/summernote/summernote-bs4.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

  <style type="text/css">
    .red {
      color: red;
    }
  </style>

  <?php
  include '../koneksi.php';

  // mengaktifkan session
  session_start();
  if (!isset($_SESSION['userlogin'])) {
  // if($_SESSION['status'] != "login") {
    echo '<script language="javascript">alert("Dilarang akses, login sebagai user terlebih dahulu"); location.href="../logout.php"</script>';
  }

  $sql = "SELECT id, nama, email, foto FROM tb_user WHERE email='$_SESSION[userlogin]'";
  $qry = mysqli_query($koneksi, $sql) or die ("Query user salah!");
  $row = mysqli_fetch_array($qry);
  $iduser = $row['id'];

  if (isset($_GET['id'])) {
    $id = $_GET['id'];

    $data = explode(".", $id);

    $data1 = $data[0];
    $data2 = $data[1];
    $data3 = $data[2];

    $sql2 = "SELECT a.id AS id2, a.nama_karyawan, a.nik, a.id_user1, b.id, b.email, b.nama, c.id_karyawan, c.status_penilaian AS sp1, c.banyak_penilaian AS bp1, c.id_proses, d.id_karyawan, d.status_penilaian AS sp2, d.banyak_penilaian AS bp2, d.id_proses, e.id_karyawan, e.status_penilaian AS sp3, e.banyak_penilaian AS bp3, e.id_proses, f.id_karyawan, f.hasil AS hk1p1, g.id_karyawan, g.hasil AS hk1p2, h.id_karyawan, h.hasil AS hk1p3, i.id_karyawan, i.hasil AS hk1p4, j.id_karyawan, j.hasil AS hk2p1, k.id_karyawan, k.hasil AS hk2p2, l.id_karyawan, l.hasil AS hk2p3, m.id_karyawan, m.hasil AS hk2p4, n.id_karyawan, n.hasil AS hk3p1, o.id_karyawan, o.hasil AS hk3p2, p.id_karyawan, p.hasil AS hk3p3, q.id_karyawan, q.hasil AS hk3p4 FROM tb_karyawan AS a INNER JOIN tb_user AS b ON a.id_user1=b.id INNER JOIN tb_kontrak1 AS c ON a.id=c.id_karyawan INNER JOIN tb_kontrak2 AS d ON a.id=d.id_karyawan INNER JOIN tb_kontrak3 AS e ON a.id=e.id_karyawan INNER JOIN tb_kon1_pen1 AS f ON a.id=f.id_karyawan INNER JOIN tb_kon1_pen2 AS g ON a.id=g.id_karyawan INNER JOIN tb_kon1_pen3 AS h ON a.id=h.id_karyawan INNER JOIN tb_kon1_pen4 AS i ON a.id=i.id_karyawan INNER JOIN tb_kon2_pen1 AS j ON a.id=j.id_karyawan INNER JOIN tb_kon2_pen2 AS k ON a.id=k.id_karyawan INNER JOIN tb_kon2_pen3 AS l ON a.id=l.id_karyawan INNER JOIN tb_kon2_pen4 AS m ON a.id=m.id_karyawan INNER JOIN tb_kon3_pen1 AS n ON a.id=n.id_karyawan INNER JOIN tb_kon3_pen2 AS o ON a.id=o.id_karyawan INNER JOIN tb_kon3_pen3 AS p ON a.id=p.id_karyawan INNER JOIN tb_kon3_pen4 AS q ON a.id=q.id_karyawan WHERE a.id=$data1";

    $qry2 = mysqli_query($koneksi, $sql2) or die ("Query karyawan salah!");
    $row2 = mysqli_fetch_array($qry2);

    if ($row2['sp1'] == 1 && $row2['bp1'] == 1 && $row2['hk1p1'] == "") {
      echo "<script>alert('Saat ini $row2[nama_karyawan] telah proses penilaian terakhir pada Contract Pertama');</script>";
    }elseif($row2['sp1'] == 2 && $row2['bp1'] == 2 && $row2['hk1p2'] == ""){
      if ($row2['sp1'] == 2 && $row2['bp1'] == 2 && $row2['hk1p1'] == "") {
        echo "<script>alert('Upss! Saat ini halaman tidak tersedia!');history.go(-1)</script>";  
      }else{
        echo "<script>alert('Saat ini $row2[nama_karyawan] telah proses penilaian terakhir pada Contract Pertama');</script>";
      }
    }elseif ($row2['sp1'] == 3 && $row2['bp1'] == 3 && $row2['hk1p3'] == "") {
      if ($row2['sp1'] == 3 && $row2['bp1'] == 3 && $row2['hk1p2'] == "") {
        echo "<script>alert('Upss! Saat ini halaman tidak tersedia!');history.go(-1)</script>";  
      }else{
        echo "<script>alert('Saat ini $row2[nama_karyawan] telah proses penilaian terakhir pada Contract Pertama');</script>";
      }
    }elseif ($row2['sp1'] == 4 && $row2['bp1'] == 4 && $row2['hk1p4'] == "") {
      if ($row2['sp1'] == 4 && $row2['bp1'] == 4 && $row2['hk1p3'] == "") {
        echo "<script>alert('Upss! Saat ini halaman tidak tersedia!');history.go(-1)</script>";  
      }else{
        echo "<script>alert('Saat ini $row2[nama_karyawan] telah proses penilaian terakhir pada Contract Pertama');</script>";
      }
    }elseif ($row2['sp2'] == 1 && $row2['bp2'] == 1 && $row2['hk2p1'] == "") {
      echo "<script>alert('Saat ini $row2[nama_karyawan] telah proses penilaian terakhir pada Contract Kedua');</script>";
    }elseif ($row2['sp2'] == 2 && $row2['bp2'] == 2 && $row2['hk2p2'] == "") {
      if ($row2['sp2'] == 2 && $row2['bp2'] == 2 && $row2['hk2p1'] == "") {
        echo "<script>alert('Upss! Saat ini halaman tidak tersedia!');history.go(-1)</script>";  
      }else{
        echo "<script>alert('Saat ini $row2[nama_karyawan] telah proses penilaian terakhir pada Contract Kedua');</script>";
      }
    }elseif ($row2['sp2'] == 3 && $row2['bp2'] == 3 && $row2['hk2p3'] == "") {
      if ($row2['sp2'] == 3 && $row2['bp2'] == 3 && $row2['hk2p2'] == "") {
        echo "<script>alert('Upss! Saat ini halaman tidak tersedia!');history.go(-1)</script>";  
      }else{
        echo "<script>alert('Saat ini $row2[nama_karyawan] telah proses penilaian terakhir pada Contract Kedua');</script>";
      }
    }elseif ($row2['sp2'] == 4 && $row2['bp2'] == 4 && $row2['hk2p4'] == "") {
      if ($row2['sp2'] == 4 && $row2['bp2'] == 4 && $row2['hk2p3'] == "") {
        echo "<script>alert('Upss! Saat ini halaman tidak tersedia!');history.go(-1)</script>";  
      }else{
        echo "<script>alert('Saat ini $row2[nama_karyawan] telah proses penilaian terakhir pada Contract Kedua');</script>";
      }
    }elseif ($row2['sp3'] == 1 && $row2['bp3'] == 1 && $row2['hk3p1'] == "") {
      echo "<script>alert('Saat ini $row2[nama_karyawan] telah proses penilaian terakhir pada Contract Ketiga');</script>";
    }elseif ($row2['sp3'] == 2 && $row2['bp3'] == 2 && $row2['hk3p2'] == "") {
      if ($row2['sp3'] == 2 && $row2['bp3'] == 2 && $row2['hk3p1'] == "") {
        echo "<script>alert('Upss! Saat ini halaman tidak tersedia!');history.go(-1)</script>";  
      }else{
        echo "<script>alert('Saat ini $row2[nama_karyawan] telah proses penilaian terakhir pada Contract Ketiga');</script>";
      }
    }elseif ($row2['sp3'] == 3 && $row2['bp3'] == 3 && $row2['hk3p3'] == "") {
      if ($row2['sp3'] == 3 && $row2['bp3'] == 3 && $row2['hk3p2'] == "") {
        echo "<script>alert('Upss! Saat ini halaman tidak tersedia!');history.go(-1)</script>";  
      }else{
        echo "<script>alert('Saat ini $row2[nama_karyawan] telah proses penilaian terakhir pada Contract Ketiga');</script>";
      }
    }elseif ($row2['sp3'] == 4 && $row2['bp3'] == 4 && $row2['hk3p4'] == "") {
      if ($row2['sp3'] == 4 && $row2['bp3'] == 4 && $row2['hk3p3'] == "") {
        echo "<script>alert('Upss! Saat ini halaman tidak tersedia!');history.go(-1)</script>";  
      }else{
        echo "<script>alert('Saat ini $row2[nama_karyawan] telah proses penilaian terakhir pada Contract Ketiga');</script>";
      }
    }else{
      echo "<script>alert('Upss! Saat ini halaman tidak tersedia!');history.go(-1)</script>";
    }
  }

  if (isset($_POST['submit'])) {
    $r1 = trim($_POST['r1']);
    $r2 = trim($_POST['r2']);
    $r3 = trim($_POST['r3']);
    $r4 = trim($_POST['r4']);
    $r5 = trim($_POST['r5']);
    $r6 = trim($_POST['r6']);
    $r7 = trim($_POST['r7']);
    $r8 = trim($_POST['r8']);
    $r9 = trim($_POST['r9']);
    $r10 = trim($_POST['r10']);
    $r11 = trim($_POST['r11']);
    $ket1 = trim($_POST['ket1']);
    $ket2 = trim($_POST['ket2']);
    $ket3 = trim($_POST['ket3']);
    $rekomendasi = trim($_POST['rekomendasi']);


    $sum = $r1 + $r2 + $r3 + $r4 + $r5 + $r6 + $r7 + $r8 + $r9 + $r10 + $r11 ;
    $pembagian = $sum / 11;
    $hasil = $pembagian / 5 * 100;
    $hasilkonversi = number_format($hasil,0,",",".");

    if ($hasilkonversi >= 93 && $hasilkonversi <= 100) {
      $nilai = "A";
    }elseif ($hasilkonversi >= 72 && $hasilkonversi < 92) {
      $nilai = "B";
    }elseif ($hasilkonversi >= 50 && $hasilkonversi < 73) {
      $nilai = "C";
    }elseif ($hasilkonversi >= 30 && $hasilkonversi < 50) {
      $nilai = "D";
    }elseif ($hasilkonversi >= 0 && $hasilkonversi < 30) {
      $nilai = "E";
    }


    if (empty($r1)) {
      echo "<script>alert('Silahkan pilih nilai pada Aspect KEHADIRAN no 1!');history.go(-1)</script>";
    }elseif (empty($r2)) {
      echo "<script>alert('Silahkan pilih nilai pada Aspect KEHADIRAN no 2!');history.go(-1)</script>";
    }elseif (empty($r3)) {
      echo "<script>alert('Silahkan pilih nilai pada Aspect PENGETAHUAN no 1 !');history.go(-1)</script>";
    }elseif (empty($r4)) {
      echo "<script>alert('Silahkan pilih nilai pada Aspect PENGETAHUAN no 2 !');history.go(-1)</script>";
    }elseif (empty($r5)) {
      echo "<script>alert('Silahkan pilih nilai pada Aspect PENGETAHUAN no 3 !');history.go(-1)</script>";
    }elseif (empty($r6)) {
      echo "<script>alert('Silahkan pilih nilai pada Aspect HASIL KERJA no 1 !');history.go(-1)</script>";
    }elseif (empty($r7)) {
      echo "<script>alert('Silahkan pilih nilai pada Aspect HASIL KERJA no 2 !');history.go(-1)</script>";
    }elseif (empty($r8)) {
      echo "<script>alert('Silahkan pilih nilai pada Aspect SIAP KERJA no 1 !');history.go(-1)</script>";
    }elseif (empty($r9)) {
      echo "<script>alert('Silahkan pilih nilai pada Aspect SIAP KERJA no 2 !');history.go(-1)</script>";
    }elseif (empty($r10)) {
      echo "<script>alert('Silahkan pilih nilai pada Aspect SIAP KERJA no 3 !');history.go(-1)</script>";
    }elseif (empty($r11)) {
      echo "<script>alert('Silahkan pilih nilai pada Aspect SIAP KERJA no 4 !');history.go(-1)</script>";
    }elseif (empty($ket1)) {
      echo "<script>alert('Silahkan isi Keterangan tentang prestasi atau unjuk kerja!');history.go(-1)</script>";
    }elseif (empty($ket2)) {
      echo "<script>alert('Silahkan isi Hal-hal penting yang terjadi!');history.go(-1)</script>";
    }elseif (empty($ket2)) {
      echo "<script>alert('Silahkan isi Keterangan surat rekomendasi!');history.go(-1)</script>";
    }elseif (empty($rekomendasi)) {
      echo "<script>alert('Silahkan pilih pada Rekomendasi!');history.go(-1)</script>";
    }elseif (!preg_match("/^[0-5]*$/", $r1)){
      echo "<script>alert('Aspect KEHADIRAN no 1 ada yang salah!');history.go(-1)</script>";
    }elseif (!preg_match("/^[0-5]*$/", $r2)){
      echo "<script>alert('Aspect KEHADIRAN no 2 ada yang salah!');history.go(-1)</script>";
    }elseif (!preg_match("/^[0-5]*$/", $r3)){
      echo "<script>alert('Aspect PENGETAHUAN no 1 ada yang salah!');history.go(-1)</script>";
    }elseif (!preg_match("/^[0-5]*$/", $r4)){
      echo "<script>alert('Aspect PENGETAHUAN no 2 ada yang salah!');history.go(-1)</script>";
    }elseif (!preg_match("/^[0-5]*$/", $r5)){
      echo "<script>alert('Aspect PENGETAHUAN no 3 ada yang salah!');history.go(-1)</script>";
    }elseif (!preg_match("/^[0-5]*$/", $r6)){
      echo "<script>alert('Aspect HASIL KERJA no 1 ada yang salah!');history.go(-1)</script>";
    }elseif (!preg_match("/^[0-5]*$/", $r7)){
      echo "<script>alert('Aspect HASIL KERJA no 2 ada yang salah!');history.go(-1)</script>";
    }elseif (!preg_match("/^[0-5]*$/", $r8)){
      echo "<script>alert('Aspect SIAP KERJA no 1 ada yang salah!');history.go(-1)</script>";
    }elseif (!preg_match("/^[0-5]*$/", $r9)){
      echo "<script>alert('Aspect SIAP KERJA no 2 ada yang salah!');history.go(-1)</script>";
    }elseif (!preg_match("/^[0-5]*$/", $r10)){
      echo "<script>alert('Aspect SIAP KERJA no 3 ada yang salah!');history.go(-1)</script>";
    }elseif (!preg_match("/^[0-5]*$/", $r11)){
      echo "<script>alert('Aspect SIAP KERJA no 4 ada yang salah!');history.go(-1)</script>";
    }elseif(strlen($ket1) > 1200){
      echo "<script>alert('Panjang Max 1200 Character pada Keterangan tentang prestasi atau unjuk kerja!');history.go(-1)</script>";
    }elseif(strlen($ket2) > 1200){
      echo "<script>alert('Panjang Max 1200 Character pada Hal-hal penting yang terjadi!');history.go(-1)</script>";
    }elseif(strlen($ket3) > 1200){
      echo "<script>alert('Panjang Max 1200 Character pada Keterangan surat rekomendasi!');history.go(-1)</script>";
    }elseif ($data2 == "1" && $data3 == "1" && $row2['hk1p1'] == "") {
      $sql1 = "UPDATE tb_kon1_pen1 SET jawaban1='$r1', jawaban2='$r2', jawaban3='$r3', jawaban4='$r4', jawaban5='$r5', jawaban6='$r6', jawaban7='$r7', jawaban8='$r8', jawaban9='$r9', jawaban10='$r10', jawaban11='$r11', ket1='$ket1', ket2='$ket2', ket3='$ket3', rekomendasi='$rekomendasi', hasil='$nilai' WHERE id_karyawan='$data1'";
      $qry1 = mysqli_query($koneksi, $sql1);

      $sql2 = "UPDATE tb_kontrak1 SET id_proses=1 WHERE id_karyawan='$data1'";
      $qry2 = mysqli_query($koneksi, $sql2);
      echo "<script>alert('Penilaian telah berhasil!') ;window.location='assuser.php'; </script>";
    }elseif ($data2 == "1" && $data3 == "2" && $row2['hk1p2'] == "") {
      $sql3 = "UPDATE tb_kon1_pen2 SET jawaban1='$r1', jawaban2='$r2', jawaban3='$r3', jawaban4='$r4', jawaban5='$r5', jawaban6='$r6', jawaban7='$r7', jawaban8='$r8', jawaban9='$r9', jawaban10='$r10', jawaban11='$r11', ket1='$ket1', ket2='$ket2', ket3='$ket3', rekomendasi='$rekomendasi', hasil='$nilai' WHERE id_karyawan='$data1'";
      $qry3 = mysqli_query($koneksi, $sql3);

      $sql4 = "UPDATE tb_kontrak1 SET id_proses=1 WHERE id_karyawan='$data1'";
      $qry4 = mysqli_query($koneksi, $sq42);
      echo "<script>alert('Penilaian telah berhasil!') ;window.location='assuser.php'; </script>";
    }elseif ($data2 == "1" && $data3 == "3" && $row2['hk1p3'] == "") {
      $sql5 = "UPDATE tb_kon1_pen3 SET jawaban1='$r1', jawaban2='$r2', jawaban3='$r3', jawaban4='$r4', jawaban5='$r5', jawaban6='$r6', jawaban7='$r7', jawaban8='$r8', jawaban9='$r9', jawaban10='$r10', jawaban11='$r11', ket1='$ket1', ket2='$ket2', ket3='$ket3', rekomendasi='$rekomendasi', hasil='$nilai' WHERE id_karyawan='$data1'";
      $qry5 = mysqli_query($koneksi, $sql5);

      $sql6 = "UPDATE tb_kontrak1 SET id_proses=1 WHERE id_karyawan='$data1'";
      $qry6 = mysqli_query($koneksi, $sql6);
      echo "<script>alert('Penilaian telah berhasil!') ;window.location='assuser.php'; </script>";
    }elseif ($data2 == "1" && $data3 == "4" && $row2['hk1p4'] == "") {
      $sql7 = "UPDATE tb_kon1_pen4 SET jawaban1='$r1', jawaban2='$r2', jawaban3='$r3', jawaban4='$r4', jawaban5='$r5', jawaban6='$r6', jawaban7='$r7', jawaban8='$r8', jawaban9='$r9', jawaban10='$r10', jawaban11='$r11', ket1='$ket1', ket2='$ket2', ket3='$ket3', rekomendasi='$rekomendasi', hasil='$nilai' WHERE id_karyawan='$data1'";
      $qry7 = mysqli_query($koneksi, $sql7);

      $sql8 = "UPDATE tb_kontrak1 SET id_proses=1 WHERE id_karyawan='$data1'";
      $qry8 = mysqli_query($koneksi, $sql8);
      echo "<script>alert('Penilaian telah berhasil!') ;window.location='assuser.php'; </script>";
    }elseif ($data2 == "2" && $data3 == "1" && $row2['hk2p1'] == "") {
      $sql9 = "UPDATE tb_kon2_pen1 SET jawaban1='$r1', jawaban2='$r2', jawaban3='$r3', jawaban4='$r4', jawaban5='$r5', jawaban6='$r6', jawaban7='$r7', jawaban8='$r8', jawaban9='$r9', jawaban10='$r10', jawaban11='$r11', ket1='$ket1', ket2='$ket2', ket3='$ket3', rekomendasi='$rekomendasi', hasil='$nilai' WHERE id_karyawan='$data1'";
      $qry9 = mysqli_query($koneksi, $sql9);

      $sql10 = "UPDATE tb_kontrak2 SET id_proses=1 WHERE id_karyawan='$data1'";
      $qry10 = mysqli_query($koneksi, $sql10);
      echo "<script>alert('Penilaian telah berhasil!') ;window.location='assuser.php'; </script>";
    }elseif ($data2 == "2" && $data3 == "2" && $row2['hk2p2'] == "") {
      $sql11 = "UPDATE tb_kon2_pen2 SET jawaban1='$r1', jawaban2='$r2', jawaban3='$r3', jawaban4='$r4', jawaban5='$r5', jawaban6='$r6', jawaban7='$r7', jawaban8='$r8', jawaban9='$r9', jawaban10='$r10', jawaban11='$r11', ket1='$ket1', ket2='$ket2', ket3='$ket3', rekomendasi='$rekomendasi', hasil='$nilai' WHERE id_karyawan='$data1'";
      $qry11 = mysqli_query($koneksi, $sql11);

      $sql12 = "UPDATE tb_kontrak2 SET id_proses=1 WHERE id_karyawan='$data1'";
      $qry12 = mysqli_query($koneksi, $sql12);
      echo "<script>alert('Penilaian telah berhasil!') ;window.location='assuser.php'; </script>";
    }elseif ($data2 == "2" && $data3 == "3" && $row2['hk2p1'] == "") {
      $sql13 = "UPDATE tb_kon2_pen3 SET jawaban1='$r1', jawaban2='$r2', jawaban3='$r3', jawaban4='$r4', jawaban5='$r5', jawaban6='$r6', jawaban7='$r7', jawaban8='$r8', jawaban9='$r9', jawaban10='$r10', jawaban11='$r11', ket1='$ket1', ket2='$ket2', ket3='$ket3', rekomendasi='$rekomendasi', hasil='$nilai' WHERE id_karyawan='$data1'";
      $qry13 = mysqli_query($koneksi, $sql13);

      $sql14 = "UPDATE tb_kontrak2 SET id_proses=1 WHERE id_karyawan='$data1'";
      $qry14 = mysqli_query($koneksi, $sql14);
      echo "<script>alert('Penilaian telah berhasil!') ;window.location='assuser.php'; </script>";
    }elseif ($data2 == "2" && $data3 == "4" && $row2['hk2p4'] == "") {
      $sql15 = "UPDATE tb_kon2_pen4 SET jawaban1='$r1', jawaban2='$r2', jawaban3='$r3', jawaban4='$r4', jawaban5='$r5', jawaban6='$r6', jawaban7='$r7', jawaban8='$r8', jawaban9='$r9', jawaban10='$r10', jawaban11='$r11', ket1='$ket1', ket2='$ket2', ket3='$ket3', rekomendasi='$rekomendasi', hasil='$nilai' WHERE id_karyawan='$data1'";
      $qry15 = mysqli_query($koneksi, $sql15);

      $sql16 = "UPDATE tb_kontrak2 SET id_proses=1 WHERE id_karyawan='$data1'";
      $qry16 = mysqli_query($koneksi, $sql16);
      echo "<script>alert('Penilaian telah berhasil!') ;window.location='assuser.php'; </script>";
    }elseif ($data2 == "3" && $data3 == "1" && $row2['hk3p1'] == "") {
      $sql17 = "UPDATE tb_kon3_pen1 SET jawaban1='$r1', jawaban2='$r2', jawaban3='$r3', jawaban4='$r4', jawaban5='$r5', jawaban6='$r6', jawaban7='$r7', jawaban8='$r8', jawaban9='$r9', jawaban10='$r10', jawaban11='$r11', ket1='$ket1', ket2='$ket2', ket3='$ket3', rekomendasi='$rekomendasi', hasil='$nilai' WHERE id_karyawan='$data1'";
      $qry17 = mysqli_query($koneksi, $sql17);

      $sql18 = "UPDATE tb_kontrak3 SET id_proses=1 WHERE id_karyawan='$data1'";
      $qry18 = mysqli_query($koneksi, $sql18);
      echo "<script>alert('Penilaian telah berhasil!') ;window.location='assuser.php'; </script>";
    }elseif ($data2 == "3" && $data3 == "2" && $row2['hk3p2'] == "") {
      $sql19 = "UPDATE tb_kon3_pen2 SET jawaban1='$r1', jawaban2='$r2', jawaban3='$r3', jawaban4='$r4', jawaban5='$r5', jawaban6='$r6', jawaban7='$r7', jawaban8='$r8', jawaban9='$r9', jawaban10='$r10', jawaban11='$r11', ket1='$ket1', ket2='$ket2', ket3='$ket3', rekomendasi='$rekomendasi', hasil='$nilai' WHERE id_karyawan='$data1'";
      $qry19 = mysqli_query($koneksi, $sql19);

      $sql20 = "UPDATE tb_kontrak3 SET id_proses=1 WHERE id_karyawan='$data1'";
      $qry20 = mysqli_query($koneksi, $sql20);
      echo "<script>alert('Penilaian telah berhasil!') ;window.location='assuser.php'; </script>";
    }elseif ($data2 == "3" && $data3 == "3" && $row2['hk3p3'] == "") {
      $sql21 = "UPDATE tb_kon3_pen3 SET jawaban1='$r1', jawaban2='$r2', jawaban3='$r3', jawaban4='$r4', jawaban5='$r5', jawaban6='$r6', jawaban7='$r7', jawaban8='$r8', jawaban9='$r9', jawaban10='$r10', jawaban11='$r11', ket1='$ket1', ket2='$ket2', ket3='$ket3', rekomendasi='$rekomendasi', hasil='$nilai' WHERE id_karyawan='$data1'";
      $qry21 = mysqli_query($koneksi, $sql21);

      $sql22 = "UPDATE tb_kontrak3 SET id_proses=1 WHERE id_karyawan='$data1'";
      $qry22 = mysqli_query($koneksi, $sql22);
      echo "<script>alert('Penilaian telah berhasil!') ;window.location='assuser.php'; </script>";
    }elseif ($data2 == "3" && $data3 == "4" && $row2['hk3p4'] == "") {
      $sql23 = "UPDATE tb_kon3_pen4 SET jawaban1='$r1', jawaban2='$r2', jawaban3='$r3', jawaban4='$r4', jawaban5='$r5', jawaban6='$r6', jawaban7='$r7', jawaban8='$r8', jawaban9='$r9', jawaban10='$r10', jawaban11='$r11', ket1='$ket1', ket2='$ket2', ket3='$ket3', rekomendasi='$rekomendasi', hasil='$nilai' WHERE id_karyawan='$data1'";
      $qry23 = mysqli_query($koneksi, $sql23);

      $sql24 = "UPDATE tb_kontrak3 SET id_proses=1 WHERE id_karyawan='$data1'";
      $qry24 = mysqli_query($koneksi, $sql24);
      echo "<script>alert('Penilaian telah berhasil!') ;window.location='assuser.php'; </script>";
    // }elseif ($data2 == "1") {
    //   if ($row2['sp1'] == 1 && $row2['bp1'] == 1 && $row2['hk1p1'] == "") {
    //     $sql1 = "UPDATE tb_kon1_pen1 SET jawaban1='$r1', jawaban2='$r2', jawaban3='$r3', jawaban4='$r4', jawaban5='$r5', jawaban6='$r6', jawaban7='$r7', jawaban8='$r8', jawaban9='$r9', jawaban10='$r10', jawaban11='$r11', ket1='$ket1', ket2='$ket2', ket3='$ket3', rekomendasi='$rekomendasi', hasil='$nilai' WHERE id_karyawan='$data1'";
    //     $qry1 = mysqli_query($koneksi, $sql1);

    //     $sql2 = "UPDATE tb_kontrak1 SET id_proses=1 WHERE id_karyawan='$data1'";
    //     $qry2 = mysqli_query($koneksi, $sql2);
    //     echo "<script>alert('Penilaian telah berhasil!') ;window.location='assuser.php'; </script>";
    //   }elseif($row2['sp1'] == 2 && $row2['bp1'] == 2 && $row2['hk1p2'] == ""){
    //     $sql3 = "UPDATE tb_kon1_pen2 SET jawaban1='$r1', jawaban2='$r2', jawaban3='$r3', jawaban4='$r4', jawaban5='$r5', jawaban6='$r6', jawaban7='$r7', jawaban8='$r8', jawaban9='$r9', jawaban10='$r10', jawaban11='$r11', ket1='$ket1', ket2='$ket2', ket3='$ket3', rekomendasi='$rekomendasi', hasil='$nilai' WHERE id_karyawan='$data1'";
    //     $qry3 = mysqli_query($koneksi, $sql3);
        
    //     $sql4 = "UPDATE tb_kontrak1 SET id_proses=1 WHERE id_karyawan='$data1'";
    //     $qry4 = mysqli_query($koneksi, $sql4);
    //     echo "<script>alert('Penilaian telah berhasil!') ;window.location='assuser.php'; </script>";
    //   }elseif ($row2['sp1'] == 3 && $row2['bp1'] == 3 && $row2['hk1p3'] == "") {
    //     $sql5 = "UPDATE tb_kon1_pen3 SET jawaban1='$r1', jawaban2='$r2', jawaban3='$r3', jawaban4='$r4', jawaban5='$r5', jawaban6='$r6', jawaban7='$r7', jawaban8='$r8', jawaban9='$r9', jawaban10='$r10', jawaban11='$r11', ket1='$ket1', ket2='$ket2', ket3='$ket3', rekomendasi='$rekomendasi', hasil='$nilai' WHERE id_karyawan='$data1'";
    //     $qry5 = mysqli_query($koneksi, $sql5);
        
    //     $sql6 = "UPDATE tb_kontrak1 SET id_proses=1 WHERE id_karyawan='$data1'";
    //     $qry6 = mysqli_query($koneksi, $sql6);
    //     echo "<script>alert('Penilaian telah berhasil!') ;window.location='assuser.php'; </script>";
    //   }elseif ($row2['sp1'] == 4 && $row2['bp1'] == 4 && $row2['hk1p4'] == "") {
    //     $sql7 = "UPDATE tb_kon1_pen4 SET jawaban1='$r1', jawaban2='$r2', jawaban3='$r3', jawaban4='$r4', jawaban5='$r5', jawaban6='$r6', jawaban7='$r7', jawaban8='$r8', jawaban9='$r9', jawaban10='$r10', jawaban11='$r11', ket1='$ket1', ket2='$ket2', ket3='$ket3', rekomendasi='$rekomendasi', hasil='$nilai' WHERE id_karyawan='$data1'";
    //     $qry7 = mysqli_query($koneksi, $sql7);
        
    //     $sql8 = "UPDATE tb_kontrak1 SET id_proses=1 WHERE id_karyawan='$data1'";
    //     $qry8 = mysqli_query($koneksi, $sql8);
    //     echo "<script>alert('Penilaian telah berhasil!') ;window.location='assuser.php'; </script>";
    //   }else{
    //     echo "<script>alert('Employee ini sudah dinilai, Silahkan proses edit di View Assessment untuk merubah penilaian!') ;window.location='assuser.php'</script>";
    //   }
    // }elseif ($data2 == "2") {
    //   if ($row2['sp2'] == 1 && $row2['bp2'] == 1 && $row2['hk2p1'] == "") {
    //     $sql9 = "UPDATE tb_kon2_pen1 SET jawaban1='$r1', jawaban2='$r2', jawaban3='$r3', jawaban4='$r4', jawaban5='$r5', jawaban6='$r6', jawaban7='$r7', jawaban8='$r8', jawaban9='$r9', jawaban10='$r10', jawaban11='$r11', ket1='$ket1', ket2='$ket2', ket3='$ket3', rekomendasi='$rekomendasi', hasil='$nilai' WHERE id_karyawan='$data1'";
    //     $qry9 = mysqli_query($koneksi, $sql9);

    //     $sql10 = "UPDATE tb_kontrak2 SET id_proses=1 WHERE id_karyawan='$data1'";
    //     $qry10 = mysqli_query($koneksi, $sql10);
    //     echo "<script>alert('Penilaian telah berhasil!') ;window.location='assuser.php'; </script>";
    //   }elseif($row2['sp2'] == 2 && $row2['bp2'] == 2 && $row2['hk2p2'] ==""){
    //     $sql11 = "UPDATE tb_kon2_pen2 SET jawaban1='$r1', jawaban2='$r2', jawaban3='$r3', jawaban4='$r4', jawaban5='$r5', jawaban6='$r6', jawaban7='$r7', jawaban8='$r8', jawaban9='$r9', jawaban10='$r10', jawaban11='$r11', ket1='$ket1', ket2='$ket2', ket3='$ket3', rekomendasi='$rekomendasi', hasil='$nilai' WHERE id_karyawan='$data1'";
    //     $qry11 = mysqli_query($koneksi, $sql11);
        
    //     $sql12 = "UPDATE tb_kontrak2 SET id_proses=1 WHERE id_karyawan='$data1'";
    //     $qry12 = mysqli_query($koneksi, $sql12);
    //     echo "<script>alert('Penilaian telah berhasil!') ;window.location='assuser.php'; </script>";
    //   }elseif ($row2['sp2'] == 3 && $row2['bp2'] == 3 && $row2['hk2p3'] == "") {
    //     $sql13 = "UPDATE tb_kon2_pen3 SET jawaban1='$r1', jawaban2='$r2', jawaban3='$r3', jawaban4='$r4', jawaban5='$r5', jawaban6='$r6', jawaban7='$r7', jawaban8='$r8', jawaban9='$r9', jawaban10='$r10', jawaban11='$r11', ket1='$ket1', ket2='$ket2', ket3='$ket3', rekomendasi='$rekomendasi', hasil='$nilai' WHERE id_karyawan='$data1'";
    //     $qry13 = mysqli_query($koneksi, $sql13);
        
    //     $sql14 = "UPDATE tb_kontrak2 SET id_proses=1 WHERE id_karyawan='$data1'";
    //     $qry14 = mysqli_query($koneksi, $sql14);
    //     echo "<script>alert('Penilaian telah berhasil!') ;window.location='assuser.php'; </script>";
    //   }elseif ($row2['sp2'] == 4 && $row2['bp2'] == 4 && $row2['hk2p4'] =="") {
    //     $sql15 = "UPDATE tb_kon2_pen4 SET jawaban1='$r1', jawaban2='$r2', jawaban3='$r3', jawaban4='$r4', jawaban5='$r5', jawaban6='$r6', jawaban7='$r7', jawaban8='$r8', jawaban9='$r9', jawaban10='$r10', jawaban11='$r11', ket1='$ket1', ket2='$ket2', ket3='$ket3', rekomendasi='$rekomendasi', hasil='$nilai' WHERE id_karyawan='$data1'";
    //     $qry15 = mysqli_query($koneksi, $sql15);
        
    //     $sql16 = "UPDATE tb_kontrak2 SET id_proses=1 WHERE id_karyawan='$data1'";
    //     $qry16 = mysqli_query($koneksi, $sql16);
    //     echo "<script>alert('Penilaian telah berhasil!') ;window.location='assuser.php'; </script>";
    //   }else{
    //     echo "<script>alert('Employee ini sudah dinilai, Silahkan proses edit di View Assessment untuk merubah penilaian!') ;window.location='assuser.php'</script>";
    //   }
    // }elseif ($data2 == "3") {
    //   if ($row2['sp3'] == 1 && $row2['bp3'] == 1 && $row2['hk3p1'] == "") {
    //     $sql17 = "UPDATE tb_kon3_pen1 SET jawaban1='$r1', jawaban2='$r2', jawaban3='$r3', jawaban4='$r4', jawaban5='$r5', jawaban6='$r6', jawaban7='$r7', jawaban8='$r8', jawaban9='$r9', jawaban10='$r10', jawaban11='$r11', ket1='$ket1', ket2='$ket2', ket3='$ket3', rekomendasi='$rekomendasi', hasil='$nilai' WHERE id_karyawan='$data1'";
    //     $qry17 = mysqli_query($koneksi, $sql17);

    //     $sql18 = "UPDATE tb_kontrak3 SET id_proses=1 WHERE id_karyawan='$data1'";
    //     $qry18 = mysqli_query($koneksi, $sql18);
    //     echo "<script>alert('Penilaian telah berhasil!') ;window.location='assuser.php'; </script>";
    //   }elseif($row2['sp3'] == 2 && $row2['bp3'] == 2 && $row2['hk3p2'] == ""){
    //     $sql19 = "UPDATE tb_kon3_pen2 SET jawaban1='$r1', jawaban2='$r2', jawaban3='$r3', jawaban4='$r4', jawaban5='$r5', jawaban6='$r6', jawaban7='$r7', jawaban8='$r8', jawaban9='$r9', jawaban10='$r10', jawaban11='$r11', ket1='$ket1', ket2='$ket2', ket3='$ket3', rekomendasi='$rekomendasi', hasil='$nilai' WHERE id_karyawan='$data1'";
    //     $qry19 = mysqli_query($koneksi, $sql19);
        
    //     $sql20 = "UPDATE tb_kontrak3 SET id_proses=1 WHERE id_karyawan='$data1'";
    //     $qry20 = mysqli_query($koneksi, $sql20);
    //     echo "<script>alert('Penilaian telah berhasil!') ;window.location='assuser.php'; </script>";
    //   }elseif ($row2['sp3'] == 3 && $row2['bp3'] == 3 && $row2['hk3p3'] == "") {
    //     $sql21 = "UPDATE tb_kon3_pen3 SET jawaban1='$r1', jawaban2='$r2', jawaban3='$r3', jawaban4='$r4', jawaban5='$r5', jawaban6='$r6', jawaban7='$r7', jawaban8='$r8', jawaban9='$r9', jawaban10='$r10', jawaban11='$r11', ket1='$ket1', ket2='$ket2', ket3='$ket3', rekomendasi='$rekomendasi', hasil='$nilai' WHERE id_karyawan='$data1'";
    //     $qry21 = mysqli_query($koneksi, $sql21);
        
    //     $sql22 = "UPDATE tb_kontrak3 SET id_proses=1 WHERE id_karyawan='$data1'";
    //     $qry22 = mysqli_query($koneksi, $sql22);
    //     echo "<script>alert('Penilaian telah berhasil!') ;window.location='assuser.php'; </script>";
    //   }elseif ($row2['sp3'] == 4 && $row2['bp3'] == 4 && $row2['hk3p4'] == "") {
    //     $sql23 = "UPDATE tb_kon3_pen4 SET jawaban1='$r1', jawaban2='$r2', jawaban3='$r3', jawaban4='$r4', jawaban5='$r5', jawaban6='$r6', jawaban7='$r7', jawaban8='$r8', jawaban9='$r9', jawaban10='$r10', jawaban11='$r11', ket1='$ket1', ket2='$ket2', ket3='$ket3', rekomendasi='$rekomendasi', hasil='$nilai' WHERE id_karyawan='$data1'";
    //     $qry23 = mysqli_query($koneksi, $sql23);
        
    //     $sql24 = "UPDATE tb_kontrak3 SET id_proses=1 WHERE id_karyawan='$data1'";
    //     $qry24 = mysqli_query($koneksi, $sql24);
    //     echo "<script>alert('Penilaian telah berhasil!') ;window.location='assuser.php'; </script>";
    //   }
    }else{
      echo "<script>alert('Employee ini sudah dinilai, Silahkan proses edit di View Assessment untuk merubah penilaian!') ;window.location='assuser.php'</script>";
    }
  }

  ?>


</head>

<body class="hold-transition sidebar-mini layout-fixed">
  <div class="wrapper">

    <!-- Navbar -->
    <nav class="main-header navbar navbar-expand navbar-dark">
      <!-- Left navbar links -->
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
        </li>
      </ul>

      <!-- Right navbar links -->
      <ul class="navbar-nav ml-auto">
        <!-- Notifications Dropdown Menu -->
        <li class="nav-item dropdown">
          <a class="nav-link" data-toggle="dropdown" href="#">
            <span><?php echo $_SESSION['userlogin']; ?></span>
            <i class="fas fa-user-alt"></i>
          </a>
          <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
            <div class="dropdown-divider"></div>
            <a href="settinguser.php" class="dropdown-item">
              <i class="fas fa-cog mr-2"></i>
              <span class="float-right text-muted text-sm">Setting</span>
            </a>
            <div class="dropdown-divider"></div>
            <a href="../logout.php" class="dropdown-item">
              <i class="fas fa-sign-out-alt mr-2"></i>
              <span class="float-right text-muted text-sm">Logout</span>
            </a>
          </div>
        </li>
      </ul>
    </nav>
    <!-- /.navbar -->

    <!-- Main Sidebar Container -->
    <aside class="main-sidebar sidebar-light-olive elevation-4">
      <!-- Brand Logo -->
      <a href="homeuser.php" class="brand-link navbar-light">
        <img src="../gambar/logociputra2.png" alt="AdminLTE Logo" class="brand-image elevation-3"
        style="opacity: .8">
        <span class="brand-text font-weight-light"><b>CIPUTRA</b></span>
      </a>

      <!-- Sidebar -->
      <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
          <div class="image">
            <?php
            $cek_foto = $row['foto'];
            $tempat_foto = '../foto/'.$row['foto']; 
            if ($cek_foto) {
              echo "<img src='$tempat_foto' class='img-circle elevation-2' alt='User Image'>"; 
            }else{
              echo "<img src='../foto/blank.png'></a>";
            }
            ?>
          </div>
          <div class="info">
            <a href="#" class="d-block"><?php echo $row['nama']; ?></a>
          </div>
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2">
          <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
           with font-awesome or any other icon font library -->
           <li class="nav-item">
            <a href="assuser.php" class="nav-link active">
              <i class="nav-icon fas fa-edit"></i>
              <p>
                Assessment
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="viewassuser.php" class="nav-link">
              <i class="nav-icon fas fa-clipboard-list"></i>
              <p>
                View Assessment
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="settinguser.php" class="nav-link">
              <i class="nav-icon fas fa-cog"></i>
              <p>
                Setting
              </p>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>


  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Process Assessment</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="homeuser.php">Home</a></li>
              <li class="breadcrumb-item"><a href="assuser.php">Assessment</a></li>
              <li class="breadcrumb-item active">Process Assessment</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">    
        <!-- Main row -->
        <div class="row">
          <div class="col-md-12">
            <!-- TABLE: LATEST ORDERS -->
            <div class="card card-olive">
              <div class="card-body p-0">
                <div class="table-responsive">
                  <table class="table-question m-0">
                    <tbody> 
                      <tr>
                        <td colspan="2">
                          <h3><center>Saat ini anda sedang Assessment : <b><?php echo $row2['nama_karyawan']." = ".$row2['nik'];?></b></center></h3>
                        </td>
                      </tr>
                      <tr>
                        <td colspan="2">
                          <b>Pilihlah salah satu lingkaran bulat &nbsp;
                            <div class="icheck-success d-inline">
                              <input type="radio" name="r100" checked id="radioSuccess100">
                              <label for="radioSuccess100">
                              </label>
                            </div>
                          dalam setiap Aspect</b>
                        </td>
                      </tr>
                      <tr>
                        <td width="21%">
                          <span>Keterangan Penilaian&emsp; :</span>
                        </td>
                        <td>
                          <b>RENDAH</b> = Tidak Memenuhi Persyaratan<br>
                          <b>KURANG</b> = Sebagian Memenuhi Persyaratan<br>
                          <b>CUKUP</b> = Memenuhi Persyaratan<br>
                          <b>BAIK</b> = Melampaui Persyaratan<br>
                          <b>TINGGI</b> = Jauh Melampaui Persyaratan
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
                <!-- /.table-responsive -->
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
            <form action="" method="post">
              <!-- TABLE: LATEST ORDERS -->
              <div class="card card-olive">
                <div class="card-header">
                  <h3 class="card-title">KEHADIRAN</h3>
                  <div></div>
                  <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse">
                      <i class="fas fa-minus font-setting-4"></i>
                    </button>
                  </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body p-0">
                  <div class="table-responsive">
                    <table class="table-question m-0">
                      <thead>
                        <tr>
                          <th width="6%"><center>No</center></th>
                          <th><center>Aspect</center></th>
                        </tr>
                      </thead>
                      <tbody> 
                        <tr>
                          <td rowspan="2"><center>1</center></td>
                          <td>Ketepatan hadir pada wantu yang telah ditetapkan</td>
                        </tr>
                        <tr>
                          <td>
                            <div style="padding-right: 20px;" class="icheck-success d-inline">
                              <input type="radio" name="r1" id="radioSuccess1" value="1">
                              <label style="font-size: 10pt;" for="radioSuccess1">
                                RENDAH
                              </label>
                            </div>
                            <div style="padding-right: 20px;" class="icheck-success d-inline">
                              <input type="radio" name="r1" id="radioSuccess2" value="2">
                              <label style="font-size: 10pt;" for="radioSuccess2">
                                KURANG
                              </label>
                            </div>
                            <div style="padding-right: 20px;" class="icheck-success d-inline">
                              <input type="radio" name="r1" id="radioSuccess3" value="3">
                              <label style="font-size: 10pt;" for="radioSuccess3">
                                CUKUP
                              </label>
                            </div>
                            <div style="padding-right: 20px;" class="icheck-success d-inline">
                              <input type="radio" name="r1" id="radioSuccess4" value="4">
                              <label style="font-size: 10pt;" for="radioSuccess4">
                                BAIK
                              </label>
                            </div>
                            <div style="padding-right: 20px;" class="icheck-success d-inline">
                              <input type="radio" name="r1" id="radioSuccess5" value="5">
                              <label style="font-size: 10pt;" for="radioSuccess5">
                                TINGGI
                              </label>
                            </div>
                          </td>
                        </tr>
                        <tr>
                          <td rowspan="2"><center>2</center></td>
                          <td>Absensi, Ketidakhadiran disebabkan oleh ijin atau alpha</td>
                        </tr>
                        <tr>
                          <td>
                            <div style="padding-right: 20px;" class="icheck-success d-inline">
                              <input type="radio" name="r2" id="radioSuccess6" value="1">
                              <label style="font-size: 10pt;" for="radioSuccess6">
                                RENDAH
                              </label>
                            </div>
                            <div style="padding-right: 20px;" class="icheck-success d-inline">
                              <input type="radio" name="r2" id="radioSuccess7" value="2">
                              <label style="font-size: 10pt;" for="radioSuccess7">
                                KURANG
                              </label>
                            </div>
                            <div style="padding-right: 20px;" class="icheck-success d-inline">
                              <input type="radio" name="r2" id="radioSuccess8" value="3">
                              <label style="font-size: 10pt;" for="radioSuccess8">
                                CUKUP
                              </label>
                            </div>
                            <div style="padding-right: 20px;" class="icheck-success d-inline">
                              <input type="radio" name="r2" id="radioSuccess9" value="4">
                              <label style="font-size: 10pt;" for="radioSuccess9">
                                BAIK
                              </label>
                            </div>
                            <div style="padding-right: 20px;" class="icheck-success d-inline">
                              <input type="radio" name="r2" id="radioSuccess10" value="5">
                              <label style="font-size: 10pt;" for="radioSuccess10">
                                TINGGI
                              </label>
                            </div>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                  <!-- /.table-responsive -->
                </div>
                <!-- /.card-body -->
              </div>
              <!-- /.card -->
              <!-- TABLE: LATEST ORDERS -->
              <div class="card card-olive">
                <div class="card-header">
                  <h3 class="card-title">PENGETAHUAN</h3>
                  <div></div>
                  <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse">
                      <i class="fas fa-minus font-setting-4"></i>
                    </button>
                  </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body p-0">
                  <div class="table-responsive">
                    <table class="table-question m-0">
                      <thead>
                        <tr>
                          <th width="6%"><center>No</center></th>
                          <th><center>Aspect</center></th>
                        </tr>
                      </thead>
                      <tbody> 
                        <tr>
                          <td rowspan="2"><center>1</center></td>
                          <td>Pengetahuan teoritis tentang pekerjaan, yang didapat dari pendidikan formal dan / informal</td>
                        </tr>
                        <tr>
                          <td>
                            <div style="padding-right: 20px;" class="icheck-success d-inline">
                              <input type="radio" name="r3" id="radioSuccess11" value="1">
                              <label style="font-size: 10pt;" for="radioSuccess11">
                                RENDAH
                              </label>
                            </div>
                            <div style="padding-right: 20px;" class="icheck-success d-inline">
                              <input type="radio" name="r3" id="radioSuccess12" value="2">
                              <label style="font-size: 10pt;" for="radioSuccess12">
                                KURANG
                              </label>
                            </div>
                            <div style="padding-right: 20px;" class="icheck-success d-inline">
                              <input type="radio" name="r3" id="radioSuccess13" value="3">
                              <label style="font-size: 10pt;" for="radioSuccess13">
                                CUKUP
                              </label>
                            </div>
                            <div style="padding-right: 20px;" class="icheck-success d-inline">
                              <input type="radio" name="r3" id="radioSuccess14" value="4">
                              <label style="font-size: 10pt;" for="radioSuccess14">
                                BAIK
                              </label>
                            </div>
                            <div style="padding-right: 20px;" class="icheck-success d-inline">
                              <input type="radio" name="r3" id="radioSuccess15" value="5">
                              <label style="font-size: 10pt;" for="radioSuccess15">
                                TINGGI
                              </label>
                            </div>
                          </td>
                        </tr>
                        <tr>
                          <td rowspan="2"><center>2</center></td>
                          <td>Kemampuan menetapkan pengetahuan teoritis dalam pelaksanaan pekerjaan, Kemampuan kerja praktis terapan</td>
                        </tr>
                        <tr>
                          <td>
                            <div style="padding-right: 20px;" class="icheck-success d-inline">
                              <input type="radio" name="r4" id="radioSuccess16" value="1">
                              <label style="font-size: 10pt;" for="radioSuccess16">
                                RENDAH
                              </label>
                            </div>
                            <div style="padding-right: 20px;" class="icheck-success d-inline">
                              <input type="radio" name="r4" id="radioSuccess17" value="2">
                              <label style="font-size: 10pt;" for="radioSuccess17">
                                KURANG
                              </label>
                            </div>
                            <div style="padding-right: 20px;" class="icheck-success d-inline">
                              <input type="radio" name="r4" id="radioSuccess18" value="3">
                              <label style="font-size: 10pt;" for="radioSuccess18">
                                CUKUP
                              </label>
                            </div>
                            <div style="padding-right: 20px;" class="icheck-success d-inline">
                              <input type="radio" name="r4" id="radioSuccess19" value="4">
                              <label style="font-size: 10pt;" for="radioSuccess19">
                                BAIK
                              </label>
                            </div>
                            <div style="padding-right: 20px;" class="icheck-success d-inline">
                              <input type="radio" name="r4" id="radioSuccess20" value="5">
                              <label style="font-size: 10pt;" for="radioSuccess20">
                                TINGGI
                              </label>
                            </div>
                          </td>
                        </tr>
                        <tr>
                          <td rowspan="2"><center>3</center></td>
                          <td>Kemampuan Mempelajari Hal Baru Kemampuan untuk beradaptasi, mempelajari dan melaksanakan hal-hal baru dalam pekerjaan, motivasi untuk menemukan hal baru</td>
                        </tr>
                        <tr>
                          <td>
                            <div style="padding-right: 20px;" class="icheck-success d-inline">
                              <input type="radio" name="r5" id="radioSuccess21" value="1">
                              <label style="font-size: 10pt;" for="radioSuccess21">
                                RENDAH
                              </label>
                            </div>
                            <div style="padding-right: 20px;" class="icheck-success d-inline">
                              <input type="radio" name="r5" id="radioSuccess22" value="2">
                              <label style="font-size: 10pt;" for="radioSuccess22">
                                KURANG
                              </label>
                            </div>
                            <div style="padding-right: 20px;" class="icheck-success d-inline">
                              <input type="radio" name="r5" id="radioSuccess23" value="3">
                              <label style="font-size: 10pt;" for="radioSuccess23">
                                CUKUP
                              </label>
                            </div>
                            <div style="padding-right: 20px;" class="icheck-success d-inline">
                              <input type="radio" name="r5" id="radioSuccess24" value="4">
                              <label style="font-size: 10pt;" for="radioSuccess24">
                                BAIK
                              </label>
                            </div>
                            <div style="padding-right: 20px;" class="icheck-success d-inline">
                              <input type="radio" name="r5" id="radioSuccess25" value="5">
                              <label style="font-size: 10pt;" for="radioSuccess25">
                                TINGGI
                              </label>
                            </div>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                  <!-- /.table-responsive -->
                </div>
                <!-- /.card-body -->
              </div>
              <!-- /.card -->
              <!-- TABLE: LATEST ORDERS -->
              <div class="card card-olive">
                <div class="card-header">
                  <h3 class="card-title">HASIL KERJA</h3>
                  <div></div>
                  <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse">
                      <i class="fas fa-minus font-setting-4"></i>
                    </button>
                  </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body p-0">
                  <div class="table-responsive">
                    <table class="table-question m-0">
                      <thead>
                        <tr>
                          <th width="6%"><center>No</center></th>
                          <th><center>Aspect</center></th>
                        </tr>
                      </thead>
                      <tbody> 
                        <tr>
                          <td rowspan="2"><center>1</center></td>
                          <td>Kualitas Kerja Keterampilan, ketelitian, kecermatan dan kerapihan hasil kerja, termasuk tingkat pengawasan dan penelitian ualang yang diperlukan</td>
                        </tr>
                        <tr>
                          <td>
                            <div style="padding-right: 20px;" class="icheck-success d-inline">
                              <input type="radio" name="r6" id="radioSuccess26" value="1">
                              <label style="font-size: 10pt;" for="radioSuccess26">
                                RENDAH
                              </label>
                            </div>
                            <div style="padding-right: 20px;" class="icheck-success d-inline">
                              <input type="radio" name="r6" id="radioSuccess27" value="2">
                              <label style="font-size: 10pt;" for="radioSuccess27">
                                KURANG
                              </label>
                            </div>
                            <div style="padding-right: 20px;" class="icheck-success d-inline">
                              <input type="radio" name="r6" id="radioSuccess28" value="3">
                              <label style="font-size: 10pt;" for="radioSuccess28">
                                CUKUP
                              </label>
                            </div>
                            <div style="padding-right: 20px;" class="icheck-success d-inline">
                              <input type="radio" name="r6" id="radioSuccess29" value="4">
                              <label style="font-size: 10pt;" for="radioSuccess29">
                                BAIK
                              </label>
                            </div>
                            <div style="padding-right: 20px;" class="icheck-success d-inline">
                              <input type="radio" name="r6" id="radioSuccess30" value="5">
                              <label style="font-size: 10pt;" for="radioSuccess30">
                                TINGGI
                              </label>
                            </div>
                          </td>
                        </tr>
                        <tr>
                          <td rowspan="2"><center>2</center></td>
                          <td>Jumlah / Volume kerja (output) yang diselesaikan dalam jangka waktu tertentu, termasuk kecepatan kerja dan tingkat pengawasan yang diperlukan</td>
                        </tr>
                        <tr>
                          <td>
                            <div style="padding-right: 20px;" class="icheck-success d-inline">
                              <input type="radio" name="r7" id="radioSuccess31" value="1">
                              <label style="font-size: 10pt;" for="radioSuccess31">
                                RENDAH
                              </label>
                            </div>
                            <div style="padding-right: 20px;" class="icheck-success d-inline">
                              <input type="radio" name="r7" id="radioSuccess32" value="2">
                              <label style="font-size: 10pt;" for="radioSuccess32">
                                KURANG
                              </label>
                            </div>
                            <div style="padding-right: 20px;" class="icheck-success d-inline">
                              <input type="radio" name="r7" id="radioSuccess33" value="3">
                              <label style="font-size: 10pt;" for="radioSuccess33">
                                CUKUP
                              </label>
                            </div>
                            <div style="padding-right: 20px;" class="icheck-success d-inline">
                              <input type="radio" name="r7" id="radioSuccess34" value="4">
                              <label style="font-size: 10pt;" for="radioSuccess34">
                                BAIK
                              </label>
                            </div>
                            <div style="padding-right: 20px;" class="icheck-success d-inline">
                              <input type="radio" name="r7" id="radioSuccess35" value="5">
                              <label style="font-size: 10pt;" for="radioSuccess35">
                                TINGGI
                              </label>
                            </div>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                  <!-- /.table-responsive -->
                </div>
                <!-- /.card-body -->
              </div>
              <!-- /.card -->
              <!-- TABLE: LATEST ORDERS -->
              <div class="card card-olive">
                <div class="card-header">
                  <h3 class="card-title">SIAP KERJA</h3>
                  <div></div>
                  <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse">
                      <i class="fas fa-minus font-setting-4"></i>
                    </button>
                  </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body p-0">
                  <div class="table-responsive">
                    <table class="table-question m-0">
                      <thead>
                        <tr>
                          <th width="6%"><center>No</center></th>
                          <th><center>Aspect</center></th>
                        </tr>
                      </thead>
                      <tbody> 
                        <tr>
                          <td rowspan="2"><center>1</center></td>
                          <td>Cara Kerja, Kemauan untuk bekerja secara sistematis dan efisien</td>
                        </tr>
                        <tr>
                          <td>
                            <div style="padding-right: 20px;" class="icheck-success d-inline">
                              <input type="radio" name="r8" id="radioSuccess36" value="1">
                              <label style="font-size: 10pt;" for="radioSuccess36">
                                RENDAH
                              </label>
                            </div>
                            <div style="padding-right: 20px;" class="icheck-success d-inline">
                              <input type="radio" name="r8" id="radioSuccess37" value="2">
                              <label style="font-size: 10pt;" for="radioSuccess37">
                                KURANG
                              </label>
                            </div>
                            <div style="padding-right: 20px;" class="icheck-success d-inline">
                              <input type="radio" name="r8" id="radioSuccess38" value="3">
                              <label style="font-size: 10pt;" for="radioSuccess38">
                                CUKUP
                              </label>
                            </div>
                            <div style="padding-right: 20px;" class="icheck-success d-inline">
                              <input type="radio" name="r8" id="radioSuccess39" value="4">
                              <label style="font-size: 10pt;" for="radioSuccess39">
                                BAIK
                              </label>
                            </div>
                            <div style="padding-right: 20px;" class="icheck-success d-inline">
                              <input type="radio" name="r8" id="radioSuccess40" value="5">
                              <label style="font-size: 10pt;" for="radioSuccess40">
                                TINGGI
                              </label>
                            </div>
                          </td>
                        </tr>
                        <tr>
                          <td rowspan="2"><center>2</center></td>
                          <td>Komitmen, Rasa tanggung jawab terhadap pekerjaan, tingkat pengutamaan kepentingan pekerjaan diatas kepentingan lainnya</td>
                        </tr>
                        <tr>
                          <td>
                            <div style="padding-right: 20px;" class="icheck-success d-inline">
                              <input type="radio" name="r9" id="radioSuccess41" value="1">
                              <label style="font-size: 10pt;" for="radioSuccess41">
                                RENDAH
                              </label>
                            </div>
                            <div style="padding-right: 20px;" class="icheck-success d-inline">
                              <input type="radio" name="r9" id="radioSuccess42" value="2">
                              <label style="font-size: 10pt;" for="radioSuccess42">
                                KURANG
                              </label>
                            </div>
                            <div style="padding-right: 20px;" class="icheck-success d-inline">
                              <input type="radio" name="r9" id="radioSuccess43" value="3">
                              <label style="font-size: 10pt;" for="radioSuccess43">
                                CUKUP
                              </label>
                            </div>
                            <div style="padding-right: 20px;" class="icheck-success d-inline">
                              <input type="radio" name="r9" id="radioSuccess44" value="4">
                              <label style="font-size: 10pt;" for="radioSuccess44">
                                BAIK
                              </label>
                            </div>
                            <div style="padding-right: 20px;" class="icheck-success d-inline">
                              <input type="radio" name="r9" id="radioSuccess45" value="5">
                              <label style="font-size: 10pt;" for="radioSuccess45">
                                TINGGI
                              </label>
                            </div>
                          </td>
                        </tr>
                        <tr>
                          <td rowspan="2"><center>3</center></td>
                          <td>Kerja Sama, Kesediaan bekerja sama dan membantu memberi dan menerima masukan ke/dari atasan/rekan kerja/bawahan /orang lain</td>
                        </tr>
                        <tr>
                          <td>
                            <div style="padding-right: 20px;" class="icheck-success d-inline">
                              <input type="radio" name="r10" id="radioSuccess46" value="1">
                              <label style="font-size: 10pt;" for="radioSuccess46">
                                RENDAH
                              </label>
                            </div>
                            <div style="padding-right: 20px;" class="icheck-success d-inline">
                              <input type="radio" name="r10" id="radioSuccess47" value="2">
                              <label style="font-size: 10pt;" for="radioSuccess47">
                                KURANG
                              </label>
                            </div>
                            <div style="padding-right: 20px;" class="icheck-success d-inline">
                              <input type="radio" name="r10" id="radioSuccess48" value="3">
                              <label style="font-size: 10pt;" for="radioSuccess48">
                                CUKUP
                              </label>
                            </div>
                            <div style="padding-right: 20px;" class="icheck-success d-inline">
                              <input type="radio" name="r10" id="radioSuccess49" value="4">
                              <label style="font-size: 10pt;" for="radioSuccess49">
                                BAIK
                              </label>
                            </div>
                            <div style="padding-right: 20px;" class="icheck-success d-inline">
                              <input type="radio" name="r10" id="radioSuccess50" value="5">
                              <label style="font-size: 10pt;" for="radioSuccess50">
                                TINGGI
                              </label>
                            </div>
                          </td>
                        </tr>
                        <tr>
                          <td rowspan="2"><center>4</center></td>
                          <td>Keselamatan Kerja, Kesediaan untuk mementingkan keselamatan kerja diri sendiri maupun orang lain, merawat dan menggunakan peralatan kantor secara efisien</td>
                        </tr>
                        <tr>
                          <td>
                            <div style="padding-right: 20px;" class="icheck-success d-inline">
                              <input type="radio" name="r11" id="radioSuccess51" value="1">
                              <label style="font-size: 10pt;" for="radioSuccess51">
                                RENDAH
                              </label>
                            </div>
                            <div style="padding-right: 20px;" class="icheck-success d-inline">
                              <input type="radio" name="r11" id="radioSuccess52" value="2">
                              <label style="font-size: 10pt;" for="radioSuccess52">
                                KURANG
                              </label>
                            </div>
                            <div style="padding-right: 20px;" class="icheck-success d-inline">
                              <input type="radio" name="r11" id="radioSuccess53" value="3">
                              <label style="font-size: 10pt;" for="radioSuccess53">
                                CUKUP
                              </label>
                            </div>
                            <div style="padding-right: 20px;" class="icheck-success d-inline">
                              <input type="radio" name="r11" id="radioSuccess54" value="4">
                              <label style="font-size: 10pt;" for="radioSuccess54">
                                BAIK
                              </label>
                            </div>
                            <div style="padding-right: 20px;" class="icheck-success d-inline">
                              <input type="radio" name="r11" id="radioSuccess55" value="5">
                              <label style="font-size: 10pt;" for="radioSuccess55">
                                TINGGI
                              </label>
                            </div>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                  <!-- /.table-responsive -->
                </div>
                <!-- /.card-body -->
              </div>
              <!-- /.card -->
              <!-- TABLE: LATEST ORDERS -->
              <div class="card card-olive">
                <div class="card-body p-0">
                  <div class="table-responsive">
                    <table class="table-question m-0">
                      <tr>
                        <th>
                          <span>Keterangan tentang prestasi atau unjuk kerja:</span>
                          <textarea name="ket1" maxlength="1200" style="height: 150px;" class="form-control input_user border-list" type="text" name="kritik"></textarea>
                          <span>Hal-hal penting yang terjadi:</span>
                          <textarea name="ket2" maxlength="1200" style="height: 150px;" class="form-control input_user border-list" type="text" name="kritik"></textarea>
                          <span>Keterangan surat rekomendasi:</span>
                          <textarea name="ket3" maxlength="1200" style="height: 150px;" class="form-control input_user border-list" type="text" name="kritik"></textarea>
                          <br>
                          <div class="col-md-12">
                            <div class="form-group">
                              <label>Rekomendasi</label>
                              <select name="rekomendasi" class="form-control select2" style="width: 100%;">
                                <option selected="selected"></option>
                                <option value="Diangkat menjadi karyawan tetap setelah habis masa kerja kontrak.">Diangkat menjadi karyawan tetap setelah habis masa kerja kontrak.</option>
                                <option value="Diperpanjang kontrak selama 3 Bulan">Diperpanjang kontrak selama 3 Bulan</option>
                                <option value="Diperpanjang kontrak selama 6 Bulan">Diperpanjang kontrak selama 6 Bulan</option>
                                <option value="Diperpanjang kontrak selama 9 Bulan">Diperpanjang kontrak selama 9 Bulan</option>
                                <option value="Diperpanjang kontrak selama 12 Bulan">Diperpanjang kontrak selama 12 Bulan</option>
                                <option value="Diberhentikan setelah habis masa kerja kontrak">Diberhentikan setelah habis masa kerja kontrak</option>
                              </select>
                            </div>
                            <!-- /.form-group -->
                          </div>
                          <!-- /.col -->
                        </th>
                      </tr>
                    </table>
                    <div class="card-footer clearfix">
                      <input onclick="return confirm('Apakah anda yakin ingin menyimpan Penilaian?')" class="btn btn-primary float-right" type="submit" name="submit" value="Submit">
                      <a href="assuser.php" class="btn btn-dark button-left button-space">&nbsp;Back&nbsp;</a>
                    </div>
                    <!-- /.card-footer -->
                  </div>
                </div>
                <!-- /.card-body -->
              </div>
              <!-- /.card -->
            </form>
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div><!--/. container-fluid -->
    </section>
    <!-- /.content -->
    




  </div>
</div>
<!-- /.content-wrapper -->

<!-- Control Sidebar -->
<aside class="control-sidebar control-sidebar-dark">
  <!-- Control sidebar content goes here -->
</aside>
<!-- /.control-sidebar -->

<!-- /.content-wrapper -->
<footer class="main-footer">
  <strong>Copyright &copy; 2020.</strong> All rights
  reserved.
</footer>
</div>


<!-- jQuery -->
<script src="../plugins/jquery/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="../plugins/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- ChartJS -->
<script src="../plugins/chart.js/Chart.min.js"></script>
<!-- Sparkline -->
<script src="../plugins/sparklines/sparkline.js"></script>
<!-- JQVMap -->
<script src="../plugins/jqvmap/jquery.vmap.min.js"></script>
<script src="../plugins/jqvmap/maps/jquery.vmap.usa.js"></script>
<!-- jQuery Knob Chart -->
<script src="../plugins/jquery-knob/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="../plugins/moment/moment.min.js"></script>
<script src="../plugins/daterangepicker/daterangepicker.js"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="../plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
<!-- Summernote -->
<script src="../plugins/summernote/summernote-bs4.min.js"></script>
<!-- overlayScrollbars -->
<script src="../plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<!-- AdminLTE App -->
<script src="../dist/js/adminlte.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="../dist/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../dist/js/demo.js"></script>
</body>
</html>
