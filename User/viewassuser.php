<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <link rel="shortcut icon" type="image/x-icon" href="../gambar/logociputra.svg">
  <title>Ciputra</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="../plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

  <?php
  include '../koneksi.php';

  // mengaktifkan session
  session_start();
  if (!isset($_SESSION['userlogin'])) {
  // if($_SESSION['status'] != "login") {
    echo '<script language="javascript">alert("Dilarang akses, login sebagai user terlebih dahulu"); location.href="../logout.php"</script>';
  }

  $sql = "SELECT id, nama, email, foto FROM tb_user WHERE email='$_SESSION[userlogin]'";
  $qry = mysqli_query($koneksi, $sql) or die ("Query user salah!");
  $row = mysqli_fetch_array($qry);
  $iduser = $row['id'];

  $sql2 = "
  SELECT a.id AS id2, a.nama_karyawan, a.nik, a.id_user1, a.lokasi, a.departemen_karyawan, a.posisi_karyawan, a.golongan, b.id, b.email, b.nama, c.id_karyawan, c.status_penilaian AS sp1, c.banyak_penilaian AS bp1, c.id_proses, d.id_karyawan, d.status_penilaian AS sp2, d.banyak_penilaian AS bp2, d.id_proses, e.id_karyawan, e.status_penilaian AS sp3, e.banyak_penilaian AS bp3, e.id_proses, f.id_karyawan, f.hasil AS hk1p1, g.id_karyawan, g.hasil AS hk1p2, h.id_karyawan, h.hasil AS hk1p3, i.id_karyawan, i.hasil AS hk1p4, j.id_karyawan, j.hasil AS hk2p1, k.id_karyawan, k.hasil AS hk2p2, l.id_karyawan, l.hasil AS hk2p3, m.id_karyawan, m.hasil AS hk2p4, n.id_karyawan, n.hasil AS hk3p1, o.id_karyawan, o.hasil AS hk3p2, p.id_karyawan, p.hasil AS hk3p3, q.id_karyawan, q.hasil AS hk3p4 FROM tb_karyawan AS a INNER JOIN tb_user AS b ON a.id_user1=b.id INNER JOIN tb_kontrak1 AS c ON a.id=c.id_karyawan INNER JOIN tb_kontrak2 AS d ON a.id=d.id_karyawan INNER JOIN tb_kontrak3 AS e ON a.id=e.id_karyawan INNER JOIN tb_kon1_pen1 AS f ON a.id=f.id_karyawan INNER JOIN tb_kon1_pen2 AS g ON a.id=g.id_karyawan INNER JOIN tb_kon1_pen3 AS h ON a.id=h.id_karyawan INNER JOIN tb_kon1_pen4 AS i ON a.id=i.id_karyawan INNER JOIN tb_kon2_pen1 AS j ON a.id=j.id_karyawan INNER JOIN tb_kon2_pen2 AS k ON a.id=k.id_karyawan INNER JOIN tb_kon2_pen3 AS l ON a.id=l.id_karyawan INNER JOIN tb_kon2_pen4 AS m ON a.id=m.id_karyawan INNER JOIN tb_kon3_pen1 AS n ON a.id=n.id_karyawan INNER JOIN tb_kon3_pen2 AS o ON a.id=o.id_karyawan INNER JOIN tb_kon3_pen3 AS p ON a.id=p.id_karyawan INNER JOIN tb_kon3_pen4 AS q ON a.id=q.id_karyawan WHERE a.id_user1='$iduser' && f.hasil!='' || a.id_user1='$iduser' && g.hasil!='' || a.id_user1='$iduser' && h.hasil!='' || a.id_user1='$iduser' && i.hasil!='' || a.id_user1='$iduser' && j.hasil!='' || a.id_user1='$iduser' && k.hasil!='' || a.id_user1='$iduser' && l.hasil!='' || a.id_user1='$iduser' && m.hasil!='' || a.id_user1='$iduser' && n.hasil!='' || a.id_user1='$iduser' && o.hasil!='' || a.id_user1='$iduser' && p.hasil!='' || a.id_user1='$iduser' && q.hasil!=''";

  $qry2 = mysqli_query($koneksi, $sql2) or die ("Query karyawan salah!");

  ?>

</head>
<body class="hold-transition sidebar-mini">
  <div class="wrapper">
    <!-- Navbar -->
    <nav class="main-header navbar navbar-expand navbar-dark">
      <!-- Left navbar links -->
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
        </li>
      </ul>

      <!-- Right navbar links -->
      <ul class="navbar-nav ml-auto">
        <!-- Notifications Dropdown Menu -->
        <li class="nav-item dropdown">
          <a class="nav-link" data-toggle="dropdown" href="#">
            <span><?php echo $_SESSION['userlogin']; ?></span>
            <i class="fas fa-user-alt"></i>
          </a>
          <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
            <div class="dropdown-divider"></div>
            <a href="settinguser.php" class="dropdown-item">
              <i class="fas fa-cog mr-2"></i>
              <span class="float-right text-muted text-sm">Setting</span>
            </a>
            <div class="dropdown-divider"></div>
            <a href="../logout.php" class="dropdown-item">
              <i class="fas fa-sign-out-alt mr-2"></i>
              <span class="float-right text-muted text-sm">Logout</span>
            </a>
          </div>
        </li>
      </ul>
    </nav>
    <!-- /.navbar -->

    <!-- Main Sidebar Container -->
    <aside class="main-sidebar sidebar-light-olive elevation-4">
      <!-- Brand Logo -->
      <a href="homeuser.php" class="brand-link navbar-light">
        <img src="../gambar/logociputra2.png" alt="AdminLTE Logo" class="brand-image elevation-3"
        style="opacity: .8">
        <span class="brand-text font-weight-light"><b>CIPUTRA</b></span>
      </a>

      <!-- Sidebar -->
      <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
          <div class="image">
            <?php
            $cek_foto = $row['foto'];
            $tempat_foto = '../foto/'.$row['foto']; 
            if ($cek_foto) {
              echo "<img src='$tempat_foto' class='img-circle elevation-2' alt='User Image'>"; 
            }else{
              echo "<img src='../foto/blank.png'></a>";
            }
            ?>
          </div>
          <div class="info">
            <a href="#" class="d-block"><?php echo $row['nama']; ?></a>
          </div>
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2">
          <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
           with font-awesome or any other icon font library -->
           <li class="nav-item">
            <a href="assuser.php" class="nav-link">
              <i class="nav-icon fas fa-edit"></i>
              <p>
                Assessment
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="viewassuser.php" class="nav-link active">
              <i class="nav-icon fas fa-clipboard-list"></i>
              <p>
                View Assessment
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="settinguser.php" class="nav-link">
              <i class="nav-icon fas fa-cog"></i>
              <p>
                Setting
              </p>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>View Assessment</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="homeuser.php">Home</a></li>
              <li class="breadcrumb-item active">View Assessment</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- /.row -->
        <div class="row">
          <div class="col-12">
            <div class="card card-olive">
              <div class="card-header">
                <h3 class="card-title">View Employee Assessment</h3>

              </div>
              <!-- /.card-header -->
              <div class="card-body table-responsive p-0">
                <table class="table table-bordered table-hover text-nowrap">
                  <thead>
                    <tr>
                      <th><center>No</center></th>
                      <th><center>Name Employee</center></th>
                      <th><center>NIK</center></th>
                      <th><center>Location Employment</center></th>
                      <th><center>Departement</center></th>
                      <th><center>Position</center></th>
                      <th><center>Golongan</center></th>
                      <th><center>Action</center></th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                    $no = 0;
                    while ($row2 = mysqli_fetch_array($qry2)) {
                      $no++;
                      ?>
                      <tr>
                        <td><?php echo $no;?></td>
                        <td><?php echo $row2['nama_karyawan'];?></td>
                        <td><?php echo $row2['nik'];?></td>
                        <td><?php echo $row2['lokasi'];?></td>
                        <td><?php echo $row2['departemen_karyawan'];?></td>
                        <td><?php echo $row2['posisi_karyawan'];?></td>
                        <td><?php echo $row2['golongan'];;?></td>
                        <td width="8%"><a href="editassuser.php?nik=<?php echo $row2['nik'];?>" class="btn btn-info float-right button-space"><i class='font-setting-4 fas fa-edit'></i></a></td>
                      </tr>
                    <?php }?>
                  </tbody>
                </table>
                <b>*Lihat paling kanan untuk proses Edit/Print</b>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="float-right d-none d-sm-block">
      <b>Version</b> 3.0.4
    </div>
    <strong>Copyright &copy; 2020</strong> All rights
    reserved.
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="../plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="../dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../dist/js/demo.js"></script>
</body>
</html>
