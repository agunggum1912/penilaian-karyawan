<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <link rel="shortcut icon" type="image/x-icon" href="gambar/logociputra.svg">
  <title>Ciputra</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- CSS Manual -->
  <!-- <link rel="stylesheet" type="text/css" href="assets/css/style.css"> -->

  <!-- Font Awesome -->
  <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- daterange picker -->
  <link rel="stylesheet" href="plugins/daterangepicker/daterangepicker.css">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css">
  <!-- Tempusdominus Bbootstrap 4 -->
  <link rel="stylesheet" href="plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="plugins/select2/css/select2.min.css">
  <link rel="stylesheet" href="plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
  <!-- Bootstrap4 Duallistbox -->
  <link rel="stylesheet" href="plugins/bootstrap4-duallistbox/bootstrap-duallistbox.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

  <?php
  include 'koneksi.php';

  // mengaktifkan session
  session_start();
  if (!isset($_SESSION['adminlogin'])) {
  // if($_SESSION['status'] != "login") {
    echo '<script language="javascript">alert("Dilarang akses, login sebagai admin terlebih dahulu"); location.href="logout.php"</script>';
  }

  $sql = "SELECT nama, email, foto FROM tb_user WHERE email='$_SESSION[adminlogin]'";
  $qry = mysqli_query($koneksi, $sql) or die ("Query user salah!");
  $row = mysqli_fetch_array($qry);


  $nik = $_GET['nik'];

  $sqlkaryawan = "SELECT id, nama_karyawan, nik, tgl_masuk, status_karyawan, departemen_karyawan, posisi_karyawan, golongan, lokasi, id_user1, id_user2 FROM tb_karyawan WHERE nik='$nik'";
  $qrykaryawan = mysqli_query($koneksi, $sqlkaryawan) or die ("Query karyawan salah!");
  $rowkaryawan = mysqli_fetch_array($qrykaryawan);
  $id = $rowkaryawan['id'];
  $nama_karyawan = $rowkaryawan['nama_karyawan'];
  $iduser1 = $rowkaryawan['id_user1'];
  $iduser2 = $rowkaryawan['id_user2'];
  $tglmasuk = date("d-m-Y", strtotime($rowkaryawan['tgl_masuk']));

  $sqlkontrak1 = "SELECT a.id, a.nama_karyawan, a.nik, b.id_karyawan, b.mulai_penilaian AS mpk1p1, b.selesai_penilaian AS spk1p1, b.hasil AS hk1p1, c.id_karyawan, c.mulai_penilaian AS mpk1p2, c.selesai_penilaian AS spk1p2, c.hasil AS hk1p2, d.id_karyawan, d.mulai_penilaian AS mpk1p3, d.selesai_penilaian AS spk1p3, d.hasil AS hk1p3, e.id_karyawan, e.mulai_penilaian AS mpk1p4, e.selesai_penilaian AS spk1p4, e.hasil AS hk1p4, f.id_karyawan, f.status_penilaian AS sp1, f.banyak_penilaian AS bp1 FROM tb_karyawan AS a LEFT JOIN tb_kon1_pen1 AS b ON a.id=b.id_karyawan LEFT JOIN tb_kon1_pen2 AS c ON a.id=c.id_karyawan LEFT JOIN tb_kon1_pen3 AS d ON a.id=d.id_karyawan LEFT JOIN tb_kon1_pen4 AS e ON a.id=e.id_karyawan LEFT JOIN tb_kontrak1 AS f ON a.id=f.id_karyawan WHERE a.nik='$nik'";
  $qrykontrak1 = mysqli_query($koneksi, $sqlkontrak1);
  $rowkontrak1 = mysqli_fetch_array($qrykontrak1);

  $sqlkontrak2 = "SELECT a.id, a.nama_karyawan, a.nik, b.id_karyawan, b.mulai_penilaian AS mpk2p1, b.selesai_penilaian AS spk2p1, b.hasil AS hk2p1, c.id_karyawan, c.mulai_penilaian AS mpk2p2, c.selesai_penilaian AS spk2p2, c.hasil AS hk2p2, d.id_karyawan, d.mulai_penilaian AS mpk2p3, d.selesai_penilaian AS spk2p3, d.hasil AS hk2p3, e.id_karyawan, e.mulai_penilaian AS mpk2p4, e.selesai_penilaian AS spk2p4, e.hasil AS hk2p4, f.id_karyawan, f.status_penilaian AS sp2, f.banyak_penilaian AS bp2 FROM tb_karyawan AS a LEFT JOIN tb_kon2_pen1 AS b ON a.id=b.id_karyawan LEFT JOIN tb_kon2_pen2 AS c ON a.id=c.id_karyawan LEFT JOIN tb_kon2_pen3 AS d ON a.id=d.id_karyawan LEFT JOIN tb_kon2_pen4 AS e ON a.id=e.id_karyawan LEFT JOIN tb_kontrak2 AS f ON a.id=f.id_karyawan WHERE a.nik='$nik'";
  $qrykontrak2 = mysqli_query($koneksi, $sqlkontrak2);
  $rowkontrak2 = mysqli_fetch_array($qrykontrak2);

  $sqlkontrak3 = "SELECT a.id, a.nama_karyawan, a.nik, b.id_karyawan, b.mulai_penilaian AS mpk3p1, b.selesai_penilaian AS spk3p1, b.hasil AS hk3p1, c.id_karyawan, c.mulai_penilaian AS mpk3p2, c.selesai_penilaian AS spk3p2, c.hasil AS hk3p2, d.id_karyawan, d.mulai_penilaian AS mpk3p3, d.selesai_penilaian AS spk3p3, d.hasil AS hk3p3, e.id_karyawan, e.mulai_penilaian AS mpk3p4, e.selesai_penilaian AS spk3p4, e.hasil AS hk3p4, f.id_karyawan, f.status_penilaian AS sp3, f.banyak_penilaian AS bp3 FROM tb_karyawan AS a LEFT JOIN tb_kon3_pen1 AS b ON a.id=b.id_karyawan LEFT JOIN tb_kon3_pen2 AS c ON a.id=c.id_karyawan LEFT JOIN tb_kon3_pen3 AS d ON a.id=d.id_karyawan LEFT JOIN tb_kon3_pen4 AS e ON a.id=e.id_karyawan LEFT JOIN tb_kontrak3 AS f ON a.id=f.id_karyawan WHERE a.nik='$nik'";
  $qrykontrak3 = mysqli_query($koneksi, $sqlkontrak3);
  $rowkontrak3 = mysqli_fetch_array($qrykontrak3);


  $sqluser = "SELECT id, nama, departemen FROM tb_user ORDER BY nama ASC";
  $qryuser = mysqli_query($koneksi, $sqluser);

  $sql_user1 = "SELECT id, nama, departemen FROM tb_user WHERE id='$iduser1'";
  $qry_user1 = mysqli_query($koneksi, $sql_user1);
  $row_user1 = mysqli_fetch_array($qry_user1);

  $sqluser2 = "SELECT id, nama, departemen FROM tb_user ORDER BY nama ASC";
  $qryuser2 = mysqli_query($koneksi, $sqluser2);

  $sql_user2 = "SELECT id, nama, departemen FROM tb_user WHERE id='$iduser2'";
  $qry_user2 = mysqli_query($koneksi, $sql_user2);
  $row_user2 = mysqli_fetch_array($qry_user2);


  if (isset($_POST['submit'])) {
    // error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
    $id2 = $_POST['id'];
    $nama = trim($_POST['nama']);
    $nik2 = trim($_POST['nik']);
    $tgl_masuk = trim($_POST['tgl_masuk']);
    $departemen = trim($_POST['departemen']);
    $posisi = trim($_POST['posisi']);
    $golongan = trim($_POST['golongan']);
    $status_karyawan = trim($_POST['status_karyawan']);
    $lokasi = trim($_POST['lokasi']);
    $user_proses = trim($_POST['user_proses']);
    $user_review = trim($_POST['user_review']);

    function ubahTanggal1($tgl_masuk){
      $pisah1 = explode('/',$tgl_masuk);
      $array1 = array($pisah1[2],$pisah1[1],$pisah1[0]);
      $satukan1 = implode('-',$array1);
      return $satukan1;
    }

    $tgl_masuk2 = ubahTanggal1($tgl_masuk);


    $cek = "SELECT nik FROM tb_karyawan WHERE nik='$nik'";
    $cekqry = mysqli_query($koneksi, $cek);
    $cekrow = mysqli_fetch_array($cekqry);

    if ($rowkaryawan['nama_karyawan'] == $nama) {
      if ($rowkaryawan['nik'] == $nik2) {
        if ($rowkaryawan['tgl_masuk'] == $tgl_masuk2) {
          if ($rowkaryawan['status_karyawan'] == $status_karyawan) {
            if ($rowkaryawan['departemen_karyawan'] == $departemen) {
              if ($rowkaryawan['posisi_karyawan'] == $posisi) {
                if ($rowkaryawan['golongan'] == $golongan) {
                  if ($rowkaryawan['lokasi'] == $lokasi) {
                    if($rowkaryawan['id_user1'] == $user_proses) {
                      if($rowkaryawan['id_user2'] == $user_review) {
                        echo "<script>alert('Tidak ada data yang berubah!');window.location='viewass.php'; </script>";
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }


    if (empty($nama) && empty($nik2) && empty($tgl_masuk2) && empty($status_karyawan) && empty($departemen) && empty($posisi) && empty($golongan) && empty($lokasi) && empty($user_proses)&& empty($user_review)) {
      echo "<script>alert('Data masih kosong!');history.go(-1)</script>";
    }elseif (empty($nama)) {
      echo "<script>alert('Name Employment harus di isi!');history.go(-1)</script>";
    }elseif (empty($nik2)) {
      echo "<script>alert('NIK harus di isi!');history.go(-1)</script>";
    }elseif (empty($tgl_masuk)) {
      echo "<script>alert('Date Join harus di isi!');history.go(-1)</script>";
    }elseif (empty($status_karyawan)) {
      echo "<script>alert('Employment Status harus di isi!');history.go(-1)</script>";
    }elseif (empty($departemen)) {
      echo "<script>alert('Departement harus di isi!');history.go(-1)</script>";
    }elseif (empty($posisi)) {
      echo "<script>alert('Position harus di isi!');history.go(-1)</script>";  
    }elseif (empty($golongan)) {
      echo "<script>alert('Golongan harus di isi!');history.go(-1)</script>";
    }elseif (empty($user_proses)) {
      echo "<script>alert('Name User Process harus di isi!');history.go(-1)</script>"; 
    }elseif (empty($user_review)) {
      echo "<script>alert('Name User Review harus di isi!');history.go(-1)</script>"; 
    }elseif (!preg_match("/^[a-zA-Z ]*$/", $nama)) {
      echo "<script>alert('Name Employment tidak boleh menganduk special char dan angka!');history.go(-1)</script>";
    }elseif (strlen($nama) >= 60) {
      echo "<script>alert('Panjang Name Employment tidak boleh 60 Character!');history.go(-1)</script>";
    }elseif (strlen($nik) <= 7 && strlen($nik) >= 11) {
      echo "<script>alert('Panjang NIK tidak boleh lebih kecil dari 8 character atau lebih besar 10 character!');history.go(-1)</script>";
    }elseif ($status_karyawan != "Kontrak 1" && $status_karyawan != "Kontrak 2" && $status_karyawan != "Kontrak 3") {
      echo "<script>alert('Employment Status Salah!');history.go(-1)</script>";
    }elseif (strlen($golongan) >= 3) {
      echo "<script>alert('Panjang Golongan tidak boleh lebih besar dari 2!');history.go(-1)</script>";
    }else{
      $sqlupdate = "UPDATE tb_karyawan SET nik='$nik2', nama_karyawan='$nama', id_user1='$user_proses', id_user2='$user_review', tgl_masuk='$tgl_masuk2', status_karyawan='$status_karyawan', lokasi='$lokasi', departemen_karyawan='$departemen', posisi_karyawan='$posisi', golongan='$golongan' WHERE id='$id2'";
      $hasilupdate = mysqli_query($koneksi, $sqlupdate);
      echo "<script>alert('Employment Assessment telah berhasil diubah.');window.location='viewass.php'; </script>";
    }
  }

  ?>

</head>
<body class="hold-transition sidebar-mini layout-fixed">

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-dark">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
      <!-- Notifications Dropdown Menu -->
      <li class="nav-item dropdown">
        <a class="nav-link" data-toggle="dropdown" href="#">
          <span><?php echo $_SESSION['adminlogin']; ?></span>
          <i class="fas fa-user-alt"></i>
        </a>
        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
          <div class="dropdown-divider"></div>
          <a href="setting.php" class="dropdown-item">
            <i class="fas fa-cog mr-2"></i>
            <span class="float-right text-muted text-sm">Setting</span>
          </a>
          <div class="dropdown-divider"></div>
          <a href="logout.php" class="dropdown-item">
            <i class="fas fa-sign-out-alt mr-2"></i>
            <span class="float-right text-muted text-sm">Logout</span>
          </a>
        </div>
      </li>
    </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-light-olive elevation-4">
    <!-- Brand Logo -->
    <a href="index.php" class="brand-link navbar-light">
      <img src="gambar/logociputra2.png" alt="AdminLTE Logo" class="brand-image elevation-3"
      style="opacity: .8">
      <span class="brand-text font-weight-light"><b>CIPUTRA</b></span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <?php
          $cek_foto = $row['foto'];
          $tempat_foto = 'foto/'.$row['foto']; 
          if ($cek_foto) {
            echo "<img src='$tempat_foto' class='img-circle elevation-2' alt='User Image'>"; 
          }else{
            echo "<img src='foto/blank.png'></a>";
          }
          ?>
        </div>
        <div class="info">
          <a href="#" class="d-block"><?php echo $row['nama']; ?></a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
           with font-awesome or any other icon font library -->
           <li class="nav-item">
            <a href="createass.php" class="nav-link">
              <i class="nav-icon fas fa-edit"></i>
              <p>
                Create Assessment
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="viewass.php" class="nav-link active">
              <i class="nav-icon fas fa-clipboard-list"></i>
              <p>
                View Assessment
              </p>
            </a>
          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-users"></i>
              <p>
                Manage User Id
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="adduserid.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Add User Id</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="updateuserid.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Update User Id</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item">
            <a href="setting.php" class="nav-link">
              <i class="nav-icon fas fa-cog"></i>
              <p>
                Setting
              </p>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Edit Assessment</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="index.php">Home</a></li>
              <li class="breadcrumb-item"><a href="viewass.php">View Assessment</a></li>
              <li class="breadcrumb-item active">Edit Assessment</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- /.row -->
        <div class="row">
          <div class="col-12">
            <div class="card card-olive">
              <div class="card-header">
                <h3 class="card-title">Contract 1</h3>

                <div class="card-tools">
                  <a href="kontrak1.php?id_karyawan=<?php echo $id;?>" type="button" class="btn btn-tool"><i class="fas fa-edit"></i></a>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body table-responsive p-0">
                <?php if ($rowkontrak1['mpk1p1'] > 0) {;?>
                  <table class="table table-bordered table-hover text-nowrap">
                    <thead>
                      <tr>
                        <th style="padding-left: 12px;" width="6%"><center>No</center></th>
                        <th><center>Name</center></th>
                        <th><center>NIK</center></th>
                        <th><center>Assessment Status</center></th>
                        <th><center>Start Assessment</center></th>
                        <th><center>Finish Assessment</center></th>
                        <th><center>Result User</center></th>
                        <th style="padding-right: 12px;" width="10%"><center>Action</center></th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>1</td>
                        <td><?php echo $rowkontrak1['nama_karyawan'];?></td>
                        <td><?php echo $rowkontrak1['nik'];?></td>
                        <td>Assessment 1</td>
                        <td><?php echo date("d-m-Y", strtotime($rowkontrak1['mpk1p1']));?></td>
                        <td><?php echo date("d-m-Y", strtotime($rowkontrak1['spk1p1']));?></td>
                        <td><center><?php echo $rowkontrak1['hk1p1'];?></center></td>
                        <td>
                          <div style="padding-left: 12px;" class="btn-group">
                            <a href="delete-penilaian.php?id=<?php echo $id;?>.1.1" onclick="return confirm('Anda yakin ingin menghapus Penilaian 1?')" class="btn btn-danger"><i class='fas fa-trash-alt'></i></a>
                            <a href="<?php if($rowkontrak1['sp1'] == 1 && $rowkontrak1['bp1'] == 1){echo "printass2";}else{echo "printass";};?>.php?id=<?php echo $id;?>.1.1" class="btn btn-primary"><i class='fas fa-print'></i></a>
                          </div>
                        </td>
                      </tr>
                    <?php };?>
                    <?php if ($rowkontrak1['mpk1p2'] > 0) {;?>
                      <tr>
                        <td>2</td>
                        <td><?php echo $rowkontrak1['nama_karyawan'];?></td>
                        <td><?php echo $rowkontrak1['nik'];?></td>
                        <td>Assessment 2</td>
                        <td><?php echo date("d-m-Y", strtotime($rowkontrak1['mpk1p2']));?></td>
                        <td><?php echo date("d-m-Y", strtotime($rowkontrak1['spk1p2']));?></td>
                        <td><center><?php echo $rowkontrak1['hk1p2'];?></center></td>
                        <td>
                          <div style="padding-left: 12px;" class="btn-group">
                            <a href="delete-penilaian.php?id=<?php echo $id;?>.1.2" onclick="return confirm('Anda yakin ingin menghapus Penilaian 2?')" class="btn btn-danger"><i class='fas fa-trash-alt'></i></a>
                            <a href="<?php if($rowkontrak1['sp1'] == 2 && $rowkontrak1['bp1'] == 2){echo "printass2";}else{echo "printass";};?>.php?id=<?php echo $id;?>.1.2" class="btn btn-primary"><i class='fas fa-print'></i></a>
                          </div>
                        </td>
                      </tr>
                    <?php };?>
                    <?php if ($rowkontrak1['mpk1p3'] > 0) {;?>
                      <tr>
                        <td>3</td>
                        <td><?php echo $rowkontrak1['nama_karyawan'];?></td>
                        <td><?php echo $rowkontrak1['nik'];?></td>
                        <td>Assessment 3</td>
                        <td><?php echo date("d-m-Y", strtotime($rowkontrak1['mpk1p3']));?></td>
                        <td><?php echo date("d-m-Y", strtotime($rowkontrak1['spk1p3']));?></td>
                        <td><center><?php echo $rowkontrak1['hk1p3'];?></center></td>
                        <td>
                          <div style="padding-left: 12px;" class="btn-group">
                            <a href="delete-penilaian.php?id=<?php echo $id;?>.1.3" onclick="return confirm('Anda yakin ingin menghapus Penilaian 3?')" class="btn btn-danger"><i class='fas fa-trash-alt'></i></a>
                            <a href="<?php if($rowkontrak1['sp1'] == 3 && $rowkontrak1['bp1'] == 3){echo "printass2";}else{echo "printass";};?>.php?id=<?php echo $id;?>.1.3" class="btn btn-primary"><i class='fas fa-print'></i></a>
                          </div>
                        </td>
                      </tr>
                    <?php };?>
                    <?php if ($rowkontrak1['mpk1p4'] > 0) {;?>
                      <tr>
                        <td>4</td>
                        <td><?php echo $rowkontrak1['nama_karyawan'];?></td>
                        <td><?php echo $rowkontrak1['nik'];?></td>
                        <td>Assessment 4</td>
                        <td><?php echo date("d-m-Y", strtotime($rowkontrak1['mpk1p4']));?></td>
                        <td><?php echo date("d-m-Y", strtotime($rowkontrak1['spk1p4']));?></td>
                        <td><center><?php echo $rowkontrak1['hk1p4'];?></center></td>
                        <td>
                          <div style="padding-left: 12px;" class="btn-group">
                            <a href="delete-penilaian.php?id=<?php echo $id;?>.1.4" onclick="return confirm('Anda yakin ingin menghapus Penilaian 4?')" class="btn btn-danger"><i class='fas fa-trash-alt'></i></a>
                            <a href="<?php if($rowkontrak1['sp1'] == 4 && $rowkontrak1['bp1'] == 4){echo "printass2";}else{echo "printass";};?>.php?id=<?php echo $id;?>.1.4" class="btn btn-primary"><i class='fas fa-print'></i></a>
                          </div>
                        </td>
                      </tr>
                    <?php };?>
                  </tbody>
                </table>
                <b>*Lihat paling kanan untuk proses Delete/Print</b>
                <br>
                <b>*Lihat paling bawah untuk merubah data karyawan</b>
                <br>
                <b>*Icon Edit berada pojok kanan atas tabel untuk merubah tanggal penilaian</b>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
        </div>


        <div class="row">
          <div class="col-12">
            <div class="card card-olive">
              <div class="card-header">
                <h3 class="card-title">Contract 2</h3>

                <div class="card-tools">
                  <a href="kontrak2.php?id_karyawan=<?php echo $id;?>" type="button" class="btn btn-tool"><i class="fas fa-edit"></i></a>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body table-responsive p-0">
                <?php if ($rowkontrak2['mpk2p1'] > 0) {;?>
                  <table class="table table-bordered table-hover text-nowrap">
                    <thead>
                      <tr>
                        <th style="padding-left: 12px;" width="6%"><center>No</center></th>
                        <th><center>Name</center></th>
                        <th><center>NIK</center></th>
                        <th><center>Assessment Status</center></th>
                        <th><center>Start Assessment</center></th>
                        <th><center>Finish Assessment</center></th>
                        <th><center>Result User</center></th>
                        <th style="padding-right: 12px;" width="10%"><center>Action</center></th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>1</td>
                        <td><?php echo $rowkontrak2['nama_karyawan'];?></td>
                        <td><?php echo $rowkontrak2['nik'];?></td>
                        <td>Assessment 1</td>
                        <td><?php echo date("d-m-Y", strtotime($rowkontrak2['mpk2p1']));?></td>
                        <td><?php echo date("d-m-Y", strtotime($rowkontrak2['spk2p1']));?></td>
                        <td><center><?php echo $rowkontrak2['hk2p1'];?></center></td>
                        <td>
                          <div style="padding-left: 12px;" class="btn-group">
                            <a href="delete-userid-proses.php?id=<?php echo $id;?>.2.1" onclick="return confirm('Anda yakin ingin menghapus Penilaian 1?')" class="btn btn-danger"><i class='fas fa-trash-alt'></i></a>
                            <a href="<?php if($rowkontrak2['sp2'] == 1 && $rowkontrak2['bp2'] == 1){echo "printass2";}else{echo "printass";};?>.php?id=<?php echo $id;?>.2.1" class="btn btn-primary"><i class='fas fa-print'></i></a>
                          </div>
                        </td>
                      </tr>
                    <?php };?>
                    <?php if ($rowkontrak2['mpk2p2'] > 0) {;?>
                      <tr>
                        <td>2</td>
                        <td><?php echo $rowkontrak2['nama_karyawan'];?></td>
                        <td><?php echo $rowkontrak2['nik'];?></td>
                        <td>Assessment 2</td>
                        <td><?php echo date("d-m-Y", strtotime($rowkontrak2['mpk2p2']));?></td>
                        <td><?php echo date("d-m-Y", strtotime($rowkontrak2['spk2p2']));?></td>
                        <td><center><?php echo $rowkontrak2['hk2p2'];?></center></td>
                        <td>
                          <div style="padding-left: 12px;" class="btn-group">
                            <a href="delete-userid-proses.php?id=<?php echo $id;?>.2.2" onclick="return confirm('Anda yakin ingin menghapus Penilaian 2?')" class="btn btn-danger"><i class='fas fa-trash-alt'></i></a>
                            <a href="<?php if($rowkontrak2['sp2'] == 2 && $rowkontrak2['bp2'] == 2){echo "printass2";}else{echo "printass";};?>.php?id=<?php echo $id;?>.2.2" class="btn btn-primary"><i class='fas fa-print'></i></a>
                          </div>
                        </td>
                      </tr>
                    <?php };?>
                    <?php if ($rowkontrak2['mpk2p3'] > 0) {;?>
                      <tr>
                        <td>3</td>
                        <td><?php echo $rowkontrak2['nama_karyawan'];?></td>
                        <td><?php echo $rowkontrak2['nik'];?></td>
                        <td>Assessment 3</td>
                        <td><?php echo date("d-m-Y", strtotime($rowkontrak2['mpk2p3']));?></td>
                        <td><?php echo date("d-m-Y", strtotime($rowkontrak2['spk2p3']));?></td>
                        <td><center><?php echo $rowkontrak2['hk2p3'];?></center></td>
                        <td>
                          <div style="padding-left: 12px;" class="btn-group">
                            <a href="delete-userid-proses.php?id=<?php echo $id;?>.2.3" onclick="return confirm('Anda yakin ingin menghapus Penilaian 3?')" class="btn btn-danger"><i class='fas fa-trash-alt'></i></a>
                            <a href="<?php if($rowkontrak2['sp2'] == 3 && $rowkontrak2['bp2'] == 3){echo "printass2";}else{echo "printass";};?>.php?id=<?php echo $id;?>.2.3" class="btn btn-primary"><i class='fas fa-print'></i></a>
                          </div>
                        </td>
                      </tr>
                    <?php };?>
                    <?php if ($rowkontrak2['mpk2p4'] > 0) {;?>
                      <tr>
                        <td>4</td>
                        <td><?php echo $rowkontrak2['nama_karyawan'];?></td>
                        <td><?php echo $rowkontrak2['nik'];?></td>
                        <td>Assessment 4</td>
                        <td><?php echo date("d-m-Y", strtotime($rowkontrak2['mpk2p4']));?></td>
                        <td><?php echo date("d-m-Y", strtotime($rowkontrak2['spk2p4']));?></td>
                        <td><center><?php echo $rowkontrak1['hk2p4'];?></center></td>
                        <td>
                          <div style="padding-left: 12px;" class="btn-group">
                            <a href="delete-userid-proses.php?id=<?php echo $id;?>.2.4" onclick="return confirm('Anda yakin ingin menghapus Penilaian 4?')" class="btn btn-danger"><i class='fas fa-trash-alt'></i></a>
                            <a href="<?php if($rowkontrak2['sp2'] == 4 && $rowkontrak2['bp2'] == 4){echo "printass2";}else{echo "printass";};?>.php?id=<?php echo $id;?>.2.4" class="btn btn-primary"><i class='fas fa-print'></i></a>
                          </div>
                        </td>
                      </tr>
                    <?php };?>
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
        </div>

        <div class="row">
          <div class="col-12">
            <div class="card card-olive">
              <div class="card-header">
                <h3 class="card-title">Contract 3</h3>

                <div class="card-tools">
                  <a href="kontrak3.php?id_karyawan=<?php echo $id;?>" type="button" class="btn btn-tool"><i class="fas fa-edit"></i></a>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body table-responsive p-0">
                <?php if ($rowkontrak3['mpk3p1'] > 0) {;?>
                  <table class="table table-bordered table-hover text-nowrap">
                    <thead>
                      <tr>
                        <th style="padding-left: 12px;" width="6%"><center>No</center></th>
                        <th><center>Name</center></th>
                        <th><center>NIK</center></th>
                        <th><center>Assessment Status</center></th>
                        <th><center>Start Assessment</center></th>
                        <th><center>Finish Assessment</center></th>
                        <th><center>Result User</center></th>
                        <th style="padding-right: 12px;" width="10%"><center>Action</center></th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>1</td>
                        <td><?php echo $rowkontrak3['nama_karyawan'];?></td>
                        <td><?php echo $rowkontrak3['nik'];?></td>
                        <td>Assessment 1</td>
                        <td><?php echo date("d-m-Y", strtotime($rowkontrak3['mpk3p1']));?></td>
                        <td><?php echo date("d-m-Y", strtotime($rowkontrak3['spk3p1']));?></td>
                        <td><center><?php echo $rowkontrak3['hk3p1'];?></center></td>
                        <td>
                          <div style="padding-left: 12px;" class="btn-group">
                            <a href="delete-userid-proses.php?id=<?php echo $id;?>.3.1" onclick="return confirm('Anda yakin ingin menghapus Penilaian 1?')" class="btn btn-danger"><i class='fas fa-trash-alt'></i></a>
                            <a href="<?php if($rowkontrak3['sp3'] == 1 && $rowkontrak3['bp3'] == 1){echo "printass2";}else{echo "printass";};?>.php?id=<?php echo $id;?>.3.1" class="btn btn-primary"><i class='fas fa-print'></i></a>
                          </div>
                        </td>
                      </tr>
                    <?php };?>
                    <?php if ($rowkontrak3['mpk3p2'] > 0) {;?>
                      <tr>
                        <td>2</td>
                        <td><?php echo $rowkontrak3['nama_karyawan'];?></td>
                        <td><?php echo $rowkontrak3['nik'];?></td>
                        <td>Assessment 2</td>
                        <td><?php echo date("d-m-Y", strtotime($rowkontrak3['mpk3p2']));?></td>
                        <td><?php echo date("d-m-Y", strtotime($rowkontrak3['spk3p2']));?></td>
                        <td><center><?php echo $rowkontrak3['hk3p2'];?></center></td>
                        <td>
                          <div style="padding-left: 12px;" class="btn-group">
                            <a href="delete-userid-proses.php?id=<?php echo $id;?>.3.2" onclick="return confirm('Anda yakin ingin menghapus Penilaian 2?')" class="btn btn-danger"><i class='fas fa-trash-alt'></i></a>
                            <a href="<?php if($rowkontrak3['sp3'] == 2 && $rowkontrak3['bp3'] == 2){echo "printass2";}else{echo "printass";};?>.php?id=<?php echo $id;?>.3.2" class="btn btn-primary"><i class='fas fa-print'></i></a>
                          </div>
                        </td>
                      </tr>
                    <?php };?>
                    <?php if ($rowkontrak3['mpk3p3'] > 0) {;?>
                      <tr>
                        <td>3</td>
                        <td><?php echo $rowkontrak3['nama_karyawan'];?></td>
                        <td><?php echo $rowkontrak3['nik'];?></td>
                        <td>Assessment 3</td>
                        <td><?php echo date("d-m-Y", strtotime($rowkontrak3['mpk3p3']));?></td>
                        <td><?php echo date("d-m-Y", strtotime($rowkontrak3['spk3p3']));?></td>
                        <td><center><?php echo $rowkontrak3['hk3p3'];?></center></td>
                        <td>
                          <div style="padding-left: 12px;" class="btn-group">
                            <a href="delete-userid-proses.php?id=<?php echo $id;?>.3.3" onclick="return confirm('Anda yakin ingin menghapus Penilaian 3?')" class="btn btn-danger"><i class='fas fa-trash-alt'></i></a>
                            <a href="<?php if($rowkontrak3['sp3'] == 3 && $rowkontrak3['bp3'] == 3){echo "printass2";}else{echo "printass";};?>.php?id=<?php echo $id;?>.3.3" class="btn btn-primary"><i class='fas fa-print'></i></a>
                          </div>
                        </td>
                      </tr>
                    <?php };?>
                    <?php if ($rowkontrak3['mpk3p4'] > 0) {;?>
                      <tr>
                        <td>4</td>
                        <td><?php echo $rowkontrak3['nama_karyawan'];?></td>
                        <td><?php echo $rowkontrak3['nik'];?></td>
                        <td>Assessment 4</td>
                        <td><?php echo date("d-m-Y", strtotime($rowkontrak3['mpk3p4']));?></td>
                        <td><?php echo date("d-m-Y", strtotime($rowkontrak3['spk3p4']));?></td>
                        <td><center><?php echo $rowkontrak1['hk3p4'];?></center></td>
                        <td>
                          <div style="padding-left: 12px;" class="btn-group">
                            <a href="delete-userid-proses.php?id=<?php echo $id;?>.3.4" onclick="return confirm('Anda yakin ingin menghapus Penilaian 4?')" class="btn btn-danger"><i class='fas fa-trash-alt'></i></a>
                            <a href="<?php if($rowkontrak3['sp3'] == 4 && $rowkontrak3['bp3'] == 4){echo "printass2";}else{echo "printass";};?>.php?id=<?php echo $id;?>.3.4" class="btn btn-primary"><i class='fas fa-print'></i></a>
                          </div>
                        </td>
                      </tr>
                    <?php };?>
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
        </div>
        

        <form action="" method="post">
          <!-- SELECT2 EXAMPLE -->
          <div class="card card-olive">
            <div class="card-header">
              <h3 class="card-title">Create New Employee Assessment</h3>

              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
              </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <div class="row">
                <div class="col-md-4">
                  <div class="form-group">
                    <label>Name Employee</label>
                    <input  name="nama" type="text" class="form-control border-list-olive" value="<?php echo $rowkaryawan['nama_karyawan'];?>">
                  </div>
                  <!-- /.form-group -->
                </div>
                <!-- /.col -->
                <div class="col-md-4">
                  <div class="form-group">
                    <label>NIK</label>
                    <input  name="nik" type="text" class="form-control border-list-olive" value="<?php echo $rowkaryawan['nik'];?>">
                  </div>
                </div>
                <!-- /.col -->
                <div class="col-md-4">
                  <!-- /.form-group -->
                  <div class="form-group">
                    <label>Date Join</label>
                    <div class="input-group date" id="reservationdate" data-target-input="nearest">
                      <input  name="tgl_masuk" type="text" class="form-control border-list-olive datetimepicker-input" data-target="#reservationdate" data-inputmask-alias="datetime" data-inputmask-inputformat="dd/mm/yyyy" data-mask value="<?php echo $tglmasuk;?>">
                      <div class="input-group-append" data-target="#reservationdate" data-toggle="datetimepicker">
                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                      </div>
                    </div>
                  </div>
                  <!-- /.form-group -->
                </div>
                <!-- /.col -->
              </div>
              <div class="row">
                <div class="col-md-4">
                  <!-- /.form-group -->
                  <div class="form-group">
                    <label>Employment Status</label>
                    <select  name="status_karyawan" class="form-control select2" style="width: 100%;">
                      <option selected="selected" value="<?php echo $rowkaryawan['status_karyawan'];?>"><?php echo $rowkaryawan['status_karyawan'];?></option>
                      <option value="Kontrak 1">Kontrak 1</option>
                      <option value="Kontrak 2">Kontrak 2</option>
                      <option value="Kontrak 3">Kontrak 3</option>
                    </select>
                  </div>
                  <!-- /.form-group -->
                </div>
                <!-- /.col -->
                <div class="col-md-4">
                  <div class="form-group">
                    <label>Departement</label>
                    <input  name="departemen" type="text" class="form-control border-list-olive" value="<?php echo $rowkaryawan['departemen_karyawan'];?>">
                  </div>
                </div>
                <!-- /.col -->
                <div class="col-md-4">
                  <!-- /.form-group -->
                  <div class="form-group">
                    <label>Position</label>
                    <input  name="posisi" type="text" class="form-control border-list-olive" value="<?php echo $rowkaryawan['posisi_karyawan'];?>">
                  </div>
                  <!-- /.form-group -->
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Golongan</label>
                    <input  name="golongan" type="text" class="form-control border-list-olive" value="<?php echo $rowkaryawan['golongan'];?>">
                  </div>
                  <!-- /.form-group -->
                </div>
                <!-- /.col -->
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Location Employment</label>
                    <input  name="lokasi" type="text" class="form-control border-list-olive" value="<?php echo $rowkaryawan['lokasi'];?>">
                  </div>
                  <!-- /.form-group -->
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->
              <div class="row">
                <div class="col-md-6">
                  <!-- /.form-group -->
                  <div class="form-group">
                    <label>Name User Process</label>
                    <select  name="user_proses" class="form-control select2" style="width: 100%;">
                      <option selected="selected" value="<?php echo $iduser1;?>"><?php echo "$row_user1[nama]"." - "."$row_user1[departemen]";?></option>
                      <?php while ($rowuser = mysqli_fetch_array($qryuser)) { ?>
                        <option value="<?php echo $rowuser['id'];?>">
                          <?php echo "$rowuser[nama]"." - "."$rowuser[departemen]";?>
                        </option>
                      <?php }?>
                    </select>
                  </div>
                  <!-- /.form-group -->
                </div>
                <!-- /.col -->
                <div class="col-md-6">
                  <!-- /.form-group -->
                  <div class="form-group">
                    <label>Name User Review</label>
                    <select name="user_review" class="form-control select2" style="width: 100%;">
                      <?php if ($iduser2) {;?>
                        <option selected="selected" value="<?php echo $iduser2;?>"><?php echo "$row_user2[nama]"." - "."$row_user2[departemen]";?></option>
                      <?php }else{ ?>
                        <option selected="selected"></option>
                      <?php };?>
                      <?php while ($rowuser2 = mysqli_fetch_array($qryuser2)) { ?>
                        <option value="<?php echo $rowuser2['id'];?>">
                          <?php echo "$rowuser2[nama]"." - "."$rowuser2[departemen]";?>
                        </option>
                      <?php }?>
                    </select>
                  </div>
                  <!-- /.form-group -->
                </div>
                <b>*Employment Status silahkan ubah di tabel Contract di atas</b>
                <!-- /.col -->
              </div>
            </div>
            <!-- /.card-body -->
            <div class="card-footer">
              <input required type="hidden" name="id" value="<?php echo $rowkaryawan['id']; ?>">
              <button onclick="return confirm('Apakah User Id yang anda ubah sudah benar?')" name="submit" type="submit" class="btn btn-primary float-right">Submit</button>
              <a href="viewass.php" class="btn btn-dark button-left button-space">&nbsp;Back&nbsp; </a>
            </div>
          </div>
          <!-- /.card -->
        </form>

      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <strong>Copyright &copy; 2020.</strong> All rights
    reserved.
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- Select2 -->
<script src="plugins/select2/js/select2.full.min.js"></script>
<!-- Bootstrap4 Duallistbox -->
<script src="plugins/bootstrap4-duallistbox/jquery.bootstrap-duallistbox.min.js"></script>
<!-- InputMask -->
<script src="plugins/moment/moment.min.js"></script>
<script src="plugins/inputmask/min/jquery.inputmask.bundle.min.js"></script>
<!-- date-range-picker -->
<script src="plugins/daterangepicker/daterangepicker.js"></script>
<!-- bootstrap color picker -->
<script src="plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
<!-- Bootstrap Switch -->
<script src="plugins/bootstrap-switch/js/bootstrap-switch.min.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
<!-- Page script -->
<script>
  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Initialize Select2 Elements
    $('.select2bs4').select2({
      theme: 'bootstrap4'
    })

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()

    //Date range picker
    $('#reservationdate').datetimepicker({
      format: 'DD/MM/YYYY',
      useCurrent: false
    });
    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({
      timePicker: true,
      timePickerIncrement: 30,
      locale: {
        format: 'MM/DD/YYYY hh:mm A'
      }
    })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
    {
      ranges   : {
        'Today'       : [moment(), moment()],
        'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
        'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
        'This Month'  : [moment().startOf('month'), moment().endOf('month')],
        'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
      },
      startDate: moment().subtract(29, 'days'),
      endDate  : moment()
    },
    function (start, end) {
      $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
    }
    )

    //Timepicker
    $('#timepicker').datetimepicker({
      format: 'LT'
    })
    
    //Bootstrap Duallistbox
    $('.duallistbox').bootstrapDualListbox()

    //Colorpicker
    $('.my-colorpicker1').colorpicker()
    //color picker with addon
    $('.my-colorpicker2').colorpicker()

    $('.my-colorpicker2').on('colorpickerChange', function(event) {
      $('.my-colorpicker2 .fa-square').css('color', event.color.toString());
    });

    $("input[data-bootstrap-switch]").each(function(){
      $(this).bootstrapSwitch('state', $(this).prop('checked'));
    });

  })
</script>
</body>
</html>