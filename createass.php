<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <link rel="shortcut icon" type="image/x-icon" href="gambar/logociputra.svg">
  <title>Ciputra</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- CSS Manual -->
  <link rel="stylesheet" type="text/css" href="assets/css/style.css">

  <!-- Font Awesome -->
  <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- daterange picker -->
  <link rel="stylesheet" href="plugins/daterangepicker/daterangepicker.css">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css">
  <!-- Tempusdominus Bbootstrap 4 -->
  <link rel="stylesheet" href="plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="plugins/select2/css/select2.min.css">
  <link rel="stylesheet" href="plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
  <!-- Bootstrap4 Duallistbox -->
  <link rel="stylesheet" href="plugins/bootstrap4-duallistbox/bootstrap-duallistbox.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

  <?php
  include 'koneksi.php';

  // mengaktifkan session
  session_start();
  if (!isset($_SESSION['adminlogin'])) {
  // if($_SESSION['status'] != "login") {
    echo '<script language="javascript">alert("Dilarang akses, login sebagai admin terlebih dahulu"); location.href="logout.php"</script>';
  }

  $sql = "SELECT nama, email, foto FROM tb_user WHERE email='$_SESSION[adminlogin]'";
  $qry = mysqli_query($koneksi, $sql) or die ("Query user salah!");
  $row = mysqli_fetch_array($qry);

  $sqluser = "SELECT id, nama, departemen FROM tb_user ORDER BY nama ASC";
  $qryuser = mysqli_query($koneksi, $sqluser);

  $sqluser2 = "SELECT id, nama, departemen FROM tb_user ORDER BY nama ASC";
  $qryuser2 = mysqli_query($koneksi, $sqluser2);

  if (isset($_POST['submit'])) {
    // error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
    $nama = trim($_POST['nama']);
    $nik = trim($_POST['nik']);
    $tgl_masuk = trim($_POST['tgl_masuk']);
    $status_karyawan = trim($_POST['status_karyawan']);
    $departemen = trim($_POST['departemen']);
    $posisi = trim($_POST['posisi']);
    $mulai_kontrak = trim($_POST['mulai_kontrak']);
    $selesai_kontrak = trim($_POST['selesai_kontrak']);
    $golongan = trim($_POST['golongan']);
    $lokasi = trim($_POST['lokasi']);
    $user_proses = trim($_POST['user_proses']);
    $user_review = trim($_POST['user_review']);

    function ubahTanggal1($tgl_masuk){
    $pisah1 = explode('/',$tgl_masuk);
    $array1 = array($pisah1[2],$pisah1[1],$pisah1[0]);
    $satukan1 = implode('-',$array1);
     return $satukan1;
    }

    $tgl_masuk2 = ubahTanggal1($tgl_masuk);


    function ubahTanggal2($mulai_kontrak){
    $pisah2 = explode('/',$mulai_kontrak);
    $array2 = array($pisah2[2],$pisah2[1],$pisah2[0]);
    $satukan2 = implode('-',$array2);
     return $satukan2;
    }

    $mulai_kontrak2 = ubahTanggal1($mulai_kontrak);


    function ubahTanggal3($selesai_kontrak){
    $pisah3 = explode('/',$selesai_kontrak);
    $array3 = array($pisah3[2],$pisah3[1],$pisah3[0]);
    $satukan3 = implode('-',$array3);
     return $satukan3;
    }

    $selesai_kontrak2 = ubahTanggal1($selesai_kontrak);

    $ceknik = "SELECT nik FROM tb_karyawan WHERE nik='$nik'";
    $ceknik2 = mysqli_query($koneksi, $ceknik);
    $ceknik3 = mysqli_fetch_array($ceknik2);


  
    if (empty($nama) && empty($nik) && empty($tgl_masuk2) && empty($status_karyawan) && empty($departemen) && empty($posisi) && empty($mulai_kontrak2) && empty($selesai_kontrak2) && empty($golongan) && empty($lokasi) && empty($user_proses)) {
      echo "<script>alert('Data masih kosong!');history.go(-1)</script>";
    }elseif (empty($nama)) {
      echo "<script>alert('Name Employment harus di isi!');history.go(-1)</script>";
    }elseif (empty($nik)) {
      echo "<script>alert('NIK harus di isi!');history.go(-1)</script>";
    }elseif (empty($tgl_masuk)) {
      echo "<script>alert('Date Join harus di isi!');history.go(-1)</script>";
    }elseif (empty($status_karyawan)) {
      echo "<script>alert('Employment Status harus di isi!');history.go(-1)</script>";
    }elseif (empty($departemen)) {
      echo "<script>alert('Departement harus di isi!');history.go(-1)</script>";
    }elseif (empty($posisi)) {
      echo "<script>alert('Position harus di isi!');history.go(-1)</script>";  
    }elseif (empty($mulai_kontrak)) {
      echo "<script>alert('Start Contract harus di isi!');history.go(-1)</script>";
    }elseif (empty($selesai_kontrak)) {
      echo "<script>alert('Finish Contract harus di isi!');history.go(-1)</script>";
    }elseif (empty($golongan)) {
      echo "<script>alert('Golongan harus di isi!');history.go(-1)</script>";
    }elseif (empty($user_proses)) {
      echo "<script>alert('Name User Process harus di isi!');history.go(-1)</script>"; 
    }elseif (empty($user_review)) {
      echo "<script>alert('Name User Review harus di isi!');history.go(-1)</script>"; 
    }elseif (!preg_match("/^[a-zA-Z ]*$/", $nama)) {
      echo "<script>alert('Name Employment tidak boleh menganduk special char dan angka!');history.go(-1)</script>";
    }elseif (strlen($nama) >= 60) {
      echo "<script>alert('Panjang Name Employment tidak boleh 60 Character!');history.go(-1)</script>";
    }elseif($ceknik3['nik'] == $nik){
      echo "<script>alert('NIK yang anda masukkan sudah ada!');history.go(-1)</script>";
    }elseif (strlen($nik) <= 7 && strlen($nik) >= 11) {
      echo "<script>alert('Panjang NIK tidak boleh lebih kecil dari 8 character atau lebih besar 10 character!');history.go(-1)</script>";
    }elseif ($status_karyawan != "Kontrak 1" && $status_karyawan != "Kontrak 2" && $status_karyawan != "Kontrak 3") {
      echo "<script>alert('Employment Status Salah!');history.go(-1)</script>";
    }elseif (strlen($golongan) >= 3) {
      echo "<script>alert('Panjang Golongan tidak boleh lebih besar dari 2!');history.go(-1)</script>";
    }elseif($user_proses == 1){
      echo "<script>alert('Silahkan pilih User Process!');history.go(-1)</script>";
    }else{

      $sql5 = "INSERT INTO tb_karyawan (id, nik, nama_karyawan, id_user1, id_user2, tgl_masuk, status_karyawan, lokasi, departemen_karyawan, posisi_karyawan, golongan, tgl_dibuat) VALUES (NULL,'$nik','$nama','$user_proses','$user_review','$tgl_masuk2','$status_karyawan','$lokasi','$departemen','$posisi','$golongan',current_timestamp())";
      $qry5 = mysqli_query($koneksi, $sql5) or die ("query insert salah");
      
      $cek = "SELECT id, nik FROM tb_karyawan WHERE nik='$nik'";
      $cekqry = mysqli_query($koneksi, $cek);
      $cekrow = mysqli_fetch_array($cekqry);
      $id = $cekrow['id'];
      
      if ($status_karyawan = "Kontrak 1") {
        $sql2 = "UPDATE tb_kontrak1 SET mulai_kontrak='$mulai_kontrak2', selesai_kontrak='$selesai_kontrak2' WHERE id_karyawan='$id'";
        $qry2 = mysqli_query($koneksi, $sql2) or die ("Query kontrak 1 salah!");
      }elseif ($status_karyawan = "Kontrak 2") {
        $sql3 = "UPDATE tb_kontrak2 SET mulai_kontrak='$mulai_kontrak2', selesai_kontrak='$selesai_kontrak2' WHERE id_karyawan='$id'";
        $qry3 = mysqli_query($koneksi, $qry3) or die ("Query kontrak 2 salah!");
      }elseif ($status_karyawan = "Kontrak 3") {
        $sql4 = "UPDATE tb_kontrak3 SET mulai_kontrak='$mulai_kontrak2', selesai_kontrak='$selesai_kontrak2' WHERE id_karyawan='$id'";
        $qry4 = mysqli_query($koneksi, $qry4) or die ("Query kontrak 3 salah!");
      }
      echo "<script>alert('Employment Assessment telah berhasil di tambahkan.');window.location='viewass.php'; </script>";
    }
  }
  ?>

</head>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-dark">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
      <!-- Notifications Dropdown Menu -->
      <li class="nav-item dropdown">
        <a class="nav-link" data-toggle="dropdown" href="#">
          <span><?php echo $_SESSION['adminlogin'];?></span>
          <i class="fas fa-user-alt"></i>
        </a>
        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
          <div class="dropdown-divider"></div>
          <a href="setting.php" class="dropdown-item">
            <i class="fas fa-cog mr-2"></i>
            <span class="float-right text-muted text-sm">Setting</span>
          </a>
          <div class="dropdown-divider"></div>
          <a href="logout.php" class="dropdown-item">
            <i class="fas fa-sign-out-alt mr-2"></i>
            <span class="float-right text-muted text-sm">Logout</span>
          </a>
        </div>
      </li>
    </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-light-olive elevation-4">
    <!-- Brand Logo -->
    <a href="index.php" class="brand-link navbar-light">
      <img src="gambar/logociputra2.png" alt="AdminLTE Logo" class="brand-image elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light"><b>CIPUTRA</b></span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <?php
            $cek_foto = $row['foto'];
            $tempat_foto = 'foto/'.$row['foto']; 
            if ($cek_foto) {
              echo "<img src='$tempat_foto' class='img-circle elevation-2' alt='User Image'>"; 
            }else{
              echo "<img src='foto/blank.png'></a>";
            }
          ?>
        </div>
        <div class="info">
          <a href="#" class="d-block"><?php echo $row['nama']; ?></a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item">
            <a href="createass.php" class="nav-link active">
              <i class="nav-icon fas fa-edit"></i>
              <p>
                Create Assessment
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="viewass.php" class="nav-link">
              <i class="nav-icon fas fa-clipboard-list"></i>
              <p>
                View Assessment
              </p>
            </a>
          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-users"></i>
              <p>
                Manage User Id
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="adduserid.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Add User Id</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="updateuserid.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Update User Id</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item">
            <a href="setting.php" class="nav-link">
              <i class="nav-icon fas fa-cog"></i>
              <p>
                Setting
              </p>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Create Assessment</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="index.php">Home</a></li>
              <li class="breadcrumb-item active">Create Assessment</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <form action="" method="post">
          <!-- SELECT2 EXAMPLE -->
          <div class="card card-olive">
            <div class="card-header">
              <h3 class="card-title">Create New Employee Assessment</h3>

              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
              </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <div class="row">
                <div class="col-md-4">
                  <div class="form-group">
                    <label>Name Employee</label>
                    <input  name="nama" type="text" class="form-control border-list-olive">
                  </div>
                  <!-- /.form-group -->
                </div>
                <!-- /.col -->
                <div class="col-md-4">
                  <div class="form-group">
                    <label>NIK</label>
                    <input  name="nik" type="text" class="form-control border-list-olive">
                  </div>
                </div>
                <!-- /.col -->
                <div class="col-md-4">
                  <!-- /.form-group -->
                  <div class="form-group">
                    <label>Date Join</label>
                      <div class="input-group date" id="reservationdate1" data-target-input="nearest">
                        <input  name="tgl_masuk" type="text" class="form-control border-list-olive datetimepicker-input" data-target="#reservationdate1" data-inputmask-alias="datetime" data-inputmask-inputformat="dd/mm/yyyy" data-mask>
                        <div class="input-group-append" data-target="#reservationdate1" data-toggle="datetimepicker">
                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                      </div>
                    </div>
                  </div>
                  <!-- /.form-group -->
                </div>
                <!-- /.col -->
              </div>
              <div class="row">
                <div class="col-md-4">
                  <!-- /.form-group -->
                  <div class="form-group">
                    <label>Employment Status</label>
                    <select  name="status_karyawan" class="form-control select2" style="width: 100%;">
                      <option selected="selected"></option>
                      <option value="Kontrak 1">Kontrak 1</option>
                      <option value="Kontrak 2">Kontrak 2</option>
                      <option value="Kontrak 3">Kontrak 3</option>
                    </select>
                  </div>
                  <!-- /.form-group -->
                </div>
                <!-- /.col -->
                <div class="col-md-4">
                  <div class="form-group">
                    <label>Departement</label>
                    <input  name="departemen" type="text" class="form-control border-list-olive">
                  </div>
                </div>
                <!-- /.col -->
                <div class="col-md-4">
                  <!-- /.form-group -->
                  <div class="form-group">
                    <label>Position</label>
                    <input  name="posisi" type="text" class="form-control border-list-olive">
                  </div>
                  <!-- /.form-group -->
                </div>
                <!-- /.col -->
              </div>
              <div class="row">
                <div class="col-12 col-sm-6">
                  <!-- /.form-group -->
                  <div class="form-group">
                    <label>Start Contract</label>
                      <div class="input-group date" id="reservationdate2" data-target-input="nearest">
                        <input  name="mulai_kontrak" type="text" class="form-control border-list-olive datetimepicker-input" data-target="#reservationdate2" data-inputmask-alias="datetime" data-inputmask-inputformat="dd/mm/yyyy" data-mask>
                        <div class="input-group-append" data-target="#reservationdate2" data-toggle="datetimepicker">
                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                      </div>
                    </div>
                  </div>
                  <!-- /.form-group -->
                </div>
                <!-- /.col -->
                <div class="col-12 col-sm-6">
                  <!-- /.form-group -->
                  <div class="form-group">
                    <label>Finish Contract</label>
                      <div class="input-group date" id="reservationdate3" data-target-input="nearest">
                        <input  name="selesai_kontrak" type="text" class="form-control border-list-olive datetimepicker-input" data-target="#reservationdate3" data-inputmask-alias="datetime" data-inputmask-inputformat="dd/mm/yyyy" data-mask>
                        <div class="input-group-append" data-target="#reservationdate3" data-toggle="datetimepicker">
                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                      </div>
                    </div>
                  </div>
                  <!-- /.form-group -->
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Golongan</label>
                    <input  name="golongan" type="text" class="form-control border-list-olive">
                  </div>
                  <!-- /.form-group -->
                </div>
                <!-- /.col -->
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Location Employment</label>
                    <input  name="lokasi" type="text" class="form-control border-list-olive">
                  </div>
                  <!-- /.form-group -->
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->
              <div class="row">
                <div class="col-md-6">
                  <!-- /.form-group -->
                  <div class="form-group">
                    <label>Name User Process</label>
                    <select  name="user_proses" class="form-control select2" style="width: 100%;">
                      <option selected="selected" value=""></option>
                      <?php while ($rowuser = mysqli_fetch_array($qryuser)) { ?>
                      <option value="<?php echo $rowuser['id'];?>">
                        <?php echo "$rowuser[nama]"." - "."$rowuser[departemen]";?>
                      </option>
                      <?php }?>
                    </select>
                  </div>
                  <!-- /.form-group -->
                </div>
                <!-- /.col -->
                <div class="col-md-6">
                  <!-- /.form-group -->
                  <div class="form-group">
                    <label>Name User Review</label>
                    <select name="user_review" class="form-control select2" style="width: 100%;">
                      <option selected="selected" value=""></option>
                      <?php while ($rowuser2 = mysqli_fetch_array($qryuser2)) { ?>
                      <option value="<?php echo $rowuser2['id'];?>">
                        <?php echo "$rowuser2[nama]"." - "."$rowuser2[departemen]";?>
                      </option>
                      <?php }?>
                    </select>
                  </div>
                  <!-- /.form-group -->
                </div>
                <!-- /.col -->
              </div>
            </div>
            <!-- /.card-body -->
            <div class="card-footer">
              <button onclick="return confirm('Apakah User Id yang anda buat sudah benar?')" name="submit" type="submit" class="btn btn-primary float-right">Submit</button>
            </div>
          </div>
          <!-- /.card -->
        </form>
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <strong>Copyright &copy; 2020.</strong> All rights
    reserved.
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- Select2 -->
<script src="plugins/select2/js/select2.full.min.js"></script>
<!-- Bootstrap4 Duallistbox -->
<script src="plugins/bootstrap4-duallistbox/jquery.bootstrap-duallistbox.min.js"></script>
<!-- InputMask -->
<script src="plugins/moment/moment.min.js"></script>
<script src="plugins/inputmask/min/jquery.inputmask.bundle.min.js"></script>
<!-- date-range-picker -->
<script src="plugins/daterangepicker/daterangepicker.js"></script>
<!-- bootstrap color picker -->
<script src="plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
<!-- Bootstrap Switch -->
<script src="plugins/bootstrap-switch/js/bootstrap-switch.min.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
<!-- Page script -->
<script>
  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Initialize Select2 Elements
    $('.select2bs4').select2({
      theme: 'bootstrap4'
    })

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()

    //Date range picker
    $('#reservationdate1').datetimepicker({
        format: 'DD/MM/YYYY',
        useCurrent: false
    });
    $('#reservationdate2').datetimepicker({
        format: 'DD/MM/YYYY',
        useCurrent: false
    });
    $('#reservationdate3').datetimepicker({
        format: 'DD/MM/YYYY',
        useCurrent: false
    });
    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({
      timePicker: true,
      timePickerIncrement: 30,
      locale: {
        format: 'MM/DD/YYYY hh:mm A'
      }
    })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )

    //Timepicker
    $('#timepicker').datetimepicker({
      format: 'LT'
    })
    
    //Bootstrap Duallistbox
    $('.duallistbox').bootstrapDualListbox()

    //Colorpicker
    $('.my-colorpicker1').colorpicker()
    //color picker with addon
    $('.my-colorpicker2').colorpicker()

    $('.my-colorpicker2').on('colorpickerChange', function(event) {
      $('.my-colorpicker2 .fa-square').css('color', event.color.toString());
    });

    $("input[data-bootstrap-switch]").each(function(){
      $(this).bootstrapSwitch('state', $(this).prop('checked'));
    });

  })
</script>
</body>
</html>